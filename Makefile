CXX = 		g++ -g -fmessage-length=0 -w

SOURCE =	src

OUTPUT =	bin

LD_DIRS = 	-L/opt/samtools -I/opt/samtools

SAM_LIBS =	-lbam -lz -lpthread

DCP_LIBS =	-I/opt/OGDF -lOGDF

HD5_LIBS =	-lhdf5 -lhdf5_hl

all:	pair nodes edges filter bundles decompose agpmaker
	
pair:
	$(CXX) -o $(OUTPUT)/pair_sam $(SOURCE)/pair_sam.cpp $(SOURCE)/types.cpp $(LD_DIRS) $(SAM_LIBS) $(HD5_LIBS)
	
nodes:	
	$(CXX) -o $(OUTPUT)/create_nodes $(SOURCE)/create_nodes.cpp $(SOURCE)/types.cpp $(LD_DIRS) $(SAM_LIBS) $(HD5_LIBS)
	
edges:
	$(CXX) -o $(OUTPUT)/create_edges $(SOURCE)/create_edges.cpp $(SOURCE)/types.cpp $(LD_DIRS) $(SAM_LIBS) $(HD5_LIBS)
	
filter:
	$(CXX) -o $(OUTPUT)/filter_edges $(SOURCE)/filter_edges.cpp $(SOURCE)/types.cpp $(LD_DIRS) $(SAM_LIBS) $(HD5_LIBS) $(DCP_LIBS)
	
bundles:
	$(CXX) -o $(OUTPUT)/create_bundles $(SOURCE)/create_bundles.cpp $(SOURCE)/types.cpp $(LD_DIRS) $(HD5_LIBS) $(DCP_LIBS)
	
decompose:	
	$(CXX) -o $(OUTPUT)/decompose $(SOURCE)/decompose.cpp $(SOURCE)/BundleGraph.cpp $(SOURCE)/types.cpp $(LD_DIRS) $(DCP_LIBS) $(HD5_LIBS)

agpmaker:
	$(CXX) -o $(OUTPUT)/agp_maker $(SOURCE)/agp_maker.cpp $(SOURCE)/IntervalTree.h $(SOURCE)/types.cpp $(LD_DIRS) $(SAM_LIBS) $(HD5_LIBS)

clean:
	rm -f $(SOURCE)/*.o $(OUTPUT)/*
