'''
evaluates AGP
'''
# program imports.
from data_structs.nodes import load_nodes
from data_structs.edges import load_edges
from data_structs.bundles import load_bundles
from data_structs.agps import load_agps
from data_structs.types import agp_dt

# system imports.
import math
import numpy as np
import subprocess
import networkx as nx
import logging
import sys
import os

# logging.
#logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

# parameters.
NODE_FILE = sys.argv[1]
EDGE_FILE = sys.argv[2]
BUNDLE_FILE = sys.argv[3]
AGP_FILE = sys.argv[4]

########### classes ####################

########### functions ##################
	
def make_agp(fpath):
	''' returns agp graph '''
	
	# load agp array.
	agp_edges = load_agps(fpath)
	
	# build sorted edge set.
	edges = set()
	for i in range(agp_edges.size):
		
		# skip contigs themselves.
		if agp_edges[i]['comp_type'] != 'N': continue
		
		# add sorted edges.
		key = tuple(sorted([agp_edges[i-1]['comp_name'], agp_edges[i+1]['comp_name']]))
		edges.add(key)
		
	
	return edges, agp_edges
			
########### script ################## 

# load the nodes and edges.
nodes = load_nodes(NODE_FILE)
edges = load_edges(EDGE_FILE)
bundles = load_bundles(BUNDLE_FILE)

# load the agp info.
ref_edges, agps = make_agp(AGP_FILE)

# check if any of the edges are in actual.
hit = 0
mis = 0
key_hits = set()
key_miss = set()
hit_state = dict()
for edge in edges:
	
	# make keys.
	ida = nodes[edge["ctg_a_idx"]]["ctg_name"]
	idb = nodes[edge["ctg_b_idx"]]["ctg_name"]
	key1 = (ida, idb)
	key2 = (idb, ida)
	key3 = tuple(sorted([ida, idb]))
	
	# check set for key.
	if key1 in ref_edges or key2 in ref_edges:
		# coutn it.
		hit += 1
		key_hits.add(key3)
		
		# increment state.
		if key3 not in hit_state:
			hit_state[key3] = dict()
			hit_state[key3][0] = 0
			hit_state[key3][1] = 0
			hit_state[key3][2] = 0
			hit_state[key3][3] = 0
		hit_state[key3][edge["implied_state"]] += 1	
		
	else:
		mis += 1
		key_miss.add(key3)

# compute key orientation.
oriens = dict()
for e in agps:
	if e['comp_type'] == "W":
		oriens[e['comp_name']] = e['comp_orien']

# count good orientation.
go = 0
bo = 0
for ida, idb in hit_state:

	# get actual orientation.
	if oriens[ida] == oriens[idb]:
		o = 0
	else:
		o = 1
		
	# check which category its from.
	cnt_o_0 = hit_state[(ida,idb)][0] + hit_state[(ida,idb)][3]
	cnt_o_1 = hit_state[(ida,idb)][1] + hit_state[(ida,idb)][2]
	
	# find orientation.
	if o == 0:
		go += cnt_o_0
		bo += cnt_o_1
	else:
		go += cnt_o_1
		bo += cnt_o_0	
	
# check bundles.
bhit = 0
bmis = 0
bgo = 0
bbo = 0
for b in bundles:
	
	# make keys.
	ida = nodes[b["ctg_a_idx"]]["ctg_name"]
	idb = nodes[b["ctg_b_idx"]]["ctg_name"]
	key1 = (ida, idb)
	key2 = (idb, ida)
		
	if key1 in ref_edges or key2 in ref_edges:
		bhit += 1
	else:
		bmis += 1
		
	# get actual orientation.
	if oriens[ida] == oriens[idb]:
		o = 0
	else:
		o = 1
		
	# add weight.
	cnt_o_0 = b['WT_A'] + b['WT_D']
	cnt_o_1 = b['WT_B'] + b['WT_C']
	
	# find orientation.
	if o == 0:
		bgo += cnt_o_0
		bbo += cnt_o_1
	else:
		bgo += cnt_o_1
		bbo += cnt_o_0	
	

print "# good   edges: ", hit
print "# bad    edges: ", mis
print "# good   pairs: ", len(key_hits)
print "# bad    pairs: ", len(key_miss)
print "# good  oriens: ", go
print "# bad   oriens: ", bo
print "# good bundles: ", bhit
print "# bad  bundles: ", bmis
print "# good boriens: ", bgo
print "# bad  boriens: ", bbo

	
