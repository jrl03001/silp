'''
solves scaffolding problem using ILP
on pairs only. Then weighted bi-partite matching
to find the paths.
'''
# program imports.
from data_structs.nodes import load_nodes
from data_structs.bundles import load_bundles
from scaflib.misc_functions import draw

# system imports.
from collections import deque
import random
import time
import numpy as np
import subprocess
import itertools
import networkx as nx
import logging
import sys
import os
import h5py
import tempfile
time_start = time.time()

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

# parameters.
bundle_file = sys.argv[1]
print_file = sys.argv[2]

# load the edges.
bundles = load_bundles(bundle_file)

# create graph.
G = nx.Graph()
for b in bundles:
	G.add_edge(b['ctg_a_idx'],b['ctg_b_idx'])
	
draw("scaffold", G)
