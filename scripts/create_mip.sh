#!/bin/bash
# runs scaffolding pipeline.

# parameters.
SAM_1_FILE=$1
SAM_2_FILE=$2
CTG_FILE=$3
WORK_DIR=$4

# hardcoded.
MIP_MERGE=/opt/mip-scaffolder/scripts/merge.sh
MIP_MAP=/opt/mip-scaffolder/scripts/filter-mappings.sh
CTG_COVER=/home/jlindsay/central/code/SILP/scripts/contig_coverage.py

# make dirs.
for x in $GRAPH_DIR $MIP_DIR; do
        if [ ! -d "$x" ]; then
                mkdir $x || exit 1;
        fi
done;

# switch into mips dir.
cd $MIP_DIR

# merge reads.
opt/mip-scaffolder/scripts/merge.sh ${SAM_2_FILE} ${SAM_1_FILE} merged

# filter reads.
${MIP_MAP} merged.sorted2 filtered.sam

# make coverage.
python ${CTG_COVER} ${CTG_FILE} coverage.txt ${SAM_2_FILE} ${SAM_1_FILE}

# copy contig to dir.
cp ${CTG_COVER} ./

# return.
cd ../
