'''
These are generator functions which provide linear access to
variable ascii column based formats such as SAM, GFF, etc...

These functions rely on numpy for efficient buffer and
file reading.

These functions do not provide random access to elements in
these data types.
'''
# imports.
import os
import numpy as np
import mmap
import re
import contextlib
import logging
import csv
import sys
import gzip

########## default parameters.
DEFAULT_A = 50000
DEFAULT_B = 104857600
WRITE_BUFFER_SZ = 100000

########## data types.
sam_dt = np.dtype([\
	('QNAME', 'S255'),\
	('FLAG', np.int),\
	('RNAME', 'S255'),\
	('POS', np.int),\
	('MAPQ', np.int),\
	('CIGAR', 'S255'),\
	('MRNM', 'S255'),\
	('MPOS', np.int),\
	('ISIZE', np.int),\
	('SEQ', 'S71000'),\
	('QUAL', 'S71000'),\
	('OPTIONAL', 'S255')\
])

gff_dt = np.dtype([\
	("seqname","S255"),\
	("source","S50"),\
	("feature","S50"),\
	("start",np.int),\
	("end",np.int),\
	("score","S50"),\
	("strand","S5"),\
	("frame","S5"),\
	("group","S500"),\
])

fasta_dt = np.dtype([\
	("name","S255"),\
	("seq",np.object),\
])

fastq_dt = np.dtype([\
	("name","S255"),\
	("seq",np.object),\
	("qual",np.object),\
])

gff_fmt = "%s\t%s\t%s\t%i\t%i\t%s\t%s\t%s\t%s\n"
sam_fmt = "%s\t%i\t%s\t%i\t%i\t%s\t%s\t%i\t%i\t%s\t%s\t%s\n"
fasta_fmt = ">%s\n%s\n"
fastq_fmt = "@%s\n%s\n+\n%s\n"
	
########## assistance functions.
def type_cols(cast, d):
	''' applies type cast and save to array.'''
	print d
	return cast(d)
	
def open_file(file_path):
	''' tries to open file for reading '''
	
	# sanity check.
	if os.path.isfile(file_path) == False:
		logging.error("not a file.")
		sys.exit()
	
	
	# try gzip first.
	try:
		# open the file as gzip.
		fin = gzip.open(file_path, "rb")
		
		# try a read to force error.
		fin.read(10)
		
		
	except IOError:
		
		# try normal next.
		fin = open(file_path, "rb")	
		
	# return file object.
	return fin
	
########## class to handle writing.
class Writer(object):
	''' creates appropriate buffer for writing '''
	
	def __init__(self, fpath, obj_dt, obj_fmt, gzip=False):
		''' creates buffers '''
		
		# open file.
		if fpath == "-":
			# std out.
			self.file_ptr = sys.stdout
			
		else:
			# to a file.
			if gzip == False:
				self.file_ptr = open(fpath, "wb")
			else:
				self.file_ptr = gzip.open(fpath, "wb")
		
		# create buffer.
		self.write_buffer = np.zeros(WRITE_BUFFER_SZ, dtype=obj_dt)
		self.idx = 0
		
		# save info.
		self.dt = obj_dt
		self.fmt = obj_fmt
		
	def setup(self):
		''' returns simple array to fill '''
		return np.zeros(1, dtype=self.dt)[0]
		
	def write(self, arr):
		''' saves to buffer and writes if full.'''
		
		# save to buffer.
		for key in self.dt.fields.keys():
			self.write_buffer[self.idx][key] = arr[key]
		self.idx += 1
		
		# write if necessary
		if self.idx >= WRITE_BUFFER_SZ:
			# write to txt.
			for x in self.write_buffer:
				self.file_ptr.write( self.fmt % tuple(x) )
			self.idx = 0
			
	def close(self):
		''' alias for finalize '''
		self.finalize()
			
	def finalize(self):
		''' saves final buffer and closes file.'''
		
		# save buffer.
		for i in range(self.idx):
			self.file_ptr.write( self.fmt % tuple(self.write_buffer[i]) )
		
		# close file.
		self.file_ptr.close()
	
########## generator functions.
def sam_buffer(file_path, buffer_sz=DEFAULT_A, read_sz=DEFAULT_B):
	''' memory map files and split into datatype '''
	
	# allocate buffer.
	buffer = np.zeros(buffer_sz, dtype=sam_dt)
	
	# open file correctly.
	fin = open_file(file_path)
		
	# load initial lines.
	bidx = 0
	lines = fin.readlines(read_sz)
	while lines != []:
		
		# loop over line
		for line in lines:
			
							
			# split the line.
			if line[0] == "@": continue
			line = line.strip()
			tmp = line.split("\t")
			
			# sanity check.
			if len(tmp) <= 9:
				continue
			
			# fill array.
			buffer[bidx]['QNAME'] = tmp[0]
			buffer[bidx]['FLAG'] = int(tmp[1])
			buffer[bidx]['RNAME'] = tmp[2]
			buffer[bidx]['POS'] = int(tmp[3])
			buffer[bidx]['MAPQ'] = tmp[4]
			buffer[bidx]['CIGAR'] = tmp[5]
			buffer[bidx]['MRNM'] = tmp[6]
			buffer[bidx]['MPOS'] = int(tmp[7])
			buffer[bidx]['ISIZE'] = int(tmp[8])
			buffer[bidx]['SEQ'] = tmp[9]
			buffer[bidx]['QUAL'] = tmp[10]
			buffer[bidx]['OPTIONAL'] = '\t'.join(tmp[11:])
			bidx += 1
			
			# stop criteria.
			if bidx >= buffer_sz:
				
				# yield this buffer.
				for i in range(bidx):
					yield buffer[i]
					
				# reset idx.
				bidx = 0
				
		# load next set of lines.
		lines = fin.readlines(read_sz)
					
	# yield last set.
	for i in range(bidx):
		yield buffer[i]
			

	# close file.
	fin.close()

	# stop iteration.
	raise StopIteration
	
def sam_agp_buffer(sam_path, agp_path, buffer_sz=DEFAULT_A, read_sz=DEFAULT_B):
	''' memory map files and split into datatype '''
	
	# load agp file
	with open(agp_path, 'rb') as fin:
		lines = fin.readlines()
		
	# parse out translation.
	agp = {}
	for line in lines:
		# tokenize.
		tmp = line.split()
		if tmp[4] != "W": continue
		
		scaf = tmp[0]
		start = int(tmp[1])
		stop = int(tmp[2])
		ctg = tmp[5]
		
		# save to dict.
		agp[ctg] = (scaf, start, stop)
		
	
	# allocate buffer.
	buffer = np.zeros(buffer_sz, dtype=sam_dt)
	
	# open the input file.
	with open(sam_path, 'rb') as fin:
		
		# load initial lines.
		bidx = 0
		lines = fin.readlines(read_sz)
		while lines != []:
			
			# loop over line
			for line in lines:
								
				# split the line.
				if line[0] == "@": continue
				tmp = line.split("\t")
				if len(tmp) <= 9:
					continue
				
				# sanity check.
				if tmp[2] not in agp:
					logging.error("agp file missing: %s" % tmp[2])
					sys.exit(1)
					continue
				
				# fill array.
				buffer[bidx]['QNAME'] = tmp[0]
				buffer[bidx]['FLAG'] = int(tmp[1])
				buffer[bidx]['RNAME'] = agp[tmp[2]][0]
				buffer[bidx]['POS'] = agp[tmp[2]][1] + int(tmp[3]) 
				buffer[bidx]['MAPQ'] = tmp[4]
				buffer[bidx]['CIGAR'] = tmp[5]
				buffer[bidx]['MRNM'] = tmp[6]
				buffer[bidx]['MPOS'] = int(tmp[7])
				buffer[bidx]['ISIZE'] = int(tmp[8])
				buffer[bidx]['SEQ'] = tmp[9]
				buffer[bidx]['QUAL'] = tmp[10]
				buffer[bidx]['OPTIONAL'] = '\t'.join(tmp[11:])
				bidx += 1
				
				# stop criteria.
				if bidx >= buffer_sz:
					
					# yield this buffer.
					for i in range(bidx):
						yield buffer[i]
						
					# reset idx.
					bidx = 0
					
			# load next set of lines.
			lines = fin.readlines(read_sz)
					
		# yield last set.
		for i in range(bidx):
			yield buffer[i]
			

	# stop iteration.
	raise StopIteration
		

def gff_buffer(file_path, buffer_sz=DEFAULT_A, read_sz=DEFAULT_B):
	''' simple gff buffer.'''
	# allocate buffer.
	buffer = np.zeros(buffer_sz, dtype=gff_dt)
	
	# open the input file.
	fin = open_file(file_path)
		
	# load initial lines.
	bidx = 0
	lines = fin.readlines(read_sz)
	while lines != []:
		
		# loop over line
		for line in lines:
							
			# split the line.
			if line[0] == "#": continue
			tmp = line.strip().split("\t")
			if len(tmp) < 9:
				continue
			
			
			# fill array.
			buffer[bidx]['seqname'] = tmp[0]
			buffer[bidx]['source'] = tmp[1]
			buffer[bidx]['feature'] = tmp[2]
			buffer[bidx]['start'] = int(tmp[3])
			buffer[bidx]['end'] = int(tmp[4])
			buffer[bidx]['score'] = tmp[5]
			buffer[bidx]['strand'] = tmp[6]
			buffer[bidx]['frame'] = tmp[7]
			buffer[bidx]['group'] = tmp[8]
			bidx += 1
			
			# stop criteria.
			if bidx >= buffer_sz:
				
				# yield this buffer.
				for i in range(bidx):
					yield buffer[i]
					
				# reset idx.
				bidx = 0
				
		# load next set of lines.
		lines = fin.readlines(read_sz)
				
	# yield last set.
	for i in range(bidx):
		yield buffer[i]
		
	# close file.
	fin.close()
		
	# stop iteration.
	raise StopIteration
	
		

def fasta_buffer(file_path, buffer_sz=50000):
	''' iterates over fasta file '''
	
	# create buffer,
	buffer = np.zeros(buffer_sz, dtype=fasta_dt)

	# open file.
	fin = open_file(file_path)
	
	# iterate over file.
	bidx = 0
	first = True
	for line in fin:
		# check if we start new entry.
		if line[0] == ">":
			# tally.
			if first == False:
				# save entry.
				buffer[bidx]['name'] = head
				buffer[bidx]['seq'] = seq
				bidx += 1
				
				# clear buffer.
				if bidx >= buffer_sz:
					for z in range(bidx):
						yield buffer[z]
					bidx = 0
			else:
				first = False
				
			# clear.
			head = line.strip().replace(">","")
			seq = ""
			continue
		
		
		# get only seq.
		seq += line.strip()
		
	# save last and close.
	buffer[bidx]['name'] = head
	buffer[bidx]['seq'] = seq
	bidx += 1
	
	# clear buffer.
	for z in range(bidx):
		yield buffer[z]
		
	# close file.
	fin.close()
	
	# stop iteration.
	raise StopIteration

def fastq_buffer(file_path, buffer_sz=50000):
	''' iterates over fasta file '''
	
	# create buffer,
	buffer = np.zeros(buffer_sz, dtype=fastq_dt)

	# open file correctly.
	fin = open_file(file_path)
			
	# iterate over file.
	bidx = 0
	first = True
	for line in fin:
		# check if we start new entry.
		if line[0] == "@":
			# tally.
			if first == False:
				# save entry.
				buffer[bidx]['name'] = head
				buffer[bidx]['seq'] = seq
				buffer[bidx]['qual'] = qual
				bidx += 1
				
				# clear buffer.
				if bidx >= buffer_sz:
					for z in range(bidx):
						yield buffer[z]
					bidx = 0
			else:
				first = False
				
			# clear.
			head = line.strip().replace("@","")
			seq = ""
			qual = ""
			in_seq = True
			in_qual = False
			continue
		
		# check if we have qual.
		elif line[0] == "+":
			# set in qual.
			in_seq = False
			in_qual = True
			
			
		# grab quality.
		elif in_qual == True:
			qual += line.strip()
			
			
		# grab seq.
		elif in_seq == True:
			seq += line.strip()
			
		# sanity.
		else:
			logging.error("bad formatting.")
			sys.exit(1)
		
	# save last and close.
	buffer[bidx]['name'] = head
	buffer[bidx]['seq'] = seq
	buffer[bidx]['qual'] = qual
	
	# clear buffer.
	for z in range(bidx):
		yield buffer[z]
		
	# close file.
	fin.close()

	# stop iteration.
	raise StopIteration
