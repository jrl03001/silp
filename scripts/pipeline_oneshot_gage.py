'''
driver script for one shot run.
'''
# system imports.
import sys
import os
import subprocess
import logging

# program imports.
from data_structs.edges import load_edges
from data_structs.edges import save_edges

logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

######### parameters #########

## user-provided.
FASTA_FILE = os.path.abspath(sys.argv[1])
SAM_1_FILE = os.path.abspath(sys.argv[2])
SAM_2_FILE = os.path.abspath(sys.argv[3])
GFF_FILE = os.path.abspath(sys.argv[4])
WORK_DIR = os.path.abspath(sys.argv[5])
INSERT_SIZE = int(sys.argv[6])
STD_DEV = int(sys.argv[7])
NAME_BASE = sys.argv[8]
BUNDLE_SIZE = int(sys.argv[9])

## parameters.

#FILTERS = "bundle"
FILTERS = "bundle,min_bound,neighbor"
#FILTERS = "bundle,min_bound,neighbor"
FILTER_NSIZE = 15
FILTER_STD_MIN = 3
FILTER_STD_MAX = 20
FILTER_REP_OVR = 0.5

DECOMP_BOUND = 1500

## hard-coded file paths.
INPUT_DIR = "%s/input" % WORK_DIR
AGP_DIR = "%s/agps" % WORK_DIR
GRAPH_DIR = "%s/graphs" % WORK_DIR
LOG_DIR = "%s/logs" % WORK_DIR
SERIAL_DIR = "%s/serial" % WORK_DIR

NODE_FILE = "%s/nodes.hdf5" % INPUT_DIR
EDGE_FILE = "%s/edges.hdf5" % INPUT_DIR
BUNDLE_FILE = "%s/bundles.hdf5" % INPUT_DIR

AGP_FILE = "%s/%s_%i.agp" % (AGP_DIR, NAME_BASE, BUNDLE_SIZE)

PIPE_OUT = "%s/pipeline.out" % LOG_DIR
PIPE_ERR = "%s/pipeline.err" % LOG_DIR

## hard-coded commands.
CMD_CREATE_NODES = "/home/jlindsay/central/code/SILP/bin/create_nodes"
CMD_CREATE_EDGES = "/home/jlindsay/central/code/SILP/bin/create_edges"
CMD_CREATE_BUNDLES = "/home/jlindsay/central/code/SILP/bin/create_bundles"

CMD_FLIP_EDGES = "/home/jlindsay/central/code/SILP/scripts/reverse_edge.py"
CMD_FILTER_EDGES = "/home/jlindsay/central/code/SILP/bin/filter_edges"
CMD_FILTER_EDGES_2 = "/home/jlindsay/central/code/SILP/scripts/seq_filter.py"

CMD_DECOMPOSE = "/home/jlindsay/central/code/SILP/scripts/decompose.sh"
CMD_SCAFFOLD1 = "/home/jlindsay/central/code/SILP/scripts/scaffold_part1.py"
CMD_SCAFFOLD2 = "/home/jlindsay/central/code/SILP/scripts/scaffold_part2_mcmw.py"

######### functions #########

def run_wrapper(cmd, echo=False):
	''' wrapper to run commands '''
	
	# stringify cmd.
	for i in range(len(cmd)):
		cmd[i] = str(cmd[i])
	
	if echo == True:
		print ' '.join(cmd)
	
	# run command.
	fout = open(PIPE_OUT, "a")
	ferr = open(PIPE_ERR, "a")
	ret = subprocess.call(cmd, stdout=fout, stderr=ferr)
	fout.close()
	ferr.close()
	
	# print a failure.
	if ret != 0:
		logging.error("failure: %s" % ' '.join(cmd))
	
	# return result.
	return ret

def prep_dir(path):
	''' checks if dir exists, if not make it'''
	if os.path.isdir(path) == False:
		if run_wrapper(["mkdir", path]) != 0:
			logging.error("couldn't make directory: %s" % path)
			sys.exit(1)
			
######### script #########
	
## always run these working and log steps.
logging.info("prep working dir and clear logs")

# sanity check working dir.
if os.path.isdir(WORK_DIR) == False:
	subprocess.call(["mkdir", WORK_DIR])
	
# always clear logs and reset logs automatically.
if os.path.isdir(LOG_DIR) == True:
	subprocess.call(["rm", "-rf", LOG_DIR])
subprocess.call(["mkdir", LOG_DIR])

# prepare directories.
for x in [WORK_DIR, INPUT_DIR, GRAPH_DIR, LOG_DIR, SERIAL_DIR]:
	prep_dir(x)

# create nodes.
logging.info("making nodes")
if run_wrapper([CMD_CREATE_NODES, FASTA_FILE, NODE_FILE]) != 0:
	logging.error("couldn't make node file")
	sys.exit(1)

# create edges.
logging.info("making edges")
if run_wrapper([CMD_CREATE_EDGES, NODE_FILE, SAM_1_FILE, SAM_2_FILE, EDGE_FILE, INSERT_SIZE, STD_DEV]) != 0:
	logging.error("couldn't make node file")
	sys.exit(1)
	
# flip edges.
logging.info("flipping edges")
if run_wrapper(["python", CMD_FLIP_EDGES, NODE_FILE, EDGE_FILE, "read_b_orien"]) != 0:
	logging.error("couldn't flip edge file")
	sys.exit(1)
	

# filter edges.
logging.info("filtering edges 1")
if run_wrapper([
	CMD_FILTER_EDGES, 
	FILTERS, 
	"-nodes", NODE_FILE, 
	"-edges", EDGE_FILE, 
	"-gff", GFF_FILE,
	"-bundle", BUNDLE_SIZE,
	"-neighbor", FILTER_NSIZE,
	"-std_min", FILTER_STD_MIN,
	"-std_max", FILTER_STD_MAX,
	"-rep_over", FILTER_REP_OVR,
	"-agp", "-"
], echo = False) != 0:
	logging.error("couldn't filter edge file")
	sys.exit(1)
'''
# filter more edges.
logging.info("filtering edges 2")
if run_wrapper([
	"python",
	CMD_FILTER_EDGES_2, 
	NODE_FILE, 
	EDGE_FILE, 
	FASTA_FILE
], echo = False) != 0:
	logging.error("couldn't filter edge file 2")
	sys.exit(1)
'''
# make the bundles.
logging.info("making bundles")
if run_wrapper([
	CMD_CREATE_BUNDLES,
	NODE_FILE,
	EDGE_FILE,
	BUNDLE_FILE,
	#"repeat", GFF_FILE,
	"count", "-",
	"-",
	0
], echo = False) != 0:
	logging.error("couldn't create bundles")
	sys.exit(1)

# decompose the graph.
logging.info("making decomposition")
if run_wrapper([
	CMD_DECOMPOSE,
	NODE_FILE,
	BUNDLE_FILE,
	GRAPH_DIR,
	DECOMP_BOUND
], echo = False) != 0:
	logging.error("could not decompose the graph")
	sys.exit(1)

logging.info("running program: 1")
run_wrapper([
	"python",
	CMD_SCAFFOLD1,
	WORK_DIR,
	AGP_FILE,
	"-"
])
logging.info("running program: 2")
run_wrapper([
	"python",
	CMD_SCAFFOLD2,
	WORK_DIR,
	AGP_FILE,
	"-"
])

