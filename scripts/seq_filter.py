'''
applies an additional level of filter
on negative length gaps looking for lack of sequence similarity
to rule out negative overlaps.
'''
# program imports.
from data_structs.edges import load_edges
from data_structs.edges import save_edges
from data_structs.nodes import load_nodes


# system imports.
from collections import deque
import random
import time
import numpy as np
import subprocess
import itertools
import networkx as nx
import logging
import sys
import os
import h5py
import tempfile
import string

time_start = time.time()


logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

### parameters. ###
NODE_FILE = sys.argv[1]
EDGE_FILE = sys.argv[2]
REF_FILE = sys.argv[3]

X_TIMES = 3
#STD_DEV = 250
STD_DEV = 10
MAX_COV = 50

### functions. ###

def load_fasta(file_path):
	''' loads fasta file into dictionary'''
	
	# read file into memory.
	fin = open(file_path)
	lines = fin.readlines()
	fin.close()
	
	# build dictionary.
	data = dict()
	seq = ""
	for line in lines:
		
		# Skip blanks.
		if len(line) < 2: continue
		if line[0] == "#": continue

		# remove blanks.
		line = line.strip()

		# Check for ids.
		if line.count(">") > 0: 

			# Check if ending seq.
			if len(seq) > 0:
		
				# save.
				data[head] = seq
				
			# reset head.
			head = line.replace(">","")
			seq = ""
			
			# skip to next line.
			continue

		# Filter chars.
		seq += line
		
	# save the last one.
	data[head] = seq
	
	# return dictionary.
	return data


def zeros(shape):
    retval = []
    for x in range(shape[0]):
        retval.append([])
        for y in range(shape[1]):
            retval[-1].append(0)
    return retval

match_award      = 10
mismatch_penalty = -5
gap_penalty      = -15 # both for opening and extanding

def match_score(alpha, beta):
    if alpha == beta:
        return match_award
    elif alpha == '-' or beta == '-':
        return gap_penalty
    else:
        return mismatch_penalty

def finalize(align1, align2):
    align1 = align1[::-1]    #reverse sequence 1
    align2 = align2[::-1]    #reverse sequence 2
    
    i,j = 0,0
    
    #calcuate identity, score and aligned sequeces
    symbol = ''
    found = 0
    score = 0
    identity = 0
    for i in range(0,len(align1)):
        # if two AAs are the same, then output the letter
        if align1[i] == align2[i]:                
            symbol = symbol + align1[i]
            identity = identity + 1
            score += match_score(align1[i], align2[i])
    
        # if they are not identical and none of them is gap
        elif align1[i] != align2[i] and align1[i] != '-' and align2[i] != '-': 
            score += match_score(align1[i], align2[i])
            symbol += ' '
            found = 0
    
        #if one of them is a gap, output a space
        elif align1[i] == '-' or align2[i] == '-':          
            symbol += ' '
            score += gap_penalty
    
    identity = float(identity) / len(align1) * 100
    
    '''
    print 'Identity =', "%3.3f" % identity, 'percent'
    print 'Score =', score
    print align1
    print symbol
    print align2
    '''
    return align1, align2, score


def water(seq1, seq2):
    m, n = len(seq1), len(seq2)  # length of two sequences
    
    # Generate DP table and traceback path pointer matrix
    score = zeros((m+1, n+1))      # the DP table
    pointer = zeros((m+1, n+1))    # to store the traceback path
    
    max_score = 0        # initial maximum score in DP table
    # Calculate DP table and mark pointers
    for i in range(1, m + 1):
        for j in range(1, n + 1):
            score_diagonal = score[i-1][j-1] + match_score(seq1[i-1], seq2[j-1])
            score_up = score[i][j-1] + gap_penalty
            score_left = score[i-1][j] + gap_penalty
            score[i][j] = max(0,score_left, score_up, score_diagonal)
            if score[i][j] == 0:
                pointer[i][j] = 0 # 0 means end of the path
            if score[i][j] == score_left:
                pointer[i][j] = 1 # 1 means trace up
            if score[i][j] == score_up:
                pointer[i][j] = 2 # 2 means trace left
            if score[i][j] == score_diagonal:
                pointer[i][j] = 3 # 3 means trace diagonal
            if score[i][j] >= max_score:
                max_i = i
                max_j = j
                max_score = score[i][j];
    
    align1, align2 = '', ''    # initial sequences
    
    i,j = max_i,max_j    # indices of path starting point
    
    #traceback, follow pointers
    while pointer[i][j] != 0:
        if pointer[i][j] == 3:
            align1 += seq1[i-1]
            align2 += seq2[j-1]
            i -= 1
            j -= 1
        elif pointer[i][j] == 2:
            align1 += '-'
            align2 += seq2[j-1]
            j -= 1
        elif pointer[i][j] == 1:
            align1 += seq1[i-1]
            align2 += '-'
            i -= 1

    return finalize(align1, align2)

def make_key(i, j):
	''' makes key '''
	if i < j:
		return (i, j)
	else:
		return (j, i)

### script. ###

# load the data.
nodes = load_nodes(NODE_FILE)
edges = load_edges(EDGE_FILE)
refs = load_fasta(REF_FILE)

# loop over each entry.
counts = dict()
for edge in edges:
	
	# make key.
	key = make_key(edge['ctg_a_idx'], edge['ctg_b_idx'])

	# add count.
	if key not in counts:
		counts[key] = [0,0,0,0]
	counts[key][edge['implied_state']] += 1
	
	# look for appropriate edge.
	if edge["implied_dist"] > -1 * X_TIMES * STD_DEV:
		continue
	
	# zero this out.
	edge["invalid"] = 1
	
	'''
	# translate ids.
	id_1 = nodes[edge["ctg_a_idx"]]["ctg_name"]
	id_2 = nodes[edge["ctg_b_idx"]]["ctg_name"]
	
	# sanity.
	if id_1 not in refs or id_2 not in refs:
		logging.error("bad match between ref and nodes")
		sys.exit(1)
	
	# fetch sequences.
	seq1 = refs[id_1]
	seq2 = refs[id_2]
	
	# check if we need to RC a seq.
	if edge["read_a_orien"] > 0:
		seq1 = seq1.translate(string.maketrans("ATCGN", "TAGCN"))[::-1]
	
	if edge["read_b_orien"] > 0:
		seq2 = seq2.translate(string.maketrans("ATCGN", "TAGCN"))[::-1]
	'''

	
# build list of high coverage.
to_high = set()
for key in counts:
	bad = False
	for i in [0,1,2,3]:
		if counts[key][i] > MAX_COV:
			bad = True
			break 
			
	if bad == True:
		to_high.add(key)

# invalidate the high coveragers.
for edge in edges:
	
	# make key.
	key = make_key(edge['ctg_a_idx'], edge['ctg_b_idx'])
	
	# test.
	if key in to_high:
		edge["invalid"] = 1
	
# save edges.
save_edges(edges, EDGE_FILE)
