'''
reverse the super contig translation.
'''
# program imports.
from data_structs.agps import load_agps
from data_structs.agps import save_agps
from data_structs import types

# system imports.
import time
import numpy as np
import subprocess
import itertools
import networkx as nx
import logging
import sys
import os
import h5py
import tempfile
from operator import itemgetter
time_start = time.time()

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

#################### parameters ####################

## user-supplied parameters.
new_agp_file = os.path.abspath(sys.argv[1])

agp_files = list()
for i in range(2, len(sys.argv)):
	agp_files.append(os.path.abspath(sys.argv[i]))
agp_files = sorted(agp_files)

#################### classes ####################

#################### functions ####################

def agp_lookup(agp_edges):
	''' build contig lookup '''
			
	# build the lookup and dictionary.
	scaf_lists = dict()
	for e in agp_edges:
		
		if e['scaf_name'] not in scaf_lists:
			scaf_lists[e['scaf_name']] = list()
		scaf_lists[e['scaf_name']].append(e)
	
	# return the two.
	return scaf_lists

def sync_entry(prev_list, cur_edge):
	''' syncs previous list with current edge'''

	# simplify.
	scaf = cur_edge['scaf_name']
	ctg = cur_edge['comp_name']

	# always update the name.
	for i in range(len(prev_list)):
		prev_list[i]['scaf_name'] = scaf
		
	# check if orientation is flipped.
	if cur_edge['comp_orien'] != 0:
		prev_list.reverse()
		
	# update the start/stop ositions.
	scaf_idx = cur_edge['scaf_start']
	for i in range(len(prev_list)):
		
		# check for negative component size.
		if prev_list[i]['scaf_stop'] - prev_list[i]['scaf_start'] < 0:
			logging.error("negative component size")
			print prev_list[i]
			sys.exit(1)
		
		# set the scaf start/stop.
		prev_list[i]['scaf_start'] = scaf_idx
		prev_list[i]['scaf_stop'] = scaf_idx + prev_list[i]['comp_stop'] - prev_list[i]['comp_start']
		scaf_idx = prev_list[i]['scaf_stop'] + 1
	
	# return modified list.
	return prev_list

def get_runtime(agp_file):
	''' finds runtime in AGP file '''
	fin = open(agp_file)
	rt = -1
	for line in fin:
		if line.count("RUNTIME") > 0:
			rt = float(line.strip().split()[1])
			break
	fin.close()
	
	if rt == -1:
		logging.error("can't find runtime")
		sys.exit(1)
	return rt
	
def process_pair(previous_agp_file, current_agp_file, new_agp_file):
	''' loads previous and current '''

	# load the agp edges.
	prev_agp_edges = load_agps(previous_agp_file)
	curr_agp_edges = load_agps(current_agp_file)

	# build dictionary lookup of old agp.
	prev_lookup = agp_lookup(prev_agp_edges)

	# loop over current agp and expand any collapsed entries.
	tmp_edges = list()
	idx_update = dict()
	for e in curr_agp_edges:
		
		# simplify.
		scaf = e['scaf_name']
		ctg = e['comp_name']
		
		# bootstrap idx update.
		if scaf not in idx_update:
			idx_update[scaf] = 1
		
		# skip if not expandable.
		if ctg not in prev_lookup:
			
			# set idx.
			e['scaf_idx'] = idx_update[scaf]
			idx_update[scaf] += 1
			
			# add to list and continue.
			tmp_edges.append(e)
			continue
			
		# process the old list.
		prev_list = sync_entry(prev_lookup[ctg], e)
			
		# sort this and create new object.
		test_list = sorted(prev_list, key=itemgetter(0,1,2))
		
		# test for equality.
		if prev_list != test_list:
			logging.error("bad prev_list")
			for i in range(len(prev_list)):
				print prev_list[i], test_list[i]
			sys.exit(1)
			
		# sanity check this.
		for i in range(1, len(prev_list)):
			
			# check they alternate.
			if prev_list[i]['comp_type'] == "W" and prev_list[i-1]['comp_type'] != "N":
				logging.error("BAD LOGIC fire the programmer 1.")
				for x in prev_list:
					print x
				sys.exit(1)
			
			# check the starts match.
			if prev_list[i]['scaf_start'] <= prev_list[i-1]['scaf_stop']:
				logging.error("BAD LOGIC fire the programmer 2.")
				for x in prev_list:
					print x
				sys.exit(1)
			
		# add to new list.
		for x in prev_list:
			
			# set idx.
			x['scaf_idx'] = idx_update[scaf]
			idx_update[scaf] += 1
			
			# add to list.
			tmp_edges.append(x)

	# create new numpy to store expanded agp.
	new_agp_edges = np.zeros(len(tmp_edges), dtype=types.agp_dt)

	# put each one into numpy array.
	idx = 0
	for e in tmp_edges:
		for n in types.agp_dt.names:
			new_agp_edges[idx][n] = e[n]
		idx += 1
			
	# save the agp to file.
	save_agps(new_agp_file, new_agp_edges)

	# find the previous runtimes.
	rt1 = get_runtime(previous_agp_file)
	rt2 = get_runtime(current_agp_file)

	# append runtime to file.
	txt = ""
	fin = open(new_agp_file)
	for line in fin:
		txt += line
	txt += "RUNTIME:\t%f\n" % (rt1 + rt2)
	fin.close()
	fout = open(new_agp_file, "wb")
	fout.write(txt)
	fout.close()

#################### script ####################

# create temporary file.
temp_file = tempfile.NamedTemporaryFile(mode='w+b', delete=False)

# loop over files in reverse order.
cur_agp = agp_files[0]
for i in range(1, len(agp_files)-1):
	
	# do translation.
	process_pair(agp_files[i+1], cur_agp, temp_file.name)
	
	# update cur.
	cur_agp = temp_file.name
	
# move the tmp.agp to final resting.
subprocess.call(["mv", temp_file.name, new_agp_file])
subprocess.call(["chmod", "a+r", new_agp_file])
