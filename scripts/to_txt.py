'''
finds out number of valid adjacencies at different bundle sizes.
'''
# program imports.
from data_structs.nodes import load_nodes
from data_structs.edges import load_edges
from data_structs.bundles import load_bundles
from data_structs.types import node_dt, edge_dt, bundle_dt


# system imports.
from collections import deque
import random
import time
import numpy as np
import subprocess
import itertools
import networkx as nx
import logging
import sys
import os
import h5py
import tempfile
time_start = time.time()

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

# parameters.
in_file = sys.argv[1]
typeof = sys.argv[2]
out_file = sys.argv[3]

## script.

# switch based on type.
if typeof == "node":
	array = load_nodes(in_file)
	
elif typeof == "edges":
	array = load_edges(in_file)

elif typeof == "bundles":
	array = load_bundles(in_file)

else:
	logging.error("unkown type")
	sys.exit(1)
	
fout = open(out_file, "wb")
for x in array:
	fout.write('\t'.join([str(y) for y in x]) + "\n")
fout.close()
	
