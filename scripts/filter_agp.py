'''
this python script takes and AGP and a list of
valid contigs and removes non-valid contigs.
'''
# program imports.
from data_structs.agps import load_agps
from data_structs.agps import save_agps
from data_structs.types import agp_dt

# imports
import sys
import os
import random
import logging
import numpy as np

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

### parameters ###
IN_FILE = os.path.abspath(sys.argv[1])
VALID_FILE = os.path.abspath(sys.argv[2])
OUT_FILE = os.path.abspath(sys.argv[3])

### functions ###

def strip_gaps(agps):
	''' removes gaps '''
	
	# build scaffold subsets ignoring existing gaps.
	scafs = dict()
	for i in range(agps.shape[0]):
		if agps[i]['scaf_name'] not in scafs:
			scafs[agps[i]['scaf_name']] = list()
			
		if  agps[i]['comp_type'] == "W":
			scafs[agps[i]['scaf_name']].append(i)
		
		
	# count number without gaps.
	new_size = 0
	for skey in scafs:
		new_size += len(scafs[skey])
		
	# create new agps.
	new_agps = np.zeros(new_size, dtype=agp_dt)
	
	# copy contigs and add gaps.
	idx = 0
	for skey in scafs:
		
		scaf_idx = 1
		for i in range(len(scafs[skey])):
			
			j = scafs[skey][i]
			
			# copy existing.
			new_agps[idx] = agps[j]
			new_agps[idx]['scaf_idx'] = scaf_idx
			scaf_idx += 1
			idx += 1
			
	# return them modified agps.
	return new_agps

def add_gaps(agps):
	''' adds gaps to  agp file '''
		
	# build scaffold subsets ignoring existing gaps.
	scafs = dict()
	for i in range(agps.shape[0]):
		if agps[i]['scaf_name'] not in scafs:
			scafs[agps[i]['scaf_name']] = list()
			
		if  agps[i]['comp_type'] == "W":
			scafs[agps[i]['scaf_name']].append(i)
		
	# count number of missing gaps.
	new_size = 0
	for skey in scafs:
		new_size += len(scafs[skey])
		new_size += len(scafs[skey]) - 1
		
	# create new agps.
	new_agps = np.zeros(new_size, dtype=agp_dt)
	
	# fill out new agp.
	idx = 0
	for scaf in scafs.values():
		
		# zero index.
		scaf_idx = 1
		
		# copy data.
		for i in range(len(scaf)):
			
			# copy the contig.
			new_agps[idx] = agps[scaf[i]]
			idx += 1
			scaf_idx += 1
			
			# check if we add gap.
			if scaf[i] != scaf[-1]:
				
				# add gap.
				new_agps[idx]['scaf_name'] = new_agps[idx - 1]['scaf_name']
				new_agps[idx]['scaf_idx'] = scaf_idx
				new_agps[idx]['scaf_start'] = new_agps[idx - 1]['scaf_stop'] + 1
				new_agps[idx]['scaf_stop'] = agps[scaf[i + 1]]['scaf_start'] - 1
				new_agps[idx]['comp_type'] = "N"
				new_agps[idx]['comp_name'] = "gap"
				new_agps[idx]['comp_start'] = 1 
				new_agps[idx]['comp_stop'] = new_agps[idx]['scaf_stop'] - new_agps[idx]['scaf_start']
				new_agps[idx]['comp_orien'] = 0
				idx += 1
				scaf_idx += 1
				
	# return them modified agps.
	return new_agps
			

def gap_split(agps):
	''' splits scaffolds at large gaps'''
	
	# build scaffold subsets ignoring existing gaps.
	scafs = dict()
	for i in range(agps.shape[0]):
		if agps[i]['scaf_name'] not in scafs:
			scafs[agps[i]['scaf_name']] = list()
			
		if  agps[i]['comp_type'] == "W":
			scafs[agps[i]['scaf_name']].append(i)
		
			
	# loop over scaffolds.
	new_scafs = list()
	for skey in scafs:
		
		# bootstrap list.
		cur = list()
		cur.append(scafs[skey][0])
		
		# loop till end.
		for i in range(1, len(scafs[skey])):
			
			# simplify.
			gap = agps[scafs[skey][i]]['scaf_start'] - agps[scafs[skey][i-1]]['scaf_stop']
			
			# check if we add to finalize.
			if gap > INSERT_SIZE + (5.6 * STD_DEV) and len(cur) > 0:
				
				# finalize.
				new_scafs.append(cur)
					
				# reset.
				cur = list()
				
			# just add.
			cur.append(scafs[skey][i])

	# finalize.
	new_scafs.append(cur)
			
	# create new agp array.
	new_agps = np.zeros(agps.shape[0], dtype=agp_dt)
			
	# build new scaffolds...
	scaf_count = 1
	idx = 0
	for scaf in new_scafs:
		
		# reset trackers.
		scaf_name = "scaffold_%i" % scaf_count
		scaf_idx = 1
		scaf_offset = agps[scaf[0]]['scaf_start']
		scaf_count += 1
		
		# start copying to new array.
		for i in scaf:
			
			# do whole copy.
			new_agps[idx]['scaf_name'] = scaf_name
			new_agps[idx]['scaf_idx'] = scaf_idx
			new_agps[idx]['scaf_start'] = agps[i]['scaf_start'] - scaf_offset + 1
			new_agps[idx]['scaf_stop'] = agps[i]['scaf_stop'] - scaf_offset + 1
			new_agps[idx]['comp_type'] = "W"
			new_agps[idx]['comp_name'] = agps[i]['comp_name']
			new_agps[idx]['comp_start'] = agps[i]['comp_start']
			new_agps[idx]['comp_stop'] = agps[i]['comp_stop']
			new_agps[idx]['comp_orien'] = agps[i]['comp_orien']
			
			# update index.
			scaf_idx += 1
			idx += 1
		
	# return new agps.
	return new_agps

def remove_invalid(agps, valid):
	'''	removes bad contigs '''		

	# build list of bad rows.
	bad = list()
	for i in range(agps.shape[0]):
		if agps[i]['comp_name'] not in valid:
			bad.append(i)
	
	# create new array.
	new_agps = np.zeros(len(bad), dtype=agp_dt)
	
	# copy into it.
	idx = 0
	for i in bad:
		new_agps[idx] = agps[i]
		idx += 1
		
	# return agps.
	return new_agps
	

### script ###

# load the agp.
agps = load_agps(IN_FILE)

# load valid into set.
valid = set()
fin = open(VALID_FILE)
for line in fin:
	valid.add(line.strip())
fin.close()

# strip the gaps.
agps = strip_gaps(agps)

# remove the invalid contigs.
agps = remove_invalid(agps, valid)

# add the gaps back in.
agps = add_gaps(agps)

# save it to file.
save_agps(OUT_FILE, agps)
