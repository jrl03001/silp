#!/usr/bin/python
'''
print ace file from annotated ham file.
'''

# program imports
from data_structs.nodes import load_nodes
from data_structs.nodes import create_lookup
from data_structs.edges import load_edges

# system imports.
import sys
import logging
import numpy as np

logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

# parameters.
NODE_FILE = sys.argv[1]
EDGE_FILE = sys.argv[2]
REF_FILE = sys.argv[3]
ACE_FILE = sys.argv[4]
MATES_FILE = sys.argv[5]
MIN_SIZE = int(sys.argv[6])
MAX_SIZE = int(sys.argv[7])

dummy1 = "ACGTTACGTTACGTTACGTTACGTTACGTTACGTTACGTTACGTTACGTT"
dummy2 = "GCCTAGCCTAGCCTAGCCTAGCCTAGCCTAGCCTAGCCTAGCCTAGCCTA"

# load data.
nodes = load_nodes(NODE_FILE)
edges = load_edges(EDGE_FILE)
lookup = create_lookup(nodes)

# start mates file.
fout = open(MATES_FILE, "wb")
fout.write("library\tA\t%i\t%i\n" % (MIN_SIZE, MAX_SIZE))

# find double unique partners.
logging.info("building ace file.")
ace = {}
cnt = 0
namecnt = 0
for edge in edges:
	
	# get data.
	rname1 = nodes[edge['ctg_a_idx']]['ctg_name']
	rname2 = nodes[edge['ctg_b_idx']]['ctg_name']
	
	qname1 = "name1_%i" % namecnt
	qname2 = "name2_%i" % namecnt
	namecnt += 1
	
	pos1 = edge['read_a_left_pos']
	pos2 = edge['read_b_left_pos']
	szseq1 = edge['read_a_right_pos'] - pos1
	szseq2 = edge['read_b_right_pos'] - pos2
	seq1 = 'A' * szseq1
	seq2 = 'A' * szseq2
	
	# swap second orientation always.
	if edge['read_a_orien'] == 0: orien1 = "U"
	else: orien1 = "C"
	if edge['read_b_orien']: orien2 = "U"
	else: orien2 = "C"
	
	# write ace txt.
	AF1 = "AF %s %s %i\n" % (qname1, orien1, pos1)
	RD1 = "RD %s %i 0 0\n" % (qname1, szseq1)
	RD1 += seq1 + "\n\n"
	RD1 += "QA 1 %s 1 %s\n" % (szseq1, szseq1)
	RD1 += "DS VERSION: 1 TIME: Wed Dec 24 11:21:50 2008 CHEM: solexa\n\n"
	
	AF2 = "AF %s %s %i\n" % (qname2, orien2, pos2)
	RD2 = "RD %s %i 0 0\n" % (qname2, szseq2)
	RD2 += seq1 + "\n\n"
	RD2 += "QA 1 %s 1 %s\n" % (szseq2, szseq2)
	RD2 += "DS VERSION: 1 TIME: Wed Dec 24 11:21:50 2008 CHEM: solexa\n\n"
	
	# save to ace structure.
	if rname1 not in ace:
		ace[rname1] = ([], [], [], [])
	ace[rname1][0].append(AF1)
	ace[rname1][1].append(RD1)
#	ace[rname1][2].append(QA1)
#	ace[rname1][3].append(DS1)
	
	if rname2 not in ace:
		ace[rname2] = ([], [], [], [])
	ace[rname2][0].append(AF2)
	ace[rname2][1].append(RD2)
#	ace[rname2][2].append(QA2)
#	ace[rname2][3].append(DS2)

	cnt += 2
	
	# write pair to mates.
	fout.write("%s\t%s\tA\n" % (qname1, qname2))
fout.close()

# print out ace file.
logging.info("writing ace file.")



print "size of ace", len(ace)



fout = open(ACE_FILE, "wb")
fout.write("AS %i %i\n\n" % (len(ace), cnt))

for ctg in ace:
	# build ctg info.
	idx = lookup[ctg]
	szcseq = nodes[idx]['ctg_width']
	cseq = "A" * szcseq
	chits = ace[ctg]
	txt = "CO %s %i %i 0 U\n" % (ctg, szcseq, len(chits))
	txt += cseq + "\n\n"
	txt += "BQ\n"
	txt += "20 " * szcseq + "\n\n"
	
	# write contig info.
	fout.write(txt)
	
	# write read info.
	for e in chits[0]:
		fout.write(e)
	fout.write("\n")
		
	for e in chits[1]:
		fout.write(e)
		

# close h5 files.
fout.close()

