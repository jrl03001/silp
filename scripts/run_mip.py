'''
loads all relavent information and solves ILP
'''

# system imports.
import time
import subprocess
import logging
import sys
import os

#from graphs.oglinear import ScaffLinear
from graphs.Linear import Linear
from data_structs.nodes import load_nodes
from data_structs.nodes import create_lookup
from data_structs.agps import save_agps

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

time_start = time.time()

# hardcoded.

# parameters.
NODE_FILE = sys.argv[1]
COV_FILE = sys.argv[2]
WORK_DIR = sys.argv[3]
AGP_FILE = sys.argv[4]
SAM_FILE = sys.argv[5]
CTG_FILE = sys.argv[6]
BUNDLE_SIZE = int(sys.argv[7])
MIN_SIZE = int(sys.argv[8])
MAX_SIZE = int(sys.argv[9])
INSERT_SIZE = int(sys.argv[10])
PARAM_FILE = sys.argv[11]

MIP_FILE = "%s/scaffolds2.txt" % WORK_DIR

######### functions ########

def load_mip(fpath):
	''' loads mip file into dictionary '''
	
	# read file.
	fin = open(fpath, "rb")
	lines = fin.readlines()
	fin.close()
	
	# make into dict.
	scafs = {}
	for line in lines:
		
		# tokenize.
		tmp = line.split()
		
		# set current scaffold.
		if tmp[0].count("scaffold") > 0:
			# set current.
			curs = tmp[0]
			
			# set default.
			scafs[curs] = []
			continue
			
		# append.
		scafs[curs].append(tmp)
		
	# return result.
	return scafs

def annotate(nodes, lookup, mip):
	''' saves orientation to node '''
	
	# loop over scaffolds.
	for sid in mip:
		
		# loop over contigs.
		for ctg in mip[sid]:
			
			# look up name.
			cid = lookup[ctg[1]]
			
			# set orientation.
			if ctg[2] == "F":
				nodes[cid]['ctg_orien'] = 0
			else:
				nodes[cid]['ctg_orien'] = 1
				
	# return updated file.
	return nodes
	

def make_agp(nodes, lookup, mip):
	''' makes agp structure from mip dict'''

	# create empty linear graph.
	#g = ScaffLinear(None, None, None, nobreak=False, pseudo=True)
	g = Linear(nodes, None)
	
	# manually set node pointer.
	g.node_list = nodes
	
	# add edges to the graph.
	edges = []
	for sid in mip:
		
		# loop over contig.
		pcid = -1
		pend = -1
		for ctg in mip[sid]:
			
			# lookup id.
			cid = lookup[ctg[1]]
			
			# add contig.
			#g.add_node(cid)
			if g.has_node(cid) == False:
				print "Sad"
				sys.exit()
			
			# tokenize.
			start = int(ctg[3])
			end = int(ctg[4])
			
			# see if we add an edge.
			if pcid != -1:
				
				# make an edge.
				edges.append( (pcid, cid, {"gap_size":end - pend, "dist":end-pend} ) )
				
			# update vars.
			pcid = cid
			pend = end
			
	# add edges to graph.
	g.add_edges_from(edges)
	
	# return linear graph.
	return g
	
def write_config(file_path, bundle, len_ins, min_ins, max_ins, sam_path):
	''' write contig'''
	
	txt = '''# Upper bound for genome length (required)
genome_length=5000000000

#parameter specifications for the first stage
[STAGE]
# Maximum biconnected component size. (optional)
#maximum_biconnected_component=50
# Maximum allowed degree in scaffolding graph. (optional)
maximum_degree=50
# Maximum coverage for nonrepetitive contig. (optional)
#maximum_coverage=100
# The maximum overlap between contigs that is allowed without checking for
# sequence similarity. By default this is set based on the variablility in
# insert size lengths of each library. (optional)
#maximum_overlap=100
# The minimum support for an edge. (optional)
minimum_support=%i
# Should edges with negative estimated distance be checked for sequence
# similarity or removed automatically? (optional)
check_negative_edges=1

# library specification for the first stage
[LIBRARY]
# File in SAM format containing mappings for the mate pair reads
# to the contigs
mappings=%s
# Orientation of the mate pairs (in current version must be SOLID)
orientation=SOLID
# Insert length
insert_length=%i
# Minimum insert length
min_insert_length=%i
# Maximum insert length
max_insert_length=%i
''' % (bundle, sam_path, len_ins, min_ins, max_ins)

	
	# write to file.
	fout = open(file_path, "wb")
	fout.write(txt)
	fout.close()


######## script ############

# write config.
write_config(PARAM_FILE, BUNDLE_SIZE, INSERT_SIZE, MIN_SIZE, MAX_SIZE, SAM_FILE)

# execute mip.
cmd = []
cmd.append("/opt/mip-scaffolder/scripts/mip-scaffolder.pl")
cmd.append(PARAM_FILE)
cmd.append(CTG_FILE)
cmd.append(COV_FILE)
cmd.append(WORK_DIR)
#print ' '.join(cmd)
#print WORK_DIR
#sys.exit()
#subprocess.call(cmd, cwd=WORK_DIR)
subprocess.call(cmd)

# load nodes into array.
nodes = load_nodes(NODE_FILE)
lookup = create_lookup(nodes)

# load mip into dictionary.
mip = load_mip(MIP_FILE)

# annotate node array.
nodes = annotate(nodes, lookup, mip)

# make the agp graph.
g = make_agp(nodes, lookup, mip)

# make agps.
agps = g.gen_agp()

# write it to disk.
save_agps(AGP_FILE, agps)

# end of execution.
time_stop = time.time()

# append runtime to agp.
fin = open(AGP_FILE, "rb")
lines = fin.readlines()
fin.close()

lines.append("RUNTIME:\t%f\n" % (time_stop - time_start))

fout = open(AGP_FILE, "wb")
fout.write(''.join(lines))
fout.close()
