'''
runs opera on input.
'''
# program imports.
import biobuffers
from data_structs.nodes import load_nodes
from data_structs.nodes import create_lookup
from data_structs.types import edge_dt

# system imports.
import time
import string
import numpy as np
import subprocess
import logging
import sys
import os
from string import Template

time_start = time.time()

# parameters.
graph_dir = os.path.abspath(sys.argv[1])
output_agp_name = sys.argv[2]
pet_size = int(sys.argv[3])
insert_size = int(sys.argv[4])
std_dev = int(sys.argv[5])

# hardcoded formatting.
time_out = 60 * 5
#time_out = 604800
opera_exe = "/home/jlindsay/src/opera-v1.0/bin/opera"

input_dir = "%s/input" % graph_dir
node_file = "%s/nodes.hdf5" % input_dir

opera_dir = "%s/opera" % graph_dir
opera_input_dir = "%s/input" % opera_dir
opera_results_dir = "%s/results_%i" % (opera_dir, pet_size)
opera_script_dir = "%s/script_%i" % (opera_dir, pet_size)
opera_output_dir = "%s/output_%i" % (opera_dir, pet_size)
opera_working_dir = "%s/working_%i" % (opera_dir, pet_size)
opera_log_dir = "%s/log_%i" % (opera_dir, pet_size)
opera_config_file = "%s/advanced_%i.config" % (opera_script_dir, pet_size)
opera_script_file = "%s/run_%i.sh" % (opera_script_dir, pet_size)
opera_log_file = "%s/opera_%i.log" % (opera_log_dir, pet_size)
opera_node_file = "%s/opera_ctg.fasta" % opera_input_dir
opera_edge_file = "%s/opera_raw.txt" % opera_input_dir
opera_out_file = "%s/scaffolds.scaf" % opera_results_dir

agp_out = "%s/agps/%s.agp" % (graph_dir, output_agp_name)


################# functions ##############

def write_config(opera_config_file, opera_results_dir, opera_node_file, opera_edge_file, pet_size, insert_size, std_dev):
	''' writes config file '''
	txt = """#
# Essential Parameters
#

# Please always supply absolute path of each file,
# Because relative path may not work all the time.

# Output folder for final results
output_folder=${opera_results_dir}

# Contig file
contig_file=${opera_node_file}

# Mapped read locations
map_file=${opera_edge_file}

#----------------------------------------------------------------------------------------------
# Advanced Parameters

#
# Scaffolding related parameters
#

# Scaffold name in result file
scaffold_name=opera_scaffold

# PET cluster threshold (default=5) (Opera will discard all clusters 
# less than this value during scaffolding) 
cluster_threshold=${pet_size}

# Should Opera abort when running time for specific subgraph is longer 
# than 30 minutes (true or false, default=true)
abort=true

#----------------------------------------------------------------------------------------------
#
# Contig file related parameters
#

# Format of contig file (fasta or statistic, default=fasta)
file_format=fasta

# Program name generating contig file (velvet or soap, default=velvet)
file_type=velvet

# Should the repeat contigs be filtered (yes or no, default=true)
filter_repeat=yes

# Repeat threshold (default=1.5): If the coverage of a contig is higher than 
# (repeat threshold * average coverage), then it is considered as repeat
repeat_threshold=1.5

# Contig size threshold (default=500): Opera will not use the contigs whose length 
# is shorter than this value
contig_size_threshold=10

#----------------------------------------------------------------------------------------------
#
# Mapping related parameters
#

# Should Opera recalculate paired-end reads orientation (yes or no, default=no)
calculate_ori=no

# Paired end reads orientation (in, out or forward)
# in: ->...<-;  out: <-...->;   forward: ->...->(first reads are in the second position)
read_ori=in

# Format of mapping file (bowtie or opera, default=bowtie)
#map_type=bowtie

# Should Opera recalculate library information (yes or no, default=yes)
calculate_lib=no

# Library mean length
lib_mean=${lib_mean}

# Library standard deviation
lib_std=${lib_std}
"""
	txt = Template(txt)
	txt = txt.substitute(opera_results_dir=opera_results_dir, opera_node_file=opera_node_file, opera_edge_file=opera_edge_file, pet_size=pet_size, lib_mean=insert_size, lib_std=std_dev)
	
	# write to file.
	fout = open(opera_config_file, "wb")
	fout.write(txt)
	fout.close()
	
def write_script(opera_config_file, opera_script_file, opera_log_file, opera_exe):
	''' writes script '''
	txt = """#!/bin/bash

	${opera_exe} ${opera_config_file} &> ${opera_log_file};
	exit;
	"""
		
	txt = Template(txt)
	txt = txt.substitute(opera_config_file=opera_config_file, opera_log_file=opera_log_file, opera_exe=opera_exe)
	
	# write to file.
	fout = open(opera_script_file, "wb")
	fout.write(txt)
	fout.close()
	
	# make executable.
	subprocess.call(["chmod", "u+x", opera_script_file])
	
def execute_script(opera_script_file, time_out):
	''' executes program. '''
	
	# call script.
	p = subprocess.Popen([opera_script_file])

	# loop till timeout or finished.
	tstart = time.time()
	done = False
	while time.time() - tstart < time_out:
		
		# poll object.
		if p.poll() == None:
			
			# sleep.
			logging.debug(".")
			time.sleep(1)
			
		else:
			# break loop we done.
			done = True
			break
			
def create_dir(file_path):
	''' creates a dir. '''
	subprocess.call(["mkdir",file_path])
			
def create_agp(opera_out_file, agp_out, nodes):
	''' creates agp file from opera output '''
	
	# load results.
	fin = open(opera_out_file, "rb")
	lines = fin.readlines()
	fin.close()

	# create an AGP.
	fout = open(agp_out, "wb")
	for lidx in range(len(lines)):
		# setup line.
		line = lines[lidx]
		
		# check header.
		if line[0] == ">":
			header = line.strip().replace(">","").split()[0]
			#header = line.strip().replace(">","")
			part = 1
			scaf_start = 1
			scaf_stop = 0
			continue
		
		
		# tokenize.
		tmp = line.strip().split()

		# get ctgid.
		#y = int(opfix[tmp[0]])
		y = int(tmp[0])
		
		ctgid = nodes[y]['ctg_name']
		
		if tmp[1] == "BE":
			orien = "+"
		else:
			orien = "-"
			
		ctglen = int(tmp[2])
		gaplen = int(tmp[3])
		
		#print header, "|", scaf_start, "|", scaf_stop, "|", part,  "|","W", ctgid,  "|",1, ctglen,  "|",orien
		#sys.exit()
		
		# make indexs.
		scaf_stop = scaf_start + ctglen
		
		# write out AGP.
		fout.write("%s\t%i\t%i\t%i\t%s\t%s\t%i\t%i\t%s\n" % \
			(header, scaf_start, scaf_stop, part, "W", ctgid, 1, ctglen, orien))
			
		# increment pointers.
		scaf_start = scaf_stop + 1
		part += 1
		
		# add gap if size is not 0
		if gaplen != 0:
			
			# make indexs.
			scaf_stop = scaf_start + gaplen
			
			# add gap.
			fout.write("%s\t%i\t%i\t%i\t%s\t%i\t%s\t%s\n" % \
				(header, scaf_start, scaf_stop, part, "N", gaplen, "fragment", "no"))
				
			# increment pointers.
			scaf_start = scaf_stop + 1
			part += 1
			
		# otherwise.
		else:
			# check if we can peak.
			if lidx+1 >= len(lines):
				continue
			
					
			# check if new scaffold.
			if lines[lidx+1][0] != ">":
				# make fake gap.
				scaf_stop = scaf_start + 10
				
				# add gap.
				fout.write("%s\t%i\t%i\t%i\t%s\t%i\t%s\t%s\n" % \
					(header, scaf_start, scaf_stop, part, "N", gaplen, "fragment", "no"))
					
				# increment pointers.
				scaf_start = scaf_stop + 1
				part += 1			
				continue
			

	# close AGP file.
	fout.close()
	
################# script ##############

# make directories.
for x in [opera_dir, opera_input_dir, opera_results_dir, opera_script_dir, opera_log_dir]:
	if os.path.isdir(x) == False:
		create_dir(x)

# load nodes.
nodes = load_nodes(node_file)

# write config file.
write_config(opera_config_file, opera_results_dir, opera_node_file, opera_edge_file, pet_size, insert_size, std_dev)

# write script.
write_script(opera_config_file, opera_script_file, opera_log_file, opera_exe)

# execute script.
execute_script(opera_script_file, time_out)

# test for existance of opera output.
if os.path.isfile(opera_out_file) == True:

	# create agp.
	create_agp(opera_out_file, agp_out, nodes)

	# end of execution.
	time_stop = time.time()

	# append runtime to agp.
	fin = open(agp_out, "rb")
	lines = fin.readlines()
	fin.close()

	lines.append("RUNTIME:\t%f\n" % (time_stop - time_start))

	fout = open(agp_out, "wb")
	fout.write(''.join(lines))
	fout.close()
