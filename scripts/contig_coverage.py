#!/usr/bin/python
'''
calculates cover based on the following formula.

(read base count) / (contig length)

output is in the following format:
<id>    <name>        <length>        <coverage>

The first entry gives the index of the contig starting from 1.

'''
import sys
import logging
from biobuffers import fasta_buffer
from biobuffers import sam_buffer

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )


# parameters.
contig_file = sys.argv[1]
output_file = sys.argv[2]

sam_files = []
for i in range(3, len(sys.argv)):
	sam_files.append(sys.argv[i])
	
if len(sam_files) < 1:
	logging.error("bad input")
	sys.exit(1)

# load fasta data.
fdb = {}

# loop over fasta file.
logging.debug("indexing fasta file.")
for entry in fasta_buffer(contig_file):
	fdb[entry['name']] = [0, len(entry['seq'])]
	
	
# process sam file.
logging.debug("count sam entries.")
for sam_file in sam_files:
	for entry in sam_buffer(sam_file):
		if entry['RNAME'] not in fdb:
			continue
		fdb[entry['RNAME']][0] += len(entry['QUAL'])


# write out coverage.
fout = open(output_file, "wb")
idx = 1
for name in fdb:
	
	# <id>    <name>        <length>        <coverage>
	fout.write("%i\t%s\t%i\t%f\n" % (idx, name, fdb[name][1], float(fdb[name][0])/float(fdb[name][1])) )

fout.close()
