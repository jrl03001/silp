'''
takes scaffold fasta file, the contig file and
outputs agp file.
'''
import sys
import os
import nwalign

from data_structs.types import agp_dt

### parameters ###
REF_FILE = sys.argv[1]
SCAF_FILE = sys.argv[2]
AGPS_FILE = sys.argv[3]

### functions ###

def load_fasta(file_path):
	''' loads fasta file into dictionary'''
	
	# read file into memory.
	fin = open(file_path)
	lines = fin.readlines()
	fin.close()
	
	# build dictionary.
	data = dict()
	seq = ""
	for line in lines:
		
		# Skip blanks.
		if len(line) < 2: continue
		if line[0] == "#": continue

		# remove blanks.
		line = line.strip()

		# Check for ids.
		if line.count(">") > 0: 

			# Check if ending seq.
			if len(seq) > 0:
		
				# save.
				data[head] = seq
				
			# reset head.
			head = line.replace(">","")
			seq = ""
			
			# skip to next line.
			continue

		# Filter chars.
		seq += line
		
	# save the last one.
	data[head] = seq
	
	# return dictionary.
	return data

def break_scaffolds(scafs):
	''' break scaffolds '''
	
	# break scaffold into contigs.
	broken = dict()
	for key in scafs:
		
		# simplify.
		seq = scafs[key]
		
		# make sure we have lowercase n.
		seq = seq.replace("N","n")
		
		# split up by n.
		tmps = seq.split("n")
		
		# remove empty.
		ctgs = list()
		for x in tmps:
			if len(x) != 0:
				ctgs.append(x)
				
		# add to broken.
		broken[key] = ctgs

	# return new dictionary.
	return broken

### script ###

# load the scaffold and contig files.
refs = load_fasta(REF_FILE)
scafs = load_fasta(SCAF_FILE)

# break into contigs.
broken = break_scaffolds(scafs)

# create the
for x in broken:
	print len(broken[x])
	
# determine the orientations.
#orientations = find_orientations(ctgs, scafs)
