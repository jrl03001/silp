'''
reverses a piece of the edge to accomodate multiple paired end styles.
'''
# program imports.
from data_structs.nodes import load_nodes
from data_structs.edges import load_edges
from data_structs.edges import save_edges
from data_structs.edges import determine_class
from data_structs.edges import determine_dist

# system imports.
from collections import deque
import random
import time
import numpy as np
import subprocess
import itertools
import networkx as nx
import logging
import sys
import os
import h5py
import tempfile
time_start = time.time()

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

# parameters.
NODE_FILE = sys.argv[1]
EDGE_FILE = sys.argv[2]
KEY = sys.argv[3]

# sanity check key.
if KEY != "read_a_orien" and KEY != "read_b_orien":
	logging.error("bad key: %s" % key)
	sys.exit(1)

# load the edges.
nodes = load_nodes(NODE_FILE)
edges = load_edges(EDGE_FILE)

# flip all entries for that key.
for i in range(edges.size):
	
	# simplify.
	ida = edges[i]['ctg_a_idx']
	idb = edges[i]['ctg_b_idx']
	
	# flip orientation of key.
	edges[i][KEY] = 1 - edges[i][KEY]
	
	# determine state.
	state = determine_class(ida, idb, edges[i]['read_a_orien'], edges[i]['read_b_orien'])
	
	# determine dist.
	dist = determine_dist(ida, idb, state, edges[i], nodes[ida], nodes[idb])
	
	# save info.
	edges[i]['implied_state'] = state
	edges[i]['implied_dist'] = dist
	
# save edges.
save_edges(edges, EDGE_FILE)
