# imports.
import numpy as np
from data_structs.types import nlist_dt, blist_dt, tlist_dt


def build_nlist(nodes, active):
	''' build nlist from active offset '''
		
	# allocate array.
	nlist = np.zeros(len(active), dtype=nlist_dt)
	
	# fill array.
	idx = 0
	for i in active:
		nlist[idx]['idx'] = nodes[i]['node_idx']
		idx += 1
	
	# return array.
	return nlist
	
def build_blist(bundles, active):
	''' build blist from active offset '''
	
	# count number of bundles.
	bsz = 0
	bidxs = []
	for i in range(bundles.size):
		if bundles[i]['ctg_a_idx'] in active and bundles[i]['ctg_b_idx'] in active:
			bsz += 1
			bidxs.append(i)
	
	# allocate array.
	blist = np.zeros(bsz, dtype=blist_dt)
	
	# fill array.
	idx = 0
	for i in bidxs:
		blist[idx]['idxa'] = bundles[i]['ctg_a_idx']
		blist[idx]['idxb'] = bundles[i]['ctg_b_idx']
		blist[idx]['WT_A'] = bundles[i]['WT_A']
		blist[idx]['WT_B'] = bundles[i]['WT_B']
		blist[idx]['WT_C'] = bundles[i]['WT_C']
		blist[idx]['WT_D'] = bundles[i]['WT_D']
		idx += 1

	# return array.
	return blist
	
def build_tlist(bundles, active):
	''' build blist from active offset '''
	
	# build dict of neighbors.
	neibs = dict()
	bidxs = []
	for i in range(bundles.size):
		
		# simplify.
		idxa = bundles[i]['ctg_a_idx']
		idxb = bundles[i]['ctg_b_idx']
		
		# skip.
		if idxa not in active or idxb not in active:
			continue
		
		# track it.
		bidxs.append(i)
		
		# bootstrap sets.
		if idxa not in neibs:
			neibs[idxa] = set()
		if idxb not in neibs:
			neibs[idxb] = set()
			
		# add to sets.
		neibs[idxa].add(idxb)
		neibs[idxb].add(idxa)
	
	# find triangles by edge.
	tris = set()
	for i in bidxs:
		
		# simplify.
		idxa = bundles[i]['ctg_a_idx']
		idxb = bundles[i]['ctg_b_idx']
		
		# build intersection.
		isec = neibs[idxa].intersection(neibs[idxb])
		
		# enumerate triangles.
		for idxc in isec:
			
			# make key.
			key = tuple( sorted( [idxa, idxb, idxc] ) )
			
			# insert to triangles.
			tris.add(key)
						
	# build array.
	tlist = np.zeros(len(tris), dtype=tlist_dt)
	
	# populate array.
	idx = 0
	for key in tris:
		tlist[idx]['idxa'] = key[0]
		tlist[idx]['idxb'] = key[1]
		tlist[idx]['idxc'] = key[2]
		idx += 1

	# return array.
	return tlist
