# import networkx.
import subprocess
import networkx as nx
import sys

def load_decomposition(decomp_0_file, decomp_1_file, decomp_2_file):
	''' loads skeleton from decomposition file.'''
	
	# build dictionary of decomposition sets.
	DG = nx.Graph()
	
	# loop over all files.
	for file_path in [decomp_0_file, decomp_1_file, decomp_2_file]:
		
		# loop over all lines.
		fin = open(file_path, "rb")
		for line in fin:
			
			# tokenize.
			line = line.strip().strip(",").split("\t")
			stage_type = int(line[0].replace("stage=",""))
			line_type = line[1].replace("type=","")

			# look for nodes.
			if line_type == "N":
			
				# get index.
				tmp = line[2].replace("idx=","").strip().strip(",").split(",")
				idx0 = int(tmp[0])
				idx1 = -1
				idx2 = -1
				
				if len(tmp) > 1:
					idx1 = int(tmp[1])
				if len(tmp) > 2:
					idx2 = int(tmp[2])

				# build list of active.
				tmp1 = line[3].replace("set=","").strip(",")
				tmp2 = tmp1.split(",")			
				
				# add this set to list.
				tmp = set()
				for x in tmp2:
					tmp.add(int(x))

				# add set to graph.
				if idx0 != -1 and  DG.has_node(idx0) == False:
					DG.add_node(idx0, {'graph':nx.DiGraph(), 'set':set()})

				if idx1 != -1 and DG.node[idx0]['graph'].has_node(idx1) == False:
					DG.node[idx0]['graph'].add_node(idx1, {'graph':nx.DiGraph(), 'set':set()})

				if idx2 != -1 and DG.node[idx0]['graph'].node[idx1]['graph'].has_node(idx2) == False:
					DG.node[idx0]['graph'].node[idx1]['graph'].add_node(idx2, {'graph':None, 'set':set()})
				
				# add set at maximum key size.
				if idx0 != -1 and idx1 != -1 and idx2 != -1:
					DG.node[idx0]['graph'].node[idx1]['graph'].node[idx2]['set'] = tmp
					
				elif idx0 != -1 and idx1 != -1:
					DG.node[idx0]['graph'].node[idx1]['set'] = tmp
					
				elif idx0 != -1:
					DG.node[idx0]['set'] = tmp

			elif line_type == "E":
				
				# get indicies.
				tmp = line[2].replace("idx1=","").strip().strip(",").split(",")
				ida0 = int(tmp[0])
				ida1 = -1
				ida2 = -1
				if len(tmp) > 1:
					ida1 = int(tmp[1])
				if len(tmp) > 2:
					ida2 = int(tmp[2])
					
				tmp = line[3].replace("idx2=","").strip().strip(",").split(",")
				idb0 = int(tmp[0])
				idb1 = -1
				idb2 = -1	
				if len(tmp) > 1:
					idb1 = int(tmp[1])
				if len(tmp) > 2:
					idb2 = int(tmp[2])

				# ge the cut.
				cut = line[4].replace("cut=","").strip().strip(",").split(",")
				for i in range(len(cut)):
					cut[i] = int(cut[i])
				
				# add directed edge.
				if ida2 == -1 and ida1 == -1:
					DG.add_edge(ida0, idb0, {'cut':tuple(cut)})
					
				elif ida2 == -1:
					DG.node[ida0]['graph'].add_edge(ida1, idb1, {'cut':tuple(cut)})
					
				else:
					DG.node[ida0]['graph'].node[ida1]['graph'].add_edge(ida2, idb2, {'cut':tuple(cut)})
				
			elif line_type == "R":
				
				# get index.
				tmp = line[2].replace("idx=","").strip().strip(",").split(",")
				idx0 = int(tmp[0])
				idx1 = -1
				idx2 = -1
				
				if len(tmp) > 1:
					idx1 = int(tmp[1])
				if len(tmp) > 2:
					idx2 = int(tmp[2])
					
				# add the root.
				if idx2 == -1 and idx1 == -1:
					DG.graph['root'] = idx0
					
				elif idx2 == -1:
					DG.node[idx0]['graph'].graph['root'] = idx1
					
				else:
					DG.node[idx0]['graph'].node[idx1]['graph'].graph['root'] = idx2
				
		fin.close()
	
	# clear out non-decomposed entries.
	recurse_clear(DG)
	
	# compute edges.
	#recurse_edgeify(DG)
	
	# compute decomposition paths.
	for n in DG.nodes():
		
		# pathify this node if necessary.
		if DG.node[n]['graph'] != None:
			recurse_paths(DG.node[n]['graph'])
	
	
	# compact the tree recursively.
	for n in DG.nodes():
		if DG.node[n]['graph'] != None:
			DG.node[n]['graph'] = compact_tree(DG.node[n]['graph'])

	# re-compute decomposition paths.
	for n in DG.nodes():
		
		# pathify this node if necessary.
		if DG.node[n]['graph'] != None:
			recurse_paths(DG.node[n]['graph'])

	# return the final object.
	return DG


def compact_tree(G):
	''' compacts tree recursively '''

	# compact deeper graphs.
	for n in G.nodes():
		if G.node[n]['graph'] != None:
			G.node[n]['graph'] = compact_tree(G.node[n]['graph'])
	
	# loop until can't shrink.
	shrunk_cnt = 1
	while shrunk_cnt > 0:
		
		# zero shrunk.
		shrunk_cnt = 0
		
		# create list of edges.
		elist = set(G.edges())

		# loop over each edge.
		while len(elist) > 0:
			
			# pop edge.
			e = elist.pop()
			s = e[0]
			t = e[1]		
			
			# skip root.
			if G.graph['root'] == s: continue
			
			# check if we can try to merge pairs.
			if len(G.node[s]['set']) + len(G.node[t]['set']) < 1500:
				
				# increment shrunk count.
				shrunk_cnt += 1
				
				# merge target into source.
				G.node[s]['set'] = G.node[s]['set'].union(G.node[t]['set'])
				
				# remove edge from source to target.
				G.remove_edge(s, t)
				
				# hookup targets successors to source.
				for c in G.successors(t):
					
					# get cut.
					cut = G[t][c]['cut']
					
					# remove existing edge.
					G.remove_edge(t, c)
					
					# add new edge.
					G.add_edge(s, c, {'cut':cut})
					
					# remove this edge from active set.
					if (t,c) in elist:
						elist.remove((t,c))

	return G
	
def recurse_paths(G):
	''' replaces each graph with a directed version '''
	
	# create nlist.
	nlist = G.nodes()
		
	# digg deeper for each node.
	for n in nlist:
			
		# check if we add graph.
		if G.node[n]['graph'] != None:
			recurse_paths(G.node[n]['graph'])
					
	# build the edge lists.
	G.graph['bottom'] = dfs_bottom(G, G.graph['root'])
	G.graph['top'] = dfs_top(G, G.graph['root'])	

	
def recurse_edgeify(G):
	''' adds edges to graph based on overlap '''
	
	# create nlist.
	nlist = G.nodes()
	
	# loop over nodes of graph.
	for i in range(len(nlist)):
		
		# check if this requires further edgeification.
		if G.node[nlist[i]]['graph'] != None:
			recurse_edgeify(G.node[nlist[i]]['graph'])
			
		# look for edges.
		for j in range(i+1, len(nlist)):
			
			# check for intersection.
			inter = G.node[nlist[i]]['set'].intersection(G.node[nlist[j]]['set'])
			
			if len(inter) > 2:
				print "SAAAD"
			
			# if intersection annotate it and add edge.
			if len(inter) > 0:
				G.add_edge(nlist[i], nlist[j], {'cut': inter})
				

def dfs_bottom(G, root):
	''' builds edge list from bottom'''
		
	# get the predecessor dict.
	preds = nx.dfs_predecessors(G, root)
	
	# build the node list.
	nlist = [x for x in nx.dfs_postorder_nodes(G, root)]
	
	# build the edge list.
	elist = list()
	for n in nlist:
		if n in preds:
			elist.append( (n, preds[n]) )
		else:
			elist.append( (n, -1) )
	
	# return the list.
	return elist
	
		
def dfs_top(G, root):
	''' builds edge list from bottom'''
		
	# get the predecessor dict.
	preds = nx.dfs_predecessors(G, root)
	
	# build the node list.
	nlist = [x for x in nx.dfs_preorder_nodes(G, root)]
	
	# build the edge list.
	elist = list()
	for n in nlist:
		if n in preds:
			elist.append( (n, preds[n]) )
		else:
			elist.append( (n, -1) )
	
	# return the list.
	return elist
		
def recurse_clear(G):
	for n in G.nodes():
		if G.node[n]['graph'] != None:
			if len(G.node[n]['graph'].nodes()) == 0:
				G.node[n]['graph'] = None
			else:
				recurse_clear(G.node[n]['graph'])
