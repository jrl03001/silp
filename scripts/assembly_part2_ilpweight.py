'''
solves scaffolding problem using ILP
on pairs only. Then weighted bi-partite matching
to find the paths.

It then prints the AGP without gap finding or neting.
'''
# program imports.
from data_structs.nodes import load_nodes
from data_structs.nodes import create_lookup
from data_structs.edges import load_edges
from data_structs.edges import save_edges
from data_structs.weights import load_weights
from data_structs.bundles import load_bundles
from data_structs.agps import load_agps
#from data_structs.agps import save_agps
from data_structs import types

from optimize.ilp_pair_cplex import SpqrIlp
from optimize.ilp_pair_cplex import BiConstraints
from optimize.matching_cplex import MatchingIlp

from optimize.ilp_pair_cplex import nlist_dt
from optimize.ilp_pair_cplex import blist_dt
from optimize.ilp_pair_cplex import tlist_dt
from optimize.solution_pair import SpqrSolution
from optimize.solution_pair import PartialSolution

from graphs.Directed import Directed
from graphs.Flow import Flow
from graphs.Linear import Linear

from gap_estimate.average import gap_avg
from gap_estimate.minlike import min_like

from scaflib.tree_functions import *
from scaflib.heirarchy_functions import *
from scaflib.data_functions import *
from scaflib.misc_functions import *


# system imports.
from collections import deque
import random
import time
import numpy as np
import subprocess
import itertools
import networkx as nx
import logging
import sys
import os
import h5py
import tempfile
time_start = time.time()

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

# parameters.
base_dir = os.path.abspath(sys.argv[1])
output_agp_file = sys.argv[2]
valid_edge_file = sys.argv[3]

# hardcoded formatting.
input_dir = "%s/input" % base_dir
input_nodes_file = "%s/nodes.hdf5" % input_dir
input_edges_file = "%s/edges.hdf5" % input_dir
input_bundles_file = "%s/bundles.hdf5" % input_dir

graph_dir = "%s/graphs" % base_dir
decomp_0_file = "%s/decomp_zero.txt" % graph_dir
decomp_1_file = "%s/decomp_one.txt" % graph_dir
decomp_2_file = "%s/decomp_two.txt" % graph_dir

serial_dir = "%s/serial" % base_dir
directed_graph_file = "%s/directed.gpickle.gz" % serial_dir
linear_graph_file = "%s/linear.gpickle.gz" % serial_dir

agp_dir = "%s/agps" % base_dir
cplex_log_file = "%s/logs/cplex.log" % base_dir
cplex_err_file = "%s/logs/cplex.err" % base_dir

int_dir = "%s/intermediate" % base_dir

#################### classes ####################
		
#################### functions ####################
		

def save_agps(agp_file, agp):
	''' saves agp to disk.'''
	
	# write to file.
	fout = open(agp_file, "w")
	
	# write each entry.
	z = len(types.agp_dt.names)
	for i in range(agp.size):
		
		# format result.
		tmp = agp[i]
		if tmp['comp_type'] == "W":
			# get orientation.
			if tmp["comp_orien"] == 0:
				o = "+"
			else:
				o = "-"
				
			# write contig.
			txt = str(tmp['scaf_name']) + "\t"
			txt += str(tmp['scaf_start']) + "\t"
			txt += str(tmp['scaf_stop']) + "\t"
			txt += str(tmp['scaf_idx']) + "\t"
			txt += str(tmp['comp_type']) + "\t"
			txt += str(tmp['comp_name']) + "\t"
			txt += str(tmp['comp_start']) + "\t"
			txt += str(tmp['comp_stop']) + "\t"
			txt += o + "\n"
			
		else:
			# get linkage.
			if tmp['comp_linkage'] == 0:
				o = "no"
			else:
				o = "yes"
			
			# write gap.
			txt = str(tmp['scaf_name']) + "\t"
			txt += str(tmp['scaf_start']) + "\t"
			txt += str(tmp['scaf_stop']) + "\t"
			txt += str(tmp['scaf_idx']) + "\t"
			txt += str(tmp['comp_type']) + "\t"
			txt += str(tmp['comp_stop'] - tmp['comp_start']) + "\t"
			txt += str(tmp['comp_name']) + "\t"
			txt += o + "\n"
							
		fout.write(txt)
		
	# close file.
	fout.close()

		
def ilp_matching(FG, ilp_obj):
	''' uses ILP to solve matching, returns edge list'''
	
	# load the constraints.
	ilp_obj.load("weight", FG)
	
	# solve it.
	elist, val = ilp_obj.solve()

	# clear it.
	ilp_obj.clear()
	
	# return the edge list.
	return elist
		
def create_agps(LG, nodes):
	''' creates agp for assembly '''

	# grab some info.
	nsz = LG.number_of_nodes()
	esz = LG.number_of_edges()
	tsz = nsz + esz
	
	# create AGP array.
	agps = np.zeros(tsz, dtype=types.agp_dt)
	
	# locate connected components.
	components = nx.weakly_connected_components(LG)
	
	# loop over components.
	scaf_idx = 0
	row_idx = 0
	for component in components:
		
		# create subgraph.
		subg = nx.DiGraph()
		subg.add_nodes_from(component)
		
		# add edges.
		for edge in LG.edges(component):
			subg.add_edge(edge[0],edge[1],dist=LG[edge[0]][edge[1]]['dist'])
		
		# compute topological order.
		path = nx.topological_sort(subg)
		
		# validate path.
		pn = path[0]
		for i in range(1,len(path)):
			if subg.has_edge(pn, path[i]) == False:
				logging.error("Not a linear graph.")
				sys.exit()
			pn = path[i]
			
		# loop over entries in component.
		comp_idx = 1
		offset = 1
		cur_gaps = 0
		max_gaps = len(subg.nodes()) - 1
		
		# loop over the group.
		pn = -1
		for n in path:
			
			# compute difference.
			diff = 0
			if pn != -1:
				diff = subg[pn][n]['dist']
				
			
			# copy info.
			agps[row_idx]['scaf_name'] = "scaffold_%i" % scaf_idx
			agps[row_idx]['scaf_start'] = offset + diff
			agps[row_idx]['scaf_stop'] = agps[row_idx]['scaf_start'] + nodes[n]['ctg_width']
			agps[row_idx]['scaf_idx'] = comp_idx
			agps[row_idx]['comp_type'] = "W"
			agps[row_idx]['comp_name'] = nodes[n]['ctg_name']
			agps[row_idx]['comp_start'] = 1
			agps[row_idx]['comp_stop'] = nodes[n]['ctg_width']
			agps[row_idx]['comp_orien'] = LG.node[n]['orien']
			agps[row_idx]['comp_linkage'] = 0
			
			# increment info.
			offset = agps[row_idx]['scaf_stop']
			row_idx += 1
			comp_idx += 1
			pn = n
			
			# add gap if appropriate.
			if cur_gaps < max_gaps:
				agps[row_idx]['scaf_name'] = "scaffold_%i" % scaf_idx
				agps[row_idx]['scaf_start'] = -1
				agps[row_idx]['scaf_stop'] = -1
				agps[row_idx]['scaf_idx'] = comp_idx
				agps[row_idx]['comp_type'] = "N"
				agps[row_idx]['comp_name'] = "edge"
				agps[row_idx]['comp_start'] = -1
				agps[row_idx]['comp_stop'] = -1
				agps[row_idx]['comp_orien'] = -1
				agps[row_idx]['comp_linkage'] = 0				
				row_idx += 1 
				comp_idx += 1
				cur_gaps += 1
		
		# increment scaf info.
		scaf_idx += 1
		
	# return the AGPs.
	return agps

		
########### script ################## 

# load hdf5 information.
logging.info("loading data arrays")
nodes = load_nodes(input_nodes_file)
edges = load_edges(input_edges_file)
bundles = load_bundles(input_bundles_file)
node_lookup = create_lookup(nodes)

# load the directed graph.
logging.info("loading pickled graphs")
TG = nx.read_gpickle(directed_graph_file)
LG = nx.read_gpickle(linear_graph_file)

# write out edges in directed graph.
fout = open(valid_edge_file, "wb")
for e in TG.edges():
	fout.write("%s\t%s\n" % (nodes[e[0]]['ctg_name'],nodes[e[1]]['ctg_name']))
fout.close()
	
sys.exit()

# building graphs.
logging.info("building graphs.")

# loop over eachs subgraph.
logging.info("solving for paths:")
ilp_obj = MatchingIlp(cplex_log_file, cplex_err_file)
cnt = 0
for subg in TG.comps():
	
	# info.
	if cnt % 10 == 0:
		logging.info("%i" % (cnt))
	cnt += 1
	
	# create the bipartite flow graph.
	FG = Flow(subg)
	
	# calculate path.
	#elist = FG.pathify()
	#elist = call_lemon(FG)
	elist = ilp_matching(subg, ilp_obj)
	
	# add to linear graph.
	LG.create_elist(elist)
	
	#logging.info(".")


# remove cycles.
logging.info("removing cycles")
LG.remove_cycles()

# end of execution.
time_stop = time.time()

# find gap estimates.
logging.info("adding gap estimates")
gap_ests = gap_avg(nodes, edges, bundles, {})

# apply them.
LG.set_gaps(gap_ests)

# create the AGP edges.
agp_edges = create_agps(LG, nodes)


# save the agp to file.
save_agps(output_agp_file, agp_edges)


# append runtime to agp.
fin = open(output_agp_file, "rb")
lines = fin.readlines()
fin.close()

lines.append("RUNTIME:\t%f\n" % (time_stop - time_start))

fout = open(output_agp_file, "wb")
fout.write(''.join(lines))
fout.close()

logging.info("COMPLETED!")
	
	

