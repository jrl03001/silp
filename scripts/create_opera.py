'''
driver script to create input
for scaffolding algorithm.
'''
# program imports.
from data_structs.nodes import load_nodes
from data_structs.nodes import create_lookup
from data_structs.types import edge_dt

# system imports.
import string
import numpy as np
import subprocess
import logging
import sys
import os

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

# parameters.
NODE_FILE = os.path.abspath(sys.argv[1])
FASTA_FILE = os.path.abspath(sys.argv[2])
SAM_1_FILE = os.path.abspath(sys.argv[3])
SAM_2_FILE = os.path.abspath(sys.argv[4])
OPERA_DIR = os.path.abspath(sys.argv[5])
FLIP_MODE = bool(sys.argv[6])
KEY_TRIM = int(sys.argv[7])

# hardcoded formatting.
opera_input_dir = "%s/input" % OPERA_DIR
opera_results_dir = "%s/results" % OPERA_DIR
opera_script_dir = "%s/script" % OPERA_DIR
opera_output_dir = "%s/output" % OPERA_DIR
opera_log_dir = "%s/log" % OPERA_DIR
opera_node_file = "%s/opera_ctg.fasta" % opera_input_dir
opera_edge_file = "%s/opera_raw.txt" % opera_input_dir

########### functions ##################


def load_fasta(file_path):
	''' loads fasta file into dictionary'''
	
	# read file into memory.
	fin = open(file_path)
	lines = fin.readlines()
	fin.close()
	
	# build dictionary.
	data = dict()
	seq = ""
	for line in lines:
		
		# Skip blanks.
		if len(line) < 2: continue
		if line[0] == "#": continue

		# remove blanks.
		line = line.strip()

		# Check for ids.
		if line.count(">") > 0: 

			# Check if ending seq.
			if len(seq) > 0:
		
				# save.
				data[head] = seq
				
			# reset head.
			head = line.replace(">","")
			seq = ""
			
			# skip to next line.
			continue

		# Filter chars.
		seq += line
		
	# save the last one.
	data[head] = seq
	
	# return dictionary.
	return data


def create_dir(file_path):
	''' creates a dir. '''
	subprocess.call(["mkdir",file_path])
	
def build_nodes(nodes, seqs, out_path):
	''' creates reference dir.'''
	
	# open the output.
	fout = open(out_path, "wb")
	
	# loop over fasta entries.
	for i in range(nodes.shape[0]):
		
		# simplify.
		idx = nodes[i]['node_idx']
		seq = seqs[nodes[i]['ctg_name']]
		
		# save info to list.
		fout.write(">%i\n%s\n" % (idx, seq)) 

	# close output.
	fout.close()
	
def build_edges(node_lookup, sam_file1, sam_file2, file_path, flip_mode, key_trim):
		
	# open output.
	fout = open(file_path, "wb")
	
	# read in valid SAM entries.
	j = 0
	fin1 = open(sam_file1, "rb")
	fin2 = open(sam_file2, "rb")
	for line1 in fin1:
	
		# merge lines and tokenize.
		line2 = fin2.readline()

		# tokenize lines.
		tmp1 = line1.split("\t")
		tmp2 = line2.split("\t")

		# check dictionary for contig id.
		if tmp1[2] not in node_lookup:
			continue
			logging.error("contig missing in node file: %s" % tmp1[2])
			sys.exit()

		if tmp2[2] not in node_lookup:
			continue
			logging.error("contig missing in node file: %s" % tmp2[2])
			sys.exit()
						
		# get idx.
		idx1 = node_lookup[tmp1[2]]
		idx2 = node_lookup[tmp2[2]]

		# parse orientation.
		if tmp1[1] == "0":  o1 = "+"
		else:			   o1 = "-"
		
		if tmp2[1] == "0":  o2 = "+"
		else:			   o2 = "-"   
		
		# fake --> <-- style reads from SOLiD --> -->
		if flip_mode == True:
			if o2 == "+":
				o2 = "-"
			else:
				o2 = "+"
			
		# reverse compliment the flipped reads.
		#tmp2[9] = tmp2[9].translate(string.maketrans("ATCG", "TAGC"))[::-1]
		#tmp2[10] = tmp2[10][::-1]
		
		# save names.
		nm1 = tmp1[0][0:-key_trim] + ".1"
		nm2 = tmp2[0][0:-key_trim] + ".2"

		# create txt.
		line1 = "%s\t%s\t%s\t%i\t%s\t%s\t%s\n" % (nm1, o1, idx1, int(tmp1[3]), tmp1[9], tmp1[10], "0")
		line2 = "%s\t%s\t%s\t%i\t%s\t%s\t%s\n" % (nm2, o2, idx2, int(tmp2[3]), tmp2[9], tmp2[10], "0")
		j += 1
		
		
		# write to file.
		fout.write(line1)
		fout.write(line2)
	 
	# sanitfy check
	if j < 100:
		logging.error("bad edge creation, probably bad keys")
		sys.exit()
	 
	# close read files.
	fout.close()
	fin1.close()
	fin2.close()

########### script ##################

# create input dirs.
for x in [OPERA_DIR, opera_input_dir, opera_results_dir, opera_script_dir, opera_log_dir]:
	if os.path.isdir(x) == False:
		create_dir(x)

# load the data.
nodes = load_nodes(NODE_FILE)
lookup = create_lookup(nodes)
seqs = load_fasta(FASTA_FILE)

# build fasta file.
build_nodes(nodes, seqs, opera_node_file)

# build edges file.
build_edges(lookup, SAM_1_FILE, SAM_2_FILE, opera_edge_file, FLIP_MODE, KEY_TRIM)
