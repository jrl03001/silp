#!/usr/bin/python
'''
filters stdin stream of SAM files:
saving the repetative SAM hits as gff
saving the unique hits as SAM

each contig is represented as a bitarray and
any position covered by a repetative read
is flagged as 1 in the bit array.

'''
# imports.
import sys
import re
import numpy as np
from bitarray import bitarray
import subprocess

# parameters.
size_file = sys.argv[1]
sam_file = sys.argv[2]
gff_file = sys.argv[3]
tmp_file = sys.argv[4]


######### functions.

def read_sizes(size_file):
	''' reads contig sizes '''
	
	# create numpy datatype.
	ctgsz_dt = np.dtype([('size',np.int), ('name','S255')])
	
	# read in the file.
	sizes = np.loadtxt(size_file, dtype=ctgsz_dt)
	
	# return array.
	return sizes
	
def create_dict(sizes):
	''' creates bit array dictionary '''
	
	# create dict.
	bit_dict = {}
	
	# loop over each contig.
	for entry in sizes:
		
		# create array.
		tmp = bitarray(entry['size'])
		
		# initialize to zero.
		tmp[:] = False
		
		# save to dictionary.
		bit_dict[entry['name']] = tmp
		
	# return result.
	return bit_dict
	
	
def process_sam(bit_dict, tmp_file):
	''' identifies multiple mapped reads from stdin '''
	
	# declare sets.
	waiting = dict()
	bad = set()
	
	# open output for the sam.
	fout = open(tmp_file, "wb")
	
	# loop over stdin.
	mark_bad = False
	prev_sam = ""
	cur_read = ""
	in_print = False
	idx = 0
	for line in sys.stdin:
				
		# tokenize and skip.
		if line[0] == "@": 
			continue
		tmp = line.strip().split()
		if tmp[2] == "*": 
			continue
		
		# write line.
		fout.write(line)
		
		# more token.
		qname = tmp[0]
		rname = tmp[2]
		start = int(tmp[3])
		stop = start + len(tmp[9])
		
		# check if we need to clear out waiting.
		if qname in waiting:
			
			# annotate waiting entry.
			info = waiting[qname][0]
			bit_dict[ info[0] ][ info[1]:info[2] ] = True
			
			# clear out waiting.
			del waiting[qname]
			
			# add rname to bad.
			bad.add(qname)
			
		# check if rname is in bad.
		if qname in bad:
			
			# annotate bad.
			bit_dict[rname][start:stop] = True
			
			# skip the final part.
			idx += 1
			continue
		
		# add this to waiting.
		waiting[qname] = ((rname, start, stop), idx)
		idx += 1
		
	# close output.
	fout.close()
		
	# return anotated guys.
	return bit_dict, waiting
	
def create_annotation(bit_dict, out_file):
	''' processes bitarrays and makes gff '''
	
	# open output.
	fout = open(out_file, "wb")
	
	# loop over each contig.
	for ctg in bit_dict:
		
		# simplify to string.
		bitarr = bit_dict[ctg].to01()
		
		# find idx of all 1s.
		starts = [match.start() for match in re.finditer(re.escape("1"), bitarr)]
		
		# process this into gff.
		phit = -1
		in_hit = False
		for hit in starts:
				if hit == phit + 1:
						in_hit = True
				else:   
						# check if in run.
						stop = phit + 1
						if in_hit == True:
								fout.write("%s\t%s\t%s\t%i\t%i\t%s\t%s\t%s\t%s\n" % (ctg, ".", ".", start,stop,".",".",".","read_repeat"))
								in_hit = False
						start = hit
				phit = hit
		
		
				
		# process last guy.
		if len(starts) > 0:
			if in_hit == True:
				# save stop.
				stop = phit + 1
				
				# print entry.
				fout.write("%s\t%s\t%s\t%i\t%i\t%s\t%s\t%s\t%s\n" % (ctg, ".", ".", start,stop,".",".",".","read_repeat"))
		

	# close output.
	fout.close()
	
def create_sam(waiting, sam_file, tmp_file):
	''' prints unique hits tof ile'''
	
	
	# create idx list.
	idx_list = []
	for e in waiting.values():
		idx_list.append(e[1])
	idx_list.sort()
	
	# open.
	fout = open(sam_file, "wb")
	fin = open(tmp_file, "rb")
	
	# loop over entries.
	idx1 = 0
	idx2 = 0
	sz = len(idx_list)
	for line in fin:
		
		# check if we print.
		if idx1 == idx_list[idx2]:
			fout.write(line)
			idx2 += 1
			
		# increment idx1.
		idx1 += 1
		
		# break check.
		if idx2 >= sz:
			break
		
	# close.
	fin.close()
	fout.close()
	
	# delete tmp.
	subprocess.call(["rm", "-f", tmp_file])

	
######### program.

# read the contig sizes.
sizes = read_sizes(size_file)

# create bit dictionary.
bit_dict = create_dict(sizes)

# process input.
bit_dict, waiting = process_sam(bit_dict, tmp_file)

# create annotations.
create_annotation(bit_dict, gff_file)

# create sam file.
create_sam(waiting, sam_file, tmp_file)

