#!/bin/bash
# executes decomposition after uncapping recursion limit.

# uncap.
ulimit -s unlimited

# get the directory of this script.
cdir="$(pwd)"
sdir="$( dirname "${BASH_SOURCE[0]}" )"

# switch to that dir.
cd $sdir

# call decomposition.
../bin/decompose $1 $2 $3 $4 || exit 1;

# go back.
cd $cdir
