'''
solves scaffolding problem using ILP
on pairs only. Then weighted bi-partite matching
to find the paths.
'''
# program imports.
from data_structs.nodes import load_nodes
from data_structs.nodes import create_lookup
from data_structs.edges import load_edges
from data_structs.edges import save_edges
from data_structs.weights import load_weights
from data_structs.bundles import load_bundles
from data_structs.agps import load_agps
from data_structs.agps import save_agps
from data_structs import types

from optimize.ilp_pair_cplex import SpqrIlp
from optimize.ilp_pair_cplex import BiConstraints
from optimize.matching_cplex import MatchingIlp

from optimize.ilp_pair_cplex import nlist_dt
from optimize.ilp_pair_cplex import blist_dt
from optimize.ilp_pair_cplex import tlist_dt
from optimize.solution_pair import SpqrSolution
from optimize.solution_pair import PartialSolution

from graphs.Directed import Directed
from graphs.Flow import Flow
from graphs.Linear import Linear

from gap_estimate.average import gap_avg
from gap_estimate.minlike import min_like

from scaflib.tree_functions import *
from scaflib.heirarchy_functions import *
from scaflib.data_functions import *
from scaflib.misc_functions import *


from munkres import Munkres

# system imports.
from collections import deque
import random
import time
import numpy as np
import subprocess
import itertools
import networkx as nx
import logging
import sys
import os
import h5py
import tempfile
time_start = time.time()

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

# parameters.
base_dir = os.path.abspath(sys.argv[1])
output_agp_file = sys.argv[2]
previous_agp = sys.argv[3]

# hardcoded formatting.
input_dir = "%s/input" % base_dir
#input_nodes_file = "%s/nodes.txt" % input_dir
#input_edges_file = "%s/edges.txt" % input_dir
#input_bundles_file = "%s/bundles.txt" % input_dir

input_nodes_file = "%s/nodes.hdf5" % input_dir
input_edges_file = "%s/edges.hdf5" % input_dir
input_bundles_file = "%s/bundles.hdf5" % input_dir


graph_dir = "%s/graphs" % base_dir
decomp_0_file = "%s/decomp_zero.txt" % graph_dir
decomp_1_file = "%s/decomp_one.txt" % graph_dir
decomp_2_file = "%s/decomp_two.txt" % graph_dir

agp_dir = "%s/agps" % base_dir
cplex_log_file = "%s/logs/cplex.log" % base_dir
cplex_err_file = "%s/logs/cplex.err" % base_dir

int_dir = "%s/intermediate" % base_dir

#################### classes ####################
		
class IlpSol(object):
	''' interfaces with ILP to solve '''
	
	def __init__(self, nodes, bundles, prev_sol=False):
		
		# save pointer to source data.
		self._nodes = nodes
		self._bundles = bundles
		self._prev_sol = prev_sol
		
		if prev_sol == False:
			self._prev_sol = Solution(set())
		
		# create an instance of ILP_pair_cplex
		self._ilp_obj = SpqrIlp(cplex_log_file, cplex_err_file)
		
	
	def ilp(self, s0, frozen, objmods):
		''' solves using ILP interface '''
		
		# fake it.
		#return self._fake_solve(s0, frozen)

		# create a solution.
		sol = Solution(s0)
		
		# check if this solution will be invalid by a previous solution.
		nogo = False
		for n in frozen:
			if self._prev_sol.get_presence(n) == True:
				if frozen[n] != self._prev_sol.get_orientation(n):
					nogo = True
					break
					
					
		# force to be what previous is or zero.
		if nogo != False:
			for n in s0:
				sol.set_orientation(n, 0)
			sol.set_val(-10000.9999)
			return sol
		
		# create the input for ILP.
		nlist = build_nlist(self._nodes, s0)
		blist = build_blist(self._bundles, s0)
		tlist = build_tlist(self._bundles, s0)
		
		# prepare the ILP instance.
		self._ilp_obj.load(nlist, blist, tlist, sol)
		
		# add the forced constraints.
		for n in frozen:
			
			# sanity check frozen.
			if n in s0:				
				self._ilp_obj.force_one(n, frozen[n])
				
				
		# add the previous constraints.
		for n in self._prev_sol._o:
			if n in s0:
				self._ilp_obj.force_one(n, self._prev_sol.get_orientation(n))
				
		# add the objective modifications.
		for cut, vals in objmods:
			
			# sanity check.
			if cut == None:
				continue
				
			# sanitcy check.
			for x in cut:
				if x not in s0:
					logging.error("cut is not appropriate")
					sys.exit(1)
			
			# check size.
			sz = len(cut)
			if sz == 1:
				self._ilp_obj.child_one(cut, vals)
			elif sz == 2:
				self._ilp_obj.child_two(cut, vals)
			else:
				logging.error("cut is too damn large!")
				sys.exit(1)
			
		
		# logging.
		if len(s0) > 1000:
			logging.info("solving large component...")
		
		# call the ILP object.
		sol = self._ilp_obj.solve()

		if len(s0) > 1000:
			logging.info("...done")
		
		# sanity check.
		for n in self._prev_sol._o:
			if sol.get_presence(n) == True:
				if sol.get_orientation(n) != self._prev_sol.get_orientation(n):
					logging.error("hey your constraints are wrong... bozo")
					sys.exit(1)
		
		# clear the ILP.
		self._ilp_obj.clear()
				
		# return the pair.
		return sol
	
	def _fake_solve(self, s0, frozen):
		''' solves the instance '''
			
		# create a solution.
		sol = Solution(s0)
		
		# set the forced the values.
		for idx in frozen:
			sol.set_orientation(idx, frozen[idx])
			
		# randomly choose the remaining.
		for x in s0:
			if x not in frozen:
				sol.set_orientation(x, random.randint(0,1))
				#sol.set_orientation(x, 0)
				
		# return solution.
		return sol
	

class Solution(object):
	''' holds the solution '''
	
	def __init__(self, s0):
		'''sets up solution from a set.'''
		
		# copy the set and use as tracker.
		self._to_solve = set()
		for s in s0:
			self._to_solve.add(s)
			
		# track added edges.
		self._edges = set()
			
		# create empty orientation dictionary.
		self._o = dict()
		
		# create empty path dictionary.
		self._p = list()
		
		# zero objective.
		self._val = None
		
	def set_orientation(self, idx, val):
		''' sets orientation '''
		
		# sanity check right component.
		if idx not in self._to_solve:
			if idx in self._o and self._o[idx] != val:
				logging.error("tried to set node twice with diff values: %i %i %i" % (idx, val, self._o[idx]))
				sys.exit(1)
			return
			
		# solve it.
		self._o[idx] = val
		
		# remove from to solve.
		if idx in self._to_solve:
			self._to_solve.remove(idx)
		
	def get_presence(self, idx):
		''' checks if its solved '''
		if idx in self._o:
			return True
		else:
			return False
		
	def get_orientation(self, idx):
		''' gets orientation '''
		
		# sanity check right component.
		if idx not in self._o:
			logging.error("can't get this bad boi")
			sys.exit(1)
			
		# return it.
		return self._o[idx]
		
	def set_path(self, idxa, idxb, val):
		''' sets a path '''
		
		# sanity check the both have orien set '''
		if idxa not in self._o or idxb not in self._o:
			logging.error("setting path before orientation set")
			sys.exit(1)
			
		# sanity check we haven't already added this edge.
		key1 = (idxa, idxb)
		key2 = (idxb, idxa)
		
		'''
		if key1 in self._edges or key2 in self._edges:
			logging.error("adding edge more than once. impossibru\n%s" % str(key1))
			sys.exit(1)
		'''
		
		# sanity check value.
		x = 0
		if val == 'A':
			x += 1
		elif val == 'B':
			x += 1
		elif val == 'C':
			x += 1
		elif val == 'D':
			x += 1
		elif val == 'S':
			x += 1
			
		if x == 0:
			logging.error("value was set as a number somewhere?")
			err = xtz
			fg = "^^^"

		
		# simply add directed edge to list.
		self._p.append((idxa, idxb, val))
		
		# add the edge to tracker.
		self._edges.add(key1)
		self._edges.add(key2)
	
	def set_val(self, val):
		''' objective value'''
		self._val = val
		
	def get_val(self):
		''' return objective'''
		
		# sanity.
		if self._val == None:
			logging.error("value was not set")
			sys.exit(1)
		return self._val

class DecompSolve(object):
	''' solves ILP based on decomposition files '''
	
	def __init__(self, nodes, bundles, DG, sol_obj):
		''' sets tracking variables and starts solving '''
		
		# save variables.
		self._nodes = nodes
		self._bundles = bundles
		self._DG = DG
		self._complete_set = set(list(nodes[:]['node_idx']))
		self._sol_obj = sol_obj
				
		# start decomposition.
		self._zero(DG)

	def _zero(self, G):
		''' bootstraps the solution process ''' 

		# build the final solution.
		csol = Solution(self._complete_set)

		# loop over large nodes.
		for n in G.nodes():
			
			# skip n.
			if G.node[n]['graph'] == None:
				
				# get the set.
				s0 = G.node[n]['set']
				
				# switch based on size.
				if len(s0) > 1:
					
					# if its non-trivial use ILP.
					sol = self._sol_obj.ilp(G.node[n]['set'], dict(), list())
				else:
					
					# other wise just set it.
					sol = Solution(s0)
					for x in s0:
						
						# check if forced.
						if self._sol_obj._prev_sol.get_presence(x) == True:
							sol.set_orientation(x, self._sol_obj._prev_sol.get_orientation(x))
						else:
							sol.set_orientation(x, 0)
				
				# save to master.
				for x in sol._o:
					csol.set_orientation(x, sol._o[x])
				for x, y, z in sol._p:
					csol.set_path(x, y, z)
				
			else:
				# loop into this to solve.
				sol = self._solve(G.node[n]['graph'], dict(), 1)
			
				# save to master.
				for x in sol._o:
					csol.set_orientation(x, sol._o[x])
				for x, y, z in sol._p:
					csol.set_path(x, y, z)

				
		# save the solution.
		self._solution = csol
		
	def _solve(self, G, frozen, level):
		''' solves the decomposition '''

		# get the paths we follow.
		bpath = G.graph['bottom']
		tpath = G.graph['top']
		
		# track the nodes in this graph.
		nset = set()
		
		# bottom up solution.
		for p, parent in bpath:
			
			# get active set.
			s0 = G.node[p]['set']
			g0 = G.node[p]['graph']
			
			# add to nset.
			nset = nset.union(s0)
			
			# just solve root.
			if parent == -1:
				
				# check for recursion.
				if g0 != None:
					sol = self._solve(g0, frozen, level+1)
				else:
					
					# look up children.
					objmod = []
					for child in G.successors(p):
						ccut = G[p][child]['cut']
						objmod.append( (cut, G.node[child]['sols'][ccut]) )
					
					# now solve.
					sol = self._sol_obj.ilp(s0, frozen, objmod)
				
				# save root solution.
				G.node[p]['root_sol'] = sol
			
			else:
				# check for intersection.
				cut = G[parent][p]['cut']
				cutsz = len(cut)
				
				# loop over all possible values of cut.
				for x in itertools.product([0,1], repeat=cutsz):
					
					# compute new frozen.
					nfrozen = frozen.copy()
					for i in range(len(cut)):
						if cut[i] not in nfrozen:
							nfrozen[cut[i]] = x[i]
					
					# look up children.
					objmod = []
					for child in G.successors(p):
						ccut = G[p][child]['cut']
						objmod.append( (cut, G.node[child]['sols'][ccut]) )
										
					# solve given values.
					if g0 != None:
						sol = self._solve(g0, nfrozen, level+1)
					else:
						sol = self._sol_obj.ilp(s0, nfrozen, objmod)
							
					# save the solution to the node.
					if 'sols' not in G.node[p]:
						G.node[p]['sols'] = dict()
						
					if cut not in G.node[p]['sols']:
						G.node[p]['sols'][cut] = dict()
						
					G.node[p]['sols'][cut][x] = sol
					
					# sanity check solution.
					for x in sol._o:
						if x not in s0:
							logging.error("solved node shouldn't be hurr")
							sys.exit(1)
					for x, y, z in sol._p:
						if x not in s0 or y not in s0:
							logging.error("solved edge node shouldn't be hurr")
							sys.exit(1)
			
					
		# build up the complete solution object.
		csol = Solution(nset)
			
		# top down application.
		for p, parent in tpath:
			
			# get active set.
			s0 = G.node[p]['set']
			
			# just solve root.
			if parent == -1:
				
				# sanity.
				if G.node[p]['root_sol'] == None:
					logging.error("you chose an incompatible solution: root")
					sys.exit(1)
				
				# just apply the solution.
				solo = G.node[p]['root_sol']._o
				for x in solo:
					csol.set_orientation(x, solo[x])	
				for x, y, z in sol._p:
					csol.set_path(x, y, z)
					
			else:
				
				# get the cut.
				cut = G[parent][p]['cut']
				cutsz = len(cut)				
				
				# build cut values.
				vals = []
				for x in cut:
					vals.append(csol.get_orientation(x))
				vals = tuple(vals)
				
				# sanity.
				if G.node[p]['sols'][cut][vals] == None:
					logging.error("you chose an incompatible solution: not root")
					sys.exit(1)
				
				# apply the appropriate solution.
				solo = G.node[p]['sols'][cut][vals]._o
				solp = G.node[p]['sols'][cut][vals]._p
				for x in solo:
					csol.set_orientation(x, solo[x])	
				for x, y, z in solp:
					#print p, parent, " -- ", x, y, solo, solp, s0
					csol.set_path(x, y, z)			
									
		# return solution.
		return csol		

#################### functions ####################

def call_agp_gaps(agp_file, nodes):
	''' calls agp gaps'''
	
	# create node lookup.
	lookup = create_lookup(nodes)
	
	# load the agp array.
	agp_edges = load_agps(agp_file)
	
	# ensure sorted by scaffname and scafidx.
	agp_edges.sort(order=['scaf_name','scaf_idx'])
	
	# build list of component offsets.
	offsets = dict()
	for i in range(agp_edges.size):

		# skip non contigs.
		if agp_edges[i]['comp_type'] != "W": continue
		
		# record index.
		if agp_edges[i]['scaf_name'] not in offsets:
			offsets[agp_edges[i]['scaf_name']] = list()
		offsets[agp_edges[i]['scaf_name']].append(i)
		
	# add bundle info to this.
	gaps = dict()
	for key in offsets:
		
		# loop over edges.
		for i in range(len(offsets[key]) - 1):
			
			# get AGP edge.
			ea = agp_edges[offsets[key][i]]
			eb = agp_edges[offsets[key][i+1]]
			
			# get index.
			idxa = lookup[ea['comp_name']]
			idxb = lookup[eb['comp_name']]
			
			# get gap.
			gaps[(idxa,idxb)] = eb['scaf_start'] - ea['scaf_stop']
				
	return gaps

def sol_from_agp(agps, node_lookup):
	''' creates a solution object from AGP'''
	
	# identify non singelton scaffolds.
	tcnts = dict()
	for agp in agps:
		
		# count appearences.
		if agp['scaf_name'] not in tcnts:
			tcnts[agp['scaf_name']] = 0
		tcnts[agp['scaf_name']] += 1
		
	# build set of solved objects.
	s0 = set()
	for agp in agps:
		# skip gaps.
		if agp['comp_type'] != "W": continue
		
		# skip simple.
		if tcnts[agp['scaf_name']] <= 1: continue
		
		# get node.
		s0.add(node_lookup[agp['comp_name']])
		
	# create solution.
	sol = Solution(s0)

	# add orientation to solution.
	for agp in agps:
		# skip gaps.
		if agp['comp_type'] != "W": continue
				
		# get index.
		idx = node_lookup[agp['comp_name']]
		
		# skip simple.
		if tcnts[agp['scaf_name']] <= 1: continue
		if idx not in s0: continue
		
		# add the orientation.
		sol.set_orientation(idx, agp['comp_orien'])
		
	# add edges to the solution.
	for i in range(agps.size):
		# skip gaps.
		if agps[i]['comp_type'] != "N": continue
		
		# skip simple.
		if tcnts[agps[i]['scaf_name']] <= 1: continue	
		
		# get edge.
		id1 = node_lookup[agps[i-1]['comp_name']]
		id2 = node_lookup[agps[i+1]['comp_name']]
		
		# add edge.
		sol.set_path(id1, id2, "S")
		
	return sol
			
def call_lemon(PG):
	''' finds minimum weight using LEMON'''
	
	# open temporary node and edge file.
	temp_n = tempfile.NamedTemporaryFile(mode='w+b', delete=False)
	temp_e = tempfile.NamedTemporaryFile(mode='w+b', delete=False)
	temp_o = tempfile.NamedTemporaryFile(mode='w+b', delete=False)
	
	# write nodes to file.
	for n in PG.nodes():
		temp_n.write("%s\n" % n)
		
	# write edges to file.
	for e in PG.edges():
		temp_e.write("%s\t%s\t%i\n" % (e[0], e[1], PG[e[0]][e[1]]['weight']))
		
	# close the file.
	temp_n.close()
	temp_e.close()
	temp_o.close()
	
	# execute lemon code.
	cmd = ["/home/jlindsay/git/SINAH/SINAH/bin/lemon", temp_n.name, temp_e.name, temp_o.name]
	#print ' '.join(cmd)
	#sys.exit()
	
	ret = subprocess.call(cmd)
	
	# sanity check.
	if ret != 0:
		logging.error("error during matching")
		sys.exit(1)

	# load the results.
	elist = list()
	fin = open(temp_o.name, "rb")
	for line in fin:
		
		# tokenize.
		tmp = sorted(line.strip().split())
		
		
		# sanity.
		if tmp[0].count("left") != 1 or tmp[1].count("right") != 1:
			logging.error("bad matching: %s" % line)
		
		# get indexes.
		nl = int(tmp[0].replace("left_", ""))
		nr = int(tmp[1].replace("right_", ""))
			
		# add to list.
		elist.append( (nr, nl) )
		
	# clear temporary files.
	os.unlink(temp_n.name)		
	os.unlink(temp_e.name)
	os.unlink(temp_o.name)
	
	# return list.
	return elist
	
def ilp_matching(FG, ilp_obj):
	''' uses ILP to solve matching, returns edge list'''
	
	# load the constraints.
	ilp_obj.load("card", FG)
	
	# solve it.
	elist, val = ilp_obj.solve()

	# clear it.
	ilp_obj.clear()
	
	# load again for weighted.
	ilp_obj.load("mcmw", FG, card_val=val)

	# solve it.
	elist, val = ilp_obj.solve()	
	
	# clear it.
	ilp_obj.clear()
	
	# return the edge list.
	return elist
			
########### script ################## 

# make sure output dir is there.
create_dir(agp_dir)

# load hdf5 information.
logging.info("loading data arrays")
nodes = load_nodes(input_nodes_file)
edges = load_edges(input_edges_file)
bundles = load_bundles(input_bundles_file)
node_lookup = create_lookup(nodes)

# load any previous solution.
if previous_agp != "-":
	
	# load edges.
	prev_agps = load_agps(previous_agp)
	
	# build a solution object from this.
	prev_sol = sol_from_agp(prev_agps, node_lookup)

# load decomposition.
logging.info("loading decomposition")
DG = load_decomposition(decomp_0_file, decomp_1_file, decomp_2_file)

# create the solver object.
if previous_agp == "-":
	solver = IlpSol(nodes, bundles)
else:
	solver = IlpSol(nodes, bundles, prev_sol)

# create/execute decomposition solve.
decomp = DecompSolve(nodes, bundles, DG, solver)

# verify solution.
solution = decomp._solution
logging.info("verifying solution completion")
for idx in nodes[:]['node_idx']:
	
	# check orientation.
	solution.get_orientation(idx)

# verify integrity of solution w.r.t previous.
if previous_agp != "-":
	
	# compare orientation.
	for n in prev_sol._o:
		if solution.get_presence(n) == True:
			if solution.get_orientation(n) != prev_sol.get_orientation(n):
				logging.error("error in reconciling previous solution orientation")
				print n, solution.get_orientation(n)
				print n, prev_sol.get_orientation(n)
				sys.exit(1)
		else:
			solution.set_orientation(n, prev_sol.get_orientation(n))
	
	# add edges.
	for x, y, z in prev_sol._p:
		solution.set_path(x, y, z)	
			

# create a directed graph from solution.
TG = Directed(nodes, edges, bundles)
TG.create_edges(solution)

# create a linear graph.
LG = Linear(nodes, edges)
LG.set_orientation(solution)

# building graphs.
logging.info("building graphs.")

# loop over eachs subgraph.
logging.info("solving for paths:")
ilp_obj = MatchingIlp(cplex_log_file, cplex_err_file)
cnt = 0
for subg in TG.comps():
	
	# info.
	if cnt % 10 == 0:
		logging.info("%i" % (cnt))
	cnt += 1
	
	# create the bipartite flow graph.
	FG = Flow(subg)
	
	# calculate path.
	#elist = FG.pathify()
	#elist = call_lemon(FG)
	elist = ilp_matching(subg, ilp_obj)
	
	# add to linear graph.
	LG.create_elist(elist)
	
	#logging.info(".")

# see if we need to load agp edges.
logging.info("adding agp gaps")
prev_gaps = dict()
if previous_agp != "-":
	prev_gaps = call_agp_gaps(previous_agp, nodes)

# remove cycles.
logging.info("removing cycles")
LG.remove_cycles()

# find gap estimates.
logging.info("adding gap estimates")
gap_ests = gap_avg(nodes, edges, bundles, prev_gaps)
#gap_ests = min_like(nodes, node_lookup, edges, bundles, LG, prev_gaps)

# apply them.
LG.set_gaps(gap_ests)

# end of execution.
time_stop = time.time()

# verify it.
LG.verify()

# write agp.
agp_edges = LG.gen_agp()
save_agps(output_agp_file, agp_edges)
#LG.write_agp(output_agp_file)

# append runtime to agp.
fin = open(output_agp_file, "rb")
lines = fin.readlines()
fin.close()

lines.append("RUNTIME:\t%f\n" % (time_stop - time_start))

fout = open(output_agp_file, "wb")
fout.write(''.join(lines))
fout.close()

logging.info("COMPLETED!")
	
	

