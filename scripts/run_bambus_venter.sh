#!/bin/bash
# runs bambus.

### parameters ###
NODE_FILE=$1
EDGE_FILE=$2
REF_FILE=$3
WORK_DIR=$6
MIN_SIZE=$7
MAX_SIZE=$8
FIN_AGP=$9
BSIZE=$10


MAT_FILE=${WORK_DIR}/bambus.mates
ACE_FILE=${WORK_DIR}/bambus.ace
AFG_FILE=${WORK_DIR}/bambus.afg
BNK_FILE=${WORK_DIR}/bambus.bnk
SCF_FILE=${WORK_DIR}/scaffold
AGP_FILE=${SCF_FILE}.agp
TIM_FILE=${WORK_DIR}/timing.txt

### functions ###
bambus_meat (){
	# $1 = AFG_FILE
	# $2 = BNK_FILE
	# $3 = SCF_FILE
	# $4 = TIM_FILE
	# $5 = BSIZE
	
	# setup timing.
	fmt="%S";
	
	# add to bank file.
	bank-transact -cf -b $2 -m $1
	clk -b $2
	
	# bundle it.
	/usr/bin/time -f "$fmt" -o ${4}.1 Bundler -b $2
	
	# scaffold it.
	/usr/bin/time -f "$fmt" -o ${4}.2 OrientContigs -b $2 -pref $3 -redundancy ${BSIZE} -noreduce

}

### script ### 

# make ace.
#python /home/jlindsay/central/code/SILP/scripts/edge_ace.py $NODE_FILE $EDGE_FILE $REF_FILE $ACE_FILE $MAT_FILE $MIN_SIZE $MAX_SIZE

# create input.
#toAmos -o $AFG_FILE -ace $ACE_FILE -m $MAT_FILE

# call and time the function.
bambus_meat $AFG_FILE $BNK_FILE $SCF_FILE $TIM_FILE $BSIZE

# find total time.
cat ${TIM_FILE}.1 ${TIM_FILE}.2 > ${TIM_FILE}
runtime=$(awk 'BEGIN{tot=0}{tot+=$0}END{print tot}' ${TIM_FILE})

# make scaffold prettier.
grep -v "#" $AGP_FILE | awk 'BEGIN{FS="\t"}{if($5=="W"){printf("scaffold_%d\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",$1,$2,$3,$4,$5,$6,$7,$8,$9);} else {printf("scaffold_%d\t%s\t%s\t%s\tN\t%s\t%s\t%s\t%s\t%s\n",$1,$2,$3,$4,$5,$6,$7,$8,$9);}}' > x;
mv -f x $FIN_AGP;
echo -e "RUNTIME:\t${runtime}" >> $FIN_AGP
