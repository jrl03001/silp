'''
provides function to estimate gap size by average.
'''
import networkx as nx
import numpy as np
import logging
import sys

def _est_dist(nodea, nodeb, edge):
	''' estimates the distance '''
	
	# simplify.
	ctg_a_sz = nodea['ctg_width']
	ctg_b_sz = nodeb['ctg_width']
	
	aid = edge['ctg_a_idx']
	bid = edge['ctg_b_idx']
	ao = edge['read_a_orien']
	bo = edge['read_b_orien']
	al = edge['read_a_left_pos']
	ar = edge['read_a_right_pos']
	bl = edge['read_b_left_pos']
	br = edge['read_b_right_pos']
	
	# arrange contig size.
	if aid < bid:
		asz = ctg_a_sz
		bsz = ctg_b_sz
	else:
		asz = ctg_b_sz
		bsz = ctg_a_sz					

	# is this type one.
	da = db = None
	if aid < bid:
		
		# determine happyness class.
		if ao == 0 and bo == 0:
			da = asz - ar
			db = bl
		elif ao == 0 and bo == 1:
			da = asz
			db = bsz - br
		elif ao == 1 and bo == 0:
			da = al
			db = bl
		elif ao == 1 and bo == 1:
			da = al
			db = bsz - br
		
	# no its type two.
	else:
		# determine happyness class.
		if ao == 1 and bo == 1:
			da = al
			db = bsz - br
		elif ao == 0 and bo == 1:
			da = asz - ar
			db = bsz - br
		elif ao == 1 and bo == 0:
			da = al
			db = bl
		elif ao == 0 and bo == 0:
			da = asz - ar
			db = bl

	# return it
	return float(edge['insert_size'] - da - db)
	

def min_like(nodes, node_lookup, edges, bundles, LG, prev_gaps):
	''' computes gap estimate '''
	
	# determine how many bundles have paths.
	blist = []
	for i in range(bundles.size):
		if nx.has_path(LG, bundles[i]['ctg_a_idx'], bundles[i]['ctg_b_idx']) == True:
			blist.append(i)
	bsz = len(blist)
			
	# allocate data structures.
	c = np.zeros(bsz, dtype=np.float)
	G = np.zeros(bsz, dtype=np.float)
			
	# compute c array (total length of contigs spanned by each edge)
	idx = 0
	for i in blist:
		
		# simplify.
		id1 = bundles[i]['ctg_a_idx']
		id2 = bundles[i]['ctg_b_idx']
		
		# retrieve shortest path.
		path = nx.shortest_path(LG, source=id1, target=id2)
		
		print LG.node[id1]
		
		# count bases between.
		c[i] = 0.0
		for n in path:
			c[i] += float(nodes[n]['ctg_width'])
		
		
	print c
	
