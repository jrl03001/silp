'''
translates a scaffold into a contig for further scaffolding
'''
# program imports.
from data_structs.nodes import load_nodes
from data_structs.nodes import save_nodes
from data_structs.nodes import create_lookup
from data_structs.edges import load_edges
from data_structs.edges import save_edges
from data_structs.edges import determine_class, determine_dist
from data_structs.agps import load_agps
from data_structs import types

# system imports.
import time
import numpy as np
import subprocess
import itertools
import networkx as nx
import logging
import sys
import os
import h5py
import tempfile
time_start = time.time()

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

#################### parameters ####################

## user-supplied parameters.
og_node_file = os.path.abspath(sys.argv[1])
og_edge_file = os.path.abspath(sys.argv[2])
og_agp_file = os.path.abspath(sys.argv[3])
work_dir = os.path.abspath(sys.argv[4])

## hard-coded working.
input_dir = "%s/input" % work_dir
agps_dir = "%s/agps" % work_dir
graphs_dir = "%s/graphs" % work_dir
logs_dir = "%s/logs" % work_dir

nodes_file = "%s/nodes.hdf5" % input_dir
edges_file = "%s/edges.hdf5" % input_dir
bundles_file = "%s/bundles.hdf5" % input_dir

#################### classes ####################

#################### functions ####################

def process_agps(agp_edges):
	''' build contig lookup and bundled agp edges '''
	
	# count each hit.
	scaf_cnts = dict()
	for e in agp_edges:
		if e['scaf_name'] not in scaf_cnts:
			scaf_cnts[e['scaf_name']] = 0
		scaf_cnts[e['scaf_name']] += 1
		
	# build the lookup and dictionary.
	ctg_to_scaf = dict()
	scaf_lists = dict()
	for e in agp_edges:
		if scaf_cnts[e['scaf_name']] > 1:
			if e['comp_type'] == "W":
				
				# build lookup.
				ctg_to_scaf[nlookup[e['comp_name']]] = e['scaf_name']
			
				# build list.
				if e['scaf_name'] not in scaf_lists:
					scaf_lists[e['scaf_name']] = list()
				scaf_lists[e['scaf_name']].append(e)
	
	# return the two.
	return ctg_to_scaf, scaf_lists

def new_nodes(nodes, ctg_to_scaf, scaf_lists):
	''' creates new array of nodes '''
	
	# create the node lookup.
	nlookup = create_lookup(nodes)
	
	# count numebr of removed edges.
	remove_cnt = 0
	for key in scaf_lists:
		remove_cnt += len(scaf_lists[key])
	
	# build new node array.
	idx_map = dict()
	ng_nodes = np.zeros(nodes.size - remove_cnt + len(scaf_lists), dtype=types.node_dt)

	# populate new array with non-scaffolded.
	idx = 0
	for i in range(nodes.size):
		
		# copy non-scaffolded directly.
		if nodes[i]['node_idx'] not in ctg_to_scaf:
			idx_map[nodes[i]['node_idx']] = idx
			ng_nodes[idx] = nodes[i]
			ng_nodes[idx]['node_idx'] = idx
			idx += 1
			
	# add the scaffolds to the.
	for key in scaf_lists:
		
		# copy info.
		ng_nodes[idx]['node_idx'] = idx
		ng_nodes[idx]['ctg_name'] = key
		ng_nodes[idx]['ctg_width'] = scaf_lists[key][-1]['scaf_stop'] - 1
		ng_nodes[idx]['ctg_orien'] = -1
		ng_nodes[idx]['ctg_order'] = -1
		ng_nodes[idx]['invalid'] = 0
		
		# make idx map.
		for a in scaf_lists[key]:
			idx_map[nodes[nlookup[a['comp_name']]]['node_idx']] = idx
		
		idx += 1
		
	# return the nodes.
	return ng_nodes, idx_map

def new_edges(nodes, ng_nodes, ng_nlookup, edges, ctg_to_scaf, agp_lookup, idx_map):
	''' computes new edges given compressed nodes '''
		
	
	# loop over all edges.
	for i in range(edges.size):
		
		# loop over each of id pairs to be changed in the edge struct line.
		for key1, key2, key3, key4 in zip(
			['ctg_a_idx', 'ctg_b_idx'],
			['read_a_orien', 'read_b_orien'],
			['read_a_left_pos', 'read_b_left_pos'],
			['read_a_right_pos', 'read_b_right_pos'],
		):
		
			# simplify.
			idx = edges[i][key1]
			ctg = nodes[idx]['ctg_name']
			
			# fix index.
			edges[i][key1] = idx_map[edges[i][key1]]
		
			# check if contig is part of scaffold.
			if idx in ctg_to_scaf:

				# simplify.
				nidx = ng_nlookup[ctg_to_scaf[idx]]
				
				# update id.
				edges[i][key1] = nidx
				
				# check if ctg was flipped.
				if agp_lookup[ctg]['comp_orien'] > 0:
					
					# flip the orientation of map.
					edges[i][key2] = 1 - edges[i][key2]
					
				# update positions.
				edges[i][key3] = agp_lookup[ctg]['scaf_start'] + edges[i][key3]
				edges[i][key4] = agp_lookup[ctg]['scaf_start'] + edges[i][key4]

		
		
	# fix update distances.
	to_remove = 0
	for i in range(edges.size):
		
		# simplify.
		aid = edges[i]['ctg_a_idx']
		bid = edges[i]['ctg_b_idx']				
		
		# find state and distance.
		aid = edges[i]['ctg_a_idx']
		bid = edges[i]['ctg_b_idx']
		ao = edges[i]['read_a_orien']
		bo = edges[i]['read_b_orien']
		state = determine_class(aid, bid, ao, bo)
		
		# skip same contig edges.
		if aid != bid:
			dist = determine_dist(aid, bid, state, edges[i], ng_nodes[aid], ng_nodes[bid])
		else:
			dist = -1
			to_remove += 1
			
		# set info.
		edges[i]['implied_state'] = state
		edges[i]['implied_dist'] = dist
		
	# allocate new edge array removing samesies.
	ng_edges = np.zeros(edges.size - to_remove, dtype=types.edge_dt)
	
	# copy all info over.
	idx = 0
	for i in range(edges.size):
		
		# skip bad.
		if edges[i]['implied_dist'] == -1:
			continue
		
		# copy.
		ng_edges[idx] = edges[i]
		idx += 1
		
	# return the new edges.
	return ng_edges
		
		
		
#################### script ####################

# load the nodes and edges.
nodes = load_nodes(og_node_file)
edges = load_edges(og_edge_file)
nlookup = create_lookup(nodes)

# load the agp edges.
agp_edges = load_agps(og_agp_file)

# process agp.
ctg_to_scaf, scaf_lists = process_agps(agp_edges)

# compute the new nodes.
ng_nodes, idx_map = new_nodes(nodes, ctg_to_scaf, scaf_lists)

# create new lookup.
ng_nlookup = create_lookup(ng_nodes)

# create an agp lookup.
agp_lookup = dict()
for e in agp_edges:
	agp_lookup[e['comp_name']] = e

# compute the new edges.
ng_edges = new_edges(nodes, ng_nodes, ng_nlookup, edges, ctg_to_scaf, agp_lookup, idx_map)

# ensure we have an input dir.
if os.path.isdir(input_dir) == False:
	subprocess.call(["mkdir", input_dir])

# save the new nodes and edges.
save_nodes(ng_nodes, nodes_file)
save_edges(ng_edges, edges_file)
