'''
computes hypergraph decomposition 
'''
# program imports.
from data_structs.nodes import load_nodes
from data_structs.nodes import create_lookup
from data_structs.bundles import load_bundles
from data_structs import types

from scaflib.misc_functions import *

# system imports.
from collections import deque
import random
import time
import numpy as np
import subprocess
import itertools
import networkx as nx
import logging
import sys
import os
import h5py
time_start = time.time()

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

# parameters.
base_dir = os.path.abspath(sys.argv[1])

# hardcoded formatting.
input_dir = "%s/input" % base_dir
input_nodes_file = "%s/nodes.txt" % input_dir
input_edges_file = "%s/edges.txt" % input_dir
input_bundles_file = "%s/bundles.txt" % input_dir

graph_dir = "%s/graphs" % base_dir
decomp_0_file = "%s/decomp_zero.txt" % graph_dir
decomp_1_file = "%s/decomp_one.txt" % graph_dir
decomp_2_file = "%s/decomp_two.txt" % graph_dir

hyper_graph = "%s/hyper.txt" % graph_dir

#################### classes ####################


#################### functions ####################

def bundle_graph(bundles):
	''' creates bundle graph from bundles'''
	
	# create graph.
	G = nx.Graph()
	
	# create edge list.
	elist = list()
	for e in bundles:
		elist.append((e['ctg_a_idx'], e['ctg_b_idx']))
	
	# add to graph.
	G.add_edges_from(elist)
	
	# return graph.
	return G
			
def write_hyper(G, file_path):
	''' writes hyper graph to file.'''
	
	# create translator.
	t1 = dict()
	t2 = dict()
	i = 0
	for n in G.nodes():
		t1[n] = i
		t2[i] = n
		i += 1
	
	
	# open file.
	fout = open(file_path, "wb")
	
	# write header.
	fout.write("%i %i\n" % (G.number_of_nodes(), G.number_of_edges()))
	
	# write out constraints.
	for e in G.edges():
		fout.write("2 %i %i\n" % (t1[e[0]], t1[e[1]]))
	
	# close file.
	fout.close()
	
	# return translator.
	return t2
			
def execute_decomposition(file_path):
	''' uses treed to decompose '''
	
	# call it.
	subprocess.call(["/home/jlindsay/src/TreeDecomposer/examples/treed/treed", file_path, "0", "funkpants"])
	
	# read the d0t graph.
	G = nx.read_dot("funkpants.d0t")
	
	draw("able", G)
	print G.nodes()
	print G.edges()
	
	# read it.
	sys.exit()
			
########### script ################## 

# load hdf5 information.
logging.info("loading data arrays")
nodes = load_nodes(input_nodes_file)
bundles = load_bundles(input_bundles_file)
node_lookup = create_lookup(nodes)

#DG = load_decomposition(decomp_0_file, decomp_1_file, decomp_2_file)

# create a simple undirected graph.
BG = bundle_graph(bundles)

# create list of connected components.
comps = nx.connected_component_subgraphs(BG)

# write out hyper graph.
for G in comps:
	
	# write hyper graph.
	t = write_hyper(G, hyper_graph)
	
	# run it.
	execute_decomposition(hyper_graph)
	sys.exit()
	
