#!/bin/bash
# maps fragments against genome.

# parameters.
contig=$1
contigsz=$2
index_file=$3
read=$4
qual=$5
sam=$6
gff=$7
tmp=$8

# location of sam process script.
process=/home/jlindsay/central/code/SPQR_scaffold/process_sam.py

## execute map and pipeline.
bowtie -C -f -p 20 --sam -k 100 -Q $qual $index_file $read | $process $contigsz $sam $gff $tmp
