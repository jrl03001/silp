import sys
from collections import defaultdict
import math
import string

class temp(object):
   def __init__(self):
    self.read_size = 0 
    self.read_bases = ''
    self.seq_id = 0 



def implied_state(aid, bid, ao, bo):
    ''' determines class of edges '''
    
    # check if type 1
    if aid < bid:
            
        # determine happyness class.
        if ao == 0 and bo == 0:
            type = 0
        elif ao == 0 and bo == 1:
            type = 1
        elif ao == 1 and bo == 0:
            type = 2
        elif ao == 1 and bo == 1:
            type = 3
            
    # no its type two.
    else:
        
        # determine happyness class.
        if ao == 1 and bo == 1:
            type = 0
        elif ao == 0 and bo == 1:
            type = 1
        elif ao == 1 and bo == 0:
            type = 2
        elif ao == 0 and bo == 0:
            type = 3
    
    # return type.
    return type

def calc_mismatch(s1, s2):
    count = 0
    for i in range(len(s1)):
        if s1[i] != s2[i]:
            count += 1
    return count


def main():
       
        sog_file = sys.argv[1]
        node_file = sys.argv[2]
        new_edges_file = sys.argv[3]

        spamReader = open(node_file, 'rt')
        
        lines = spamReader.readlines()
        spamReader.close()
        
        nodes_dict = defaultdict(temp)
        j = 0
        read1_over_bases = ''
        read2_over_bases = '' 
        
        print ("Start reading nodes") 
        
                
        for i in range(len(lines) ):
            t =lines[i].strip().split()
            read_id = t[1]
            seq_id = int(t[0])
            read_size = t[2]
            read_bases = t[6]
            nodes_dict[read_id].seq_id = seq_id
            nodes_dict[read_id].read_size = read_size
            nodes_dict[read_id].read_bases = read_bases
            #print nodes_dict[read_id].seq_id, nodes_dict[read_id].read_bases            
                
                
                
                
        
        print ("second stage")
        
        

    	print 'start reading overlap info'
    
    	fin = open(sog_file, "rb")
	fout = open(new_edges_file, "wb") 
    	for line in fin:
    		tmp = line.strip().split()
    		if tmp[0] == "ED":
               
    
    			read1_id = nodes_dict[tmp[1]].seq_id
    			read2_id = nodes_dict[tmp[2]].seq_id
    			read1_over_start = int(tmp[3])
    			read1_over_end = int(tmp[4])
    			read2_over_start = int(tmp[6])
    			read2_over_end = int(tmp[7])
    			read1_orient = 0
    			read2_orient = int(tmp[9])
    			insert_size = -1
    			a = int(read1_id)
    			b = int(read2_id)
    			implied_state_c = implied_state( a, b, read1_orient, read2_orient)
    			implied_dist = -1
    			std_dev = -1
    			invalid = 0
    			used = 0
    		
    			o = read1_over_end - read1_over_start
    			d = int (nodes_dict[tmp[1]].read_size) - o
    			eps = 0.01
    			read1_bases = nodes_dict[tmp[1]].read_bases
    			read2_bases = nodes_dict[tmp[2]].read_bases
    			read1_over_bases = read1_bases[read1_over_start: read1_over_end]
    			
    
    			# reverse compliment
			if read2_orient == 1:
				read2_bases = read2_bases.translate(string.maketrans("ATCG", "TAGC"))[::-1]		
    			
				
			read2_over_bases = read2_bases[read2_over_start: read2_over_end]
			#print 'lllllllll', read1_over_bases, read2_over_bases

    			j = calc_mismatch(read1_over_bases, read2_over_bases)
			

                
    			weight = o - j
                	#print "===>", j, d, o, weight  
			
    			fout.write("%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%f\n" % 
    				(a, b, read1_over_start, read1_over_end, read2_over_start, read2_over_end, 0, read2_orient, insert_size, implied_state_c, implied_dist, std_dev, invalid, used, weight ))
    	
    	fin.close()
    	print 'Reading end'
if __name__ == '__main__':
    main()

