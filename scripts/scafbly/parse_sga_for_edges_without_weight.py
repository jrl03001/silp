'''
Parses string overlap graph from SGA file, outputs info for scaffolding program.
'''
# imports
import sys
import os
import gzip
from collections import defaultdict

# parameters
sog_file = sys.argv[1]
nodes_file = sys.argv[2]
edge_file = sys.argv[3]


 


###### functions.
fout = open(edge_file, "wb")

nodes = defaultdict(int)


print 'Strat reading nodes ids'
fin = open(nodes_file, "rb")
for line in fin:
	''' tokenize node '''
	# add entry.
	tmp = line.strip().split()
	nodes[tmp[1]] = tmp[0]
	


###### script.

def implied_state(aid, bid, ao, bo):
	''' determines class of edges '''
	
	# check if type 1
	if aid < bid:
			
		# determine happyness class.
		if ao == 0 and bo == 0:
			type = 0
		elif ao == 0 and bo == 1:
			type = 1
		elif ao == 1 and bo == 0:
			type = 2
		elif ao == 1 and bo == 1:
			type = 3
			
	# no its type two.
	else:
		
		# determine happyness class.
		if ao == 1 and bo == 1:
			type = 0
		elif ao == 0 and bo == 1:
			type = 1
		elif ao == 1 and bo == 0:
			type = 2
		elif ao == 0 and bo == 0:
			type = 3
	
	# return type.
	return type



# define the node list and edge list.


# iterate over the compressed file.


print 'start reading overlap info'

fin = open(sog_file, "rb")
for line in fin:
	tmp = line.strip().split()
	if tmp[0] == "ED":

		read1_id = nodes[tmp[1]]
		read2_id = nodes[tmp[2]]
		read1_over_start = int(tmp[3])
		read1_over_end = int(tmp[4])
		read2_over_start = int(tmp[6])
		read2_over_end = int(tmp[7])
		read1_orient = 0
		read2_orient = int(tmp[9])
		insert_size = -1
		a = int(read1_id)
		b = int(read2_id)
		implied_state_c = implied_state( a, b, read1_orient, read2_orient)
		implied_dist = -1
		std_dev = -1
		invalid = 0
		used = 0
		
		fout.write("%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\n" % 
			(a, b, read1_over_start, read1_over_end, read2_over_start, read2_over_end, 0, read2_orient, insert_size, implied_state_c, implied_dist, std_dev, invalid, used, 1))
	
fin.close()
print 'Reading end'
