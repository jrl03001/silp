'''
Parses string overlap graph from SGA file, outputs info for scaffolding program.
'''
# imports
import sys
import os
import gzip
from collections import defaultdict

# parameters
sog_file = sys.argv[1]
node_file = sys.argv[2]


 


###### functions.
fout = open(node_file, "wb")

nodes = defaultdict(int)
edges = list()

##### script.





# iterate over the compressed file.
print 'Strat reading nodes ids'
fin = open(sog_file, "rb")
j = 0
for line in fin:
	
	# tokenize.
	tmp = line.strip().split()
	
	# process based on type.
	if tmp[0] == "VT":
		#print  tmp
		fout.write("%i\t%s\t%i\t%i\t%i\t%i\t%s\n" % 
			( j, tmp[1], len(tmp[2]), 0, 0, 0, tmp[2]))
		j += 1
	
fin.close()
print 'Reading end'
