'''
Parses string overlap graph from SGA file, outputs info for scaffolding program.
'''
# imports
import sys
import os
import gzip
from collections import defaultdict

# parameters
sog_file = sys.argv[1]
valid_file = sys.argv[2]
new_sog_file = sys.argv[3]


 


###### functions.
fout = open(new_sog_file, "wb")

nodes = []


print 'Strat reading edges ids'
fin = open(valid_file, "rb")
for line in fin:
	''' tokenize node '''
	# add entry.
	tmp = line.strip().split()
	nodes.append([tmp[0],tmp[1]])
	


###### script.


# define the node list and edge list.


# iterate over the compressed file.


print 'start filtering edges'

fin = open(sog_file, "rb")
for line in fin:
	tmp = line.strip().split()
	if tmp[0] == "ED":

		read1_id = tmp[1]
		read2_id = tmp[2]
		for j in nodes:
			if (j[0] == read1_id and j[1] == read2_id) or (j[0] == read2_id and j[1] == read1_id):
				fout.write(line)
	else: 
		fout.write(line)
	
fin.close()
print 'Reading end'
