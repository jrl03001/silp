'''
finds out number of valid adjacencies at different bundle sizes.
'''
# program imports.
from data_structs.nodes import save_nodes
from data_structs.edges import save_edges
from data_structs.bundles import save_bundles
from data_structs.types import node_dt, edge_dt, bundle_dt


# system imports.
from collections import deque
import random
import time
import numpy as np
import subprocess
import itertools
import networkx as nx
import logging
import sys
import os
import h5py
import tempfile
time_start = time.time()

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

# parameters.
in_file = sys.argv[1]
typeof = sys.argv[2]
out_file = sys.argv[3]

## script.

# switch based on type.
if typeof == "node":
	
	# load into memory.
	fin = open(in_file)
	lines = fin.readlines()
	fin.close()
	
	# build array.
	nodes = np.zeros(len(lines), dtype=node_dt)
	
	# copy info to array.
	idx = 0
	for line in lines:
		
		# tokenize.
		tmp = line.strip().split()
		node_id = int(tmp[0])
		node_name = tmp[1]
		node_width = int(tmp[2])
		node_x = int(tmp[3])
		node_4 = int(tmp[4])
		node_5 = int(tmp[5])
		
		# copy to array.
		nodes[idx][0] = node_id
		nodes[idx][1] = node_name
		nodes[idx][2] = node_width
		nodes[idx][3] = node_x
		nodes[idx][4] = node_4
		nodes[idx][5] = node_5
		idx += 1
	
	# save node.
	save_nodes(nodes, out_file)
	
# switch based on type.
elif typeof == "edge":
	
	# load into memory.
	fin = open(in_file)
	lines = fin.readlines()
	fin.close()
	
	# build array.
	edges = np.zeros(len(lines), dtype=edge_dt)

	# copy info to array.
	idx = 0
	for line in lines:
		
		# tokenize.
		tmp = line.strip().split()

		# copy.
		for i in range(len(tmp)):
			if i != 14:
				edges[idx][i] = int(tmp[i])
			else:
				edges[idx][i] = float(tmp[i])
		idx += 1
		
	# save edges to disk.
	save_edges(edges, out_file)
