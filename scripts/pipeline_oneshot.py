'''
driver script for one shot run.
'''
# system imports.
import sys
import os
import subprocess
import logging

# program imports.
from data_structs.edges import load_edges
from data_structs.edges import save_edges

logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

######### parameters #########

## user-provided.
node_base_file = os.path.abspath(sys.argv[1])
edge_base_file = os.path.abspath(sys.argv[2])
gff_file = os.path.abspath(sys.argv[3])
work_dir = os.path.abspath(sys.argv[4])
bundle_size = int(sys.argv[5])

## hard-coded name.
name_base = "oneshot_%i" % bundle_size

## hard-coded filters.
#filters = "min_bound,neighbor,bundle"
filters = "bundle"
filter_nsize = 15
filter_std_min = 4
filter_std_max = 99
filter_rep_over = 0.9

## decomposition settings.
decomp_bound = 1500

## hard-coded data.

## hard-coded working.
input_dir = "%s/input" % work_dir
agps_dir = "%s/agps" % work_dir
graphs_dir = "%s/graphs" % work_dir
logs_dir = "%s/logs" % work_dir
serial_dir = "%s/serial" % work_dir

nodes_file = "%s/nodes.hdf5" % input_dir
edges_file = "%s/edges.hdf5" % input_dir
bundles_file = "%s/bundles.hdf5" % input_dir

graph0_file = "%s/decomp_zero.txt" % graphs_dir
graph1_file = "%s/decomp_one.txt" % graphs_dir
graph2_file = "%s/decomp_two.txt" % graphs_dir

agp_file = "%s/%s.agp" % (agps_dir, name_base)

pipeline_out = "%s/pipeline.out" % logs_dir
pipeline_err = "%s/pipeline.err" % logs_dir

## hard-coded commands.
cmd_filter_edges = "/home/jlindsay/central/code/SILP/bin/filter_edges"
cmd_create_bundles = "/home/jlindsay/central/code/SILP/bin/create_bundles"
cmd_decompose = "/home/jlindsay/central/code/SILP/scripts/decompose.sh"
cmd_scaffold1 = "/home/jlindsay/central/code/SILP/scripts/scaffold_part1.py"
cmd_scaffold2 = "/home/jlindsay/central/code/SILP/scripts/scaffold_part2_mcmw.py"

######### functions #########

def run_wrapper(cmd, echo=False):
	''' wrapper to run commands '''
	
	# stringify cmd.
	for i in range(len(cmd)):
		cmd[i] = str(cmd[i])
	
	if echo == True:
		print ' '.join(cmd)
	
	# run command.
	fout = open(pipeline_out, "a")
	ferr = open(pipeline_err, "a")
	ret = subprocess.call(cmd, stdout=fout, stderr=ferr)
	fout.close()
	ferr.close()
	
	# print a failure.
	if ret != 0:
		logging.error("failure: %s" % ' '.join(cmd))
	
	# return result.
	return ret

def prep_dir(path):
	''' checks if dir exists, if not make it'''
	if os.path.isdir(path) == False:
		if run_wrapper(["mkdir", path]) != 0:
			logging.error("couldn't make directory: %s" % path)
			sys.exit(1)
			
######### script #########
	
## always run these working and log steps.
logging.info("prep working dir and clear logs")

# sanity check working dir.
if os.path.isdir(work_dir) == False:
	subprocess.call(["mkdir", work_dir])
	
# always clear logs and reset logs automatically.
if os.path.isdir(logs_dir) == True:
	subprocess.call(["rm", "-rf", logs_dir])
subprocess.call(["mkdir", logs_dir])

# prepare directories.
for x in [work_dir, input_dir, graphs_dir, logs_dir, serial_dir]:
	prep_dir(x)

# create node if necessary.
if os.path.isfile(nodes_file) == False:
	logging.info("making nodes")
	if run_wrapper(["cp", node_base_file, nodes_file]) != 0:
		logging.error("couldn't make node file")
		sys.exit(1)

# create edge if necessary.
if os.path.isfile(edges_file) == False:
	
	# create the edge file.
	logging.info("making edges")
	if run_wrapper(["cp", edge_base_file, edges_file]) != 0:
		logging.error("couldn't make node file")
		sys.exit(1)
	
		
# make the bundles if necessary.
if os.path.isfile(bundles_file) == False:

	# filter the edge file.
	if run_wrapper([
		cmd_filter_edges, 
		filters, 
		"-nodes", nodes_file, 
		"-edges", edges_file, 
		"-gff", gff_file,
		"-bundle", bundle_size,
		"-neighbor", filter_nsize,
		"-std_min", filter_std_min,
		"-std_max", filter_std_max,
		"-rep_over", filter_rep_over,
		"-agp", "-"
	], echo = True) != 0:
		logging.error("couldn't filter edge file")
		sys.exit(1)



	# make the bundles.
	logging.info("making bundles")
	if run_wrapper([
		cmd_create_bundles,
		nodes_file,
		edges_file,
		bundles_file,
		"repeat", gff_file,
		"-",
		0
	], echo = True) != 0:
		logging.error("couldn't create bundles")
		sys.exit(1)
		
# decompose the graph.
if os.path.isfile(graph0_file) == False:
	logging.info("making decomposition")
	if run_wrapper([
		cmd_decompose,
		nodes_file,
		bundles_file,
		graphs_dir,
		decomp_bound
	], echo = True) != 0:
		logging.error("could not decompose the graph")
		sys.exit(1)
	
# run the scaffolding.
if os.path.isfile(agp_file) == False:
	logging.info("running program")
	run_wrapper([
		"python",
		cmd_scaffold1,
		work_dir,
		agp_file,
		"-"
	])
	run_wrapper([
		"python",
		cmd_scaffold2,
		work_dir,
		agp_file,
		"-"
	])
