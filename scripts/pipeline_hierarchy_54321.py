'''
driver script for heirarchy run.
'''
# system imports.
import sys
import os
import subprocess
import logging

# program imports.
from data_structs.edges import load_edges
from data_structs.edges import save_edges

logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

######### parameters #########

## user-provided.
data_dir = os.path.abspath(sys.argv[1])
test_size = int(sys.argv[2])

if len(sys.argv) > 3:
	hard_case = True
else:
	hard_case = False

logging.info("hard case")

## name settings.
name_base = "heirarchy"

## heirarchy settings.
bundles = [5, 4, 3, 2, 1]

## hard-coded filters.
filters = "min_bound,bundle"
filtersagp = "agpused,agpbad,agpsize,min_bound,bundle"
filtershard = "agpused,agpbad,agpsize,min_bound,bundle,neighbor"
filter_nsize = 15
filter_std_min = 4
filter_std_max = 99
filter_rep_over = 0.9

## decomposition settings.
decomp_bound = 1500

## hard-coded data.
contigs_file = "%s/contigs.fasta" % data_dir
gff_file = "%s/annotation.gff" % data_dir
sam1_file = "%s/venter_R3.sam" % data_dir
sam2_file = "%s/venter_F3.sam" % data_dir
node_base_file = "%s/nodes.hdf5" % data_dir
edge_base_file = "%s/edges.hdf5" % data_dir

## hard-coded commands.
cmd_create_nodes = "/home/jlindsay/central/code/SPQR_scaffold/create_nodes"
cmd_create_edges = "/home/jlindsay/central/code/SPQR_scaffold/create_edges"
cmd_filter_edges = "/home/jlindsay/central/code/SPQR_scaffold/filter_edges"
cmd_create_bundles = "/home/jlindsay/git/SINAH/SINAH/bin/create_bundles"
cmd_decompose = "/home/jlindsay/git/SINAH/SINAH/scripts/decompose.sh"
cmd_part1 = "/home/jlindsay/git/SINAH/SINAH/scripts/scaffold_part1.py"
cmd_part2 = "/home/jlindsay/git/SINAH/SINAH/scripts/scaffold_part2_ilpweight.py"
cmd_flipflop = "/home/jlindsay/central/code/SPQR_scaffold/toggle_edges"

######### functions #########

def run_wrapper(cmd):
	''' wrapper to run commands '''
	
	# stringify cmd.
	for i in range(len(cmd)):
		cmd[i] = str(cmd[i])
	
	# try to run command with logging.
	try:
		fout = open(pipeline_out, "a")
		ferr = open(pipeline_err, "a")
		ret = subprocess.call(cmd, stdout=fout, stderr=ferr)
		fout.close()
		ferr.close()
	except IOError:
		ret = subprocess.call(cmd)
	
	# print a failure.
	if ret != 0:
		logging.error("failure: %s" % ' '.join(cmd))
	
	# return result.
	return ret

def prep_dir(path):
	''' checks if dir exists, if not make it'''
	if os.path.isdir(path) == False:
		if run_wrapper(["mkdir", path]) != 0:
			logging.error("couldn't make directory: %s" % path)
			sys.exit(1)
	
######### script #########
	
# prepare all directories.
for bundle_size in bundles:
	
	# setup dirs.
	work_dir= "%s/%s_%i_%i" % (os.getcwd(), name_base, test_size, bundle_size)
	input_dir = "%s/input" % work_dir
	agps_dir = "%s/agps" % work_dir
	graphs_dir = "%s/graphs" % work_dir
	serial_dir = "%s/serial" % work_dir
	logs_dir = "%s/logs" % work_dir
	
	# set error files.
	pipeline_out = "%s/pipeline.out" % logs_dir
	pipeline_err = "%s/pipeline.err" % logs_dir
	
	# make em.
	for x in [work_dir, logs_dir, input_dir, graphs_dir, serial_dir]:
		prep_dir(x)
	
# the main loop over bundle size.
prev_agp = "-"
prev_node = "-"
prev_edge = "-"
for bundle_size in bundles:
	
	# set parameters.
	work_dir= "%s/%s_%i_%i" % (os.getcwd(), name_base, test_size, bundle_size)
	input_dir = "%s/input" % work_dir
	agps_dir = "%s/agps" % work_dir
	graphs_dir = "%s/graphs" % work_dir
	serial_dir = "%s/serial" % work_dir
	logs_dir = "%s/logs" % work_dir

	nodes_file = "%s/nodes.hdf5" % input_dir
	edges_file = "%s/edges.hdf5" % input_dir
	bundles_file = "%s/bundles.hdf5" % input_dir

	graph0_file = "%s/decomp_zero.txt" % graphs_dir
	graph1_file = "%s/decomp_one.txt" % graphs_dir
	graph2_file = "%s/decomp_two.txt" % graphs_dir

	agp_file = "%s/%s_%i_%s.agp" % (agps_dir, name_base, test_size, bundle_size)

	pipeline_out = "%s/pipeline.out" % logs_dir
	pipeline_err = "%s/pipeline.err" % logs_dir
	
	# check if we have to copy previous.
	if prev_node != "-":
		print "copying oldies"
		run_wrapper(["cp", prev_node, nodes_file])
		run_wrapper(["cp", prev_edge, edges_file])
	
	# create the node file.
	if os.path.isfile(nodes_file) == False:
		
		# sanity check.
		if bundle_size != bundles[0]:
			logging.error("should not create nodes twice in heirarchy")
			sys.exit(1)
		
		logging.info("making nodes")
		if run_wrapper(["cp", node_base_file, nodes_file]) != 0:
			logging.error("couldn't make node file")
			sys.exit(1)
		
	# create edge if necessary.
	if os.path.isfile(edges_file) == False:
		
		# sanity check.
		if bundle_size != bundles[0]:
			logging.error("should not create nodes twice in heirarchy")
			sys.exit(1)
		
		# create the edge file.
		logging.info("making edges")
		if run_wrapper(["cp", edge_base_file, edges_file]) != 0:
			logging.error("couldn't make node file")
			sys.exit(1)

		
	# make the bundles if necessary.
	if os.path.isfile(bundles_file) == False:
		
		# check how we setup filters.
		if prev_agp == "-":
			afilt = filters
		else:
			afilt = filtersagp
		
		# check if we need to modify filters.
		if hard_case == True and bundle_size == bundles[-1]:
			logging.info("activating hard case")
			filter_nsize = 3
			filter_std_min = 3
			filter_std_max = 15
			filter_rep_over = 0.98
			afilt = filtershard
		
		# run the filtering.
		if run_wrapper([
			cmd_filter_edges, 
			afilt, 
			"-nodes", nodes_file, 
			"-edges", edges_file, 
			"-gff", gff_file,
			"-bundle", bundle_size,
			"-neighbor", filter_nsize,
			"-std_min", filter_std_min,
			"-std_max", filter_std_max,
			"-rep_over", filter_rep_over,
			"-agp", prev_agp
		]) != 0:
			logging.error("couldn't filter edge file with agp")
			sys.exit(1)
		
		# make the bundles.
		logging.info("making bundles")
		if run_wrapper([
			cmd_create_bundles,
			nodes_file,
			edges_file,
			bundles_file,
			"repeat", gff_file,
			"-",
			0
		]) != 0:
			logging.error("couldn't create bundles")
			sys.exit(1)
		
	# decompose the graph.
	if len(os.listdir(graphs_dir)) == 0:
		logging.info("making decomposition")
		if run_wrapper([
			cmd_decompose,
			nodes_file,
			bundles_file,
			graphs_dir,
			decomp_bound
		]) != 0:
			logging.error("could not decompose the graph")
			sys.exit(1)
			
	# run the scaffolding.
	if os.path.isfile(agp_file) == False:
		logging.info("running program")
		run_wrapper([
			"python",
			cmd_part1,
			work_dir,
			agp_file,
			prev_agp
		])
		run_wrapper([
			"python",
			cmd_part2,
			work_dir,
			agp_file,
			prev_agp
		])
	
		## the old style heirarchy.
		# flipflop the edges.
		logging.info("flippy floppy")
		run_wrapper([
			cmd_flipflop,
			edges_file,
			bundle_size,
		])
	
	# set previous.
	prev_agp = agp_file
	prev_node = nodes_file
	prev_edge = edges_file
	
