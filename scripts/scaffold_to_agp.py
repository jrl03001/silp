'''
takes scaffold fasta file, the contig file and
outputs agp file.
'''
import sys
import os
import string
import numpy as np

from data_structs.agps import save_agps
from data_structs.types import agp_dt

### parameters ###
REF_FILE = sys.argv[1]
SCAF_FILE = sys.argv[2]
AGPS_FILE = sys.argv[3]

### functions ###

def load_fasta(file_path):
	''' loads fasta file into dictionary'''
	
	# read file into memory.
	fin = open(file_path)
	lines = fin.readlines()
	fin.close()
	
	# build dictionary.
	data = dict()
	seq = ""
	for line in lines:
		
		# Skip blanks.
		if len(line) < 2: continue
		if line[0] == "#": continue

		# remove blanks.
		line = line.strip()

		# Check for ids.
		if line.count(">") > 0: 

			# Check if ending seq.
			if len(seq) > 0:
		
				# save.
				data[head] = seq
				
			# reset head.
			head = line.replace(">","")
			seq = ""
			
			# skip to next line.
			continue

		# Filter chars.
		seq += line
		
	# save the last one.
	data[head] = seq
	
	# return dictionary.
	return data

def break_scaffolds(scafs):
	''' break scaffolds '''
	
	# break scaffold into contigs.
	broken = dict()
	for key in scafs:
		
		# simplify.
		seq = scafs[key]
		
		# make sure we have lowercase n.
		seq = seq.replace("N","n")
		
		# split up by n.
		tmps = seq.split("n")
		
		# remove empty.
		ctgs = list()
		for x in tmps:
			if len(x) != 0:
				ctgs.append(x)
				
		# add to broken.
		broken[key] = [ctgs,[]]

	# add gap sizes.
	for key in scafs:
		
		# simplify.
		seq = scafs[key].replace("N","n")
		
		# identify cap.
		ingap = False
		for i in range(len(seq)):
			
			# check status.
			if seq[i] == "n":
				
				# check if in gap.
				if ingap == False:
					
					# bootstrap.
					ingap = True
					gsize = 1
					
				# grow gap.
				else:
					
					# increment
					gsize += 1
			
			else:
				
				if ingap == True:
					broken[key][1].append(gsize)
					ingap = False
		
	# return new dictionary.
	return broken

def find_orientations(ctgs, ctg):
	''' compares contigs against reference '''
	
	# loop over each reference.
	for x in ctgs:
		
		# simplify.
		seq1 = ctgs[x]
		seq2 = seq1.translate(string.maketrans("ATCGN", "TAGCN"))[::-1]
		
		# check if this key, same orientation.
		if seq1 == ctg:
			return 0, x
			
		# check if this key, diff orientation.
		if seq2 == ctg:
			return 1, x
			
			
	# not found error.
	print "hey bad stuff"
	sys.exit(1)
	

### script ###

# load the scaffold and contig files.
refs = load_fasta(REF_FILE)
scafs = load_fasta(SCAF_FILE)

# break into contigs.
broken = break_scaffolds(scafs)

# create the agp array.
acnt = 0
for x in broken:
	acnt += len(broken[x][0]) + len(broken[x][1])
agps = np.zeros(acnt, dtype=agp_dt)	

	
# copy results into array.
idx = 0
for scaf_key in sorted(broken.keys()):
	
	# setup idx tracking.
	sloc = 1
	sidx = 1
	
	# make a reverse list of gaps.
	gaps = broken[scaf_key][1]
	gaps.reverse()
	
	# loop over each contig.
	sz = len(broken[scaf_key][0])
	for i in range(sz):
		
		# simplify.
		ctg = broken[scaf_key][0][i]
		
		# find orientation.
		o, ctg_key = find_orientations(refs, ctg)
		
		# build agp entry.
		agps[idx]['scaf_name'] = scaf_key
		agps[idx]['scaf_start'] = sloc
		agps[idx]['scaf_stop'] = sloc + len(ctg)
		agps[idx]['scaf_idx'] = sidx
		agps[idx]['comp_type'] = "W"
		agps[idx]['comp_name'] = ctg_key
		agps[idx]['comp_start'] = 1
		agps[idx]['comp_stop'] = len(ctg) + 1
		agps[idx]['comp_orien'] = o
		agps[idx]['comp_linkage'] = 0
	
		# increment index.
		sloc = agps[idx]['scaf_stop'] + 1
		sidx += 1
		idx += 1
		
		# do gap.
		if i < sz - 1:
		
			# get gap.
			gap = gaps.pop()
		
			# add resulting gap.
			agps[idx]['scaf_name'] = scaf_key
			agps[idx]['scaf_start'] = sloc
			agps[idx]['scaf_stop'] = sloc + gap
			agps[idx]['scaf_idx'] = sidx
			agps[idx]['comp_type'] = "N"
			agps[idx]['comp_name'] = "gap"
			agps[idx]['comp_start'] = 1
			agps[idx]['comp_stop'] = gap + 1
			agps[idx]['comp_orien'] = o
			agps[idx]['comp_linkage'] = 0
		
			# increment index.
			sloc = agps[idx]['scaf_stop'] + 1
			sidx += 1
			idx += 1

# save agp to disk.
save_agps(AGPS_FILE, agps)
