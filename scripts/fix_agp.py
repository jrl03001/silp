'''
temporary script to fix AGP gap sizes to be atleast 10
'''
# program imports.
from data_structs.agps import load_agps
from data_structs.agps import save_agps
from data_structs import types

# system imports.
import random
import time
import subprocess
import itertools
import logging
import sys
import os
import h5py
import tempfile
time_start = time.time()

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

# parameters.
agp_file = sys.argv[1]

#################### classes ####################

#################### functions ####################

def get_runtime(agp_file):
	''' finds runtime in AGP file '''
	fin = open(agp_file)
	rt = -1
	for line in fin:
		if line.count("RUNTIME") > 0:
			rt = float(line.strip().split()[1])
			break
	fin.close()
	
	if rt == -1:
		logging.error("can't find runtime")
		sys.exit(1)
	return rt
			
########### script ################## 

# load the agp file into memory.
agp_edges = load_agps(agp_file)

# set fragment size to 10.
for i in range(agp_edges.size):
	if agp_edges[i]['comp_type'] == 'N':
		if agp_edges[i]['comp_stop'] - agp_edges[i]['comp_start'] < 10:
			agp_edges[i]['comp_stop'] = 10

# iterate over the array looking for scaffolds.
idxs = dict()
for i in range(agp_edges.size):
	
	# simplify.
	scaf = agp_edges[i]['scaf_name']
	ctg = agp_edges[i]['comp_name']
	
	# bootstrap idx.
	if scaf not in idxs:
		idxs[scaf] = 1
		
	#  update start/stop.
	agp_edges[i]['scaf_start'] = idxs[scaf]
	agp_edges[i]['scaf_stop'] = idxs[scaf] + (agp_edges[i]['comp_stop'] - agp_edges[i]['comp_start'])
	
	# update index.
	idxs[scaf] = agp_edges[i]['scaf_stop'] + 1
	
# load previous runtime.
rt = get_runtime(agp_file)
	
# save to disk.
save_agps(agp_file, agp_edges)	
	
# append runtime to file.
txt = ""
fin = open(agp_file)
for line in fin:
	txt += line
txt += "RUNTIME:\t%f\n" % (rt)
fin.close()
fout = open(new_agp_file, "wb")
fout.write(txt)
fout.close()

