'''
solves scaffolding problem using ILP
on pairs only. Then weighted bi-partite matching
to find the paths.
'''
# program imports.
from data_structs.nodes import load_nodes
from data_structs.nodes import create_lookup
from data_structs.edges import load_edges
from data_structs.edges import save_edges
from data_structs.weights import load_weights
from data_structs.bundles import load_bundles
from data_structs.agps import load_agps
from data_structs.agps import save_agps
from data_structs import types

from optimize.ilp_pair_cplex import SpqrIlp
from optimize.ilp_pair_cplex import BiConstraints
from optimize.matching_cplex import MatchingIlp

from optimize.ilp_pair_cplex import nlist_dt
from optimize.ilp_pair_cplex import blist_dt
from optimize.ilp_pair_cplex import tlist_dt
from optimize.solution_pair import SpqrSolution
from optimize.solution_pair import PartialSolution

from graphs.Directed import Directed
from graphs.Flow import Flow
from graphs.Linear import Linear

from gap_estimate.average import gap_avg
from gap_estimate.minlike import min_like

from scaflib.tree_functions import *
from scaflib.heirarchy_functions import *
from scaflib.data_functions import *
from scaflib.misc_functions import *


from munkres import Munkres

# system imports.
from collections import deque
import random
import time
import numpy as np
import subprocess
import itertools
import networkx as nx
import logging
import sys
import os
import h5py
import tempfile
time_start = time.time()

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

# parameters.
base_dir = os.path.abspath(sys.argv[1])
output_agp_file = sys.argv[2]
previous_agp = sys.argv[3]

# hardcoded formatting.
input_dir = "%s/input" % base_dir
input_nodes_file = "%s/nodes.hdf5" % input_dir
input_edges_file = "%s/edges.hdf5" % input_dir
input_bundles_file = "%s/bundles.hdf5" % input_dir

graph_dir = "%s/graphs" % base_dir
decomp_0_file = "%s/decomp_zero.txt" % graph_dir
decomp_1_file = "%s/decomp_one.txt" % graph_dir
decomp_2_file = "%s/decomp_two.txt" % graph_dir

serial_dir = "%s/serial" % base_dir
directed_graph_file = "%s/directed.gpickle.gz" % serial_dir
linear_graph_file = "%s/linear.gpickle.gz" % serial_dir

agp_dir = "%s/agps" % base_dir
cplex_log_file = "%s/logs/cplex.log" % base_dir
cplex_err_file = "%s/logs/cplex.err" % base_dir
cplex_prg_file = "%s/logs/cplex.lp" % base_dir

int_dir = "%s/intermediate" % base_dir

#################### classes ####################
		
#################### functions ####################
		
def ilp_matching(FG, ilp_obj):
	''' uses ILP to solve matching, returns edge list'''
	
	# load the constraints.
	ilp_obj.load("card", FG)
	
	# solve it.
	elist, val = ilp_obj.solve(file_path=cplex_prg_file)

	# clear it.
	ilp_obj.clear()
	
	# load again for weighted.
	ilp_obj.load("mcmw", FG, card_val=val)

	# solve it.
	elist, val = ilp_obj.solve(file_path=cplex_prg_file)	
	
	# clear it.
	ilp_obj.clear()
	
	# return the edge list.
	return elist
		
########### script ################## 

# load hdf5 information.
logging.info("loading data arrays")
nodes = load_nodes(input_nodes_file)
edges = load_edges(input_edges_file)
bundles = load_bundles(input_bundles_file)
node_lookup = create_lookup(nodes)

# load the directed graph.
logging.info("loading pickled graphs")
TG = nx.read_gpickle(directed_graph_file)
LG = nx.read_gpickle(linear_graph_file)

# building graphs.
logging.info("building graphs.")

# loop over eachs subgraph.
logging.info("solving for paths:")
ilp_obj = MatchingIlp(cplex_log_file, cplex_err_file)
cnt = 0
for subg in TG.comps():
	
	# info.
	if cnt % 10 == 0:
		logging.info("%i" % (cnt))
	cnt += 1
	
	# create the bipartite flow graph.
	FG = Flow(subg)
	
	# calculate path.
	elist = ilp_matching(subg, ilp_obj)
	
	# add to linear graph.
	LG.create_elist(elist)
	
	#logging.info(".")

# see if we need to load agp edges.
logging.info("adding agp gaps")
prev_gaps = dict()
if previous_agp != "-":
	prev_gaps = call_agp_gaps(previous_agp, nodes)

# remove cycles.
logging.info("removing cycles")
LG.remove_cycles()

# find gap estimates.
logging.info("adding gap estimates")
gap_ests = gap_avg(nodes, edges, bundles, prev_gaps)

# apply them.
LG.set_gaps(gap_ests)

# end of execution.
time_stop = time.time()

# verify it.
LG.verify()

# write agp.
agp_edges = LG.gen_agp()
save_agps(output_agp_file, agp_edges)
#LG.write_agp(output_agp_file)

# append runtime to agp.
fin = open(output_agp_file, "rb")
lines = fin.readlines()
fin.close()

lines.append("RUNTIME:\t%f\n" % (time_stop - time_start))

fout = open(output_agp_file, "wb")
fout.write(''.join(lines))
fout.close()

logging.info("COMPLETED!")
	
	

