'''
verifies the decomposition graphs are valid.
'''
# program imports.
from data_structs.nodes import load_nodes
from data_structs.nodes import create_lookup
from data_structs.edges import load_edges
from data_structs.edges import save_edges
from data_structs.weights import load_weights
from data_structs.bundles import load_bundles
from data_structs.agps import load_agps
from data_structs import types

from optimize.ilp_pair_cplex import SpqrIlp
from optimize.ilp_pair_cplex import BiConstraints
from optimize.ilp_pair_cplex import nlist_dt
from optimize.ilp_pair_cplex import blist_dt
from optimize.ilp_pair_cplex import tlist_dt
from optimize.solution_pair import SpqrSolution
from optimize.solution_pair import PartialSolution

from graphs.Directed import Directed
from graphs.Flow import Flow
from graphs.Linear import Linear
from graphs.BiConnected import bicomponents

from gap_estimate.average import gap_avg

# system imports.
from collections import deque
import random
import time
import numpy as np
import subprocess
import itertools
import networkx as nx
import logging
import sys
import os

time_start = time.time()
random.seed(0)

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

# parameters.
base_dir = os.path.abspath(sys.argv[1])

# hardcoded formatting.
input_dir = "%s/input" % base_dir
input_nodes_file = "%s/nodes.txt" % input_dir
input_edges_file = "%s/edges.txt" % input_dir
input_bundles_file = "%s/bundles.txt" % input_dir

graph_dir = "%s/graphs" % base_dir
decomp_0_file = "%s/decomp_zero.txt" % graph_dir
decomp_1_file = "%s/decomp_one.txt" % graph_dir
decomp_2_file = "%s/decomp_two.txt" % graph_dir


#################### classes ####################

class Solution(object):
	''' holds the solution '''
	
	def __init__(self, s0):
		'''sets up solution from a set.'''
		
		# copy the set and use as tracker.
		self._to_solve = set()
		for s in s0:
			self._to_solve.add(s)
			
		# create empty orientation dictionary.
		self._o = dict()
		
		# create empty path dictionary.
		self._p = list()
		
	def set_orientation(self, idx, val):
		''' sets orientation '''
		
		# sanity check right component.
		if idx not in self._to_solve:
			if idx in self._o and self._o[idx] != val:
				logging.error("tried to set node twice with diff values: %i %i %i" % (idx, val, self._o[idx]))
				sys.exit(1)
			return
			
		# solve it.
		self._o[idx] = val
		
		# remove from to solve.
		if idx in self._to_solve:
			self._to_solve.remove(idx)
		
	def get_orientation(self, idx):
		''' gets orientation '''
		
		# sanity check right component.
		if idx not in self._o:
			logging.error("can't get this bad boi")
			sys.exit(1)
			
		# return it.
		return self._o[idx]
		
	def set_path(self, idxa, idxb):
		''' sets a path '''
		
		# sanity check the both have orien set '''
		if idxa not in self._o or idxb not in self._o:
			logging.error("setting path before orientation set")
			sys.exit(1)
		
	
		print "NOT WRITTEN BOSS"
		sys.exit(1)
	
class TestSol(object):
	''' interfaces with ILP to solve '''
	
	def __init__(self):
		self.visited = set()
	
	def fake_solve(self, s0, frozen):
		''' solves the instance '''
			
		# create a solution.
		sol = Solution(s0)
		
		# set the forced the values.
		for idx in frozen:
			sol.set_orientation(idx, frozen[idx])
			
		# randomly choose the remaining.
		for x in s0:
			if x not in frozen:
				sol.set_orientation(x, random.randint(0,1))
				#sol.set_orientation(x, 0)
				
		# return solution.
		return sol
		

class SpqrTest(object):
	''' solves ILP based on decomposition files '''
	
	def __init__(self, nodes, bundles, DG, sol_obj):
		''' sets tracking variables and starts solving '''
		
		# save variables.
		self._nodes = nodes
		self._bundles = bundles
		self._DG = DG
		self._complete_set = set(list(nodes[:]['node_idx']))
		self._sol_obj = sol_obj
				
		# start decomposition.
		self._zero(DG)

	def _zero(self, G):
		''' bootstraps the solution process ''' 

		# build the final solution.
		csol = Solution(self._complete_set)

		# loop over large nodes.
		for n in G.nodes():
			
			# skip n.
			if G.node[n]['graph'] == None:
				
				# solve.
				sol = self._sol_obj.fake_solve(G.node[n]['set'], dict())
				
				# save to master.
				for x in sol._o:
					csol.set_orientation(x, sol._o[x])
				
			else:
				# loop into this to solve.
				sol = self._solve(G.node[n]['graph'], dict(), 1)
			
				# save to master.
				for x in sol._o:
					csol.set_orientation(x, sol._o[x])
				
		# save the solution.
		self._solution = csol
		
	def _solve(self, G, frozen, level):
		''' solves the decomposition '''

		# get the paths we follow.
		bpath = G.graph['bottom']
		tpath = G.graph['top']
		
		# track the nodes in this graph.
		nset = set()
		
		# bottom up solution.
		for p, parent in bpath:
			
			# get active set.
			s0 = G.node[p]['set']
			g0 = G.node[p]['graph']
			
			# add to nset.
			nset = nset.union(s0)
			
			# just solve root.
			if parent == -1:
				
				# check for recursion.
				if g0 != None:
					sol = self._solve(g0, frozen, level+1)
				else:
					sol = self._sol_obj.fake_solve(s0, frozen)
				
				# save root solution.
				G.node[p]['root_sol'] = sol
			
			else:
				# check for intersection.
				cut = G[parent][p]['cut']
				cutsz = len(cut)
				
				# loop over all possible values of cut.
				for x in itertools.product([0,1], repeat=cutsz):
					
					# compute new frozen.
					nfrozen = frozen.copy()
					for i in range(len(cut)):
						if cut[i] not in nfrozen:
							nfrozen[cut[i]] = x[i]
					
					# solve given values.
					if g0 != None:
						sol = self._solve(g0, nfrozen, level+1)
					else:
						sol = self._sol_obj.fake_solve(s0, nfrozen)
					
					
					# save the solution to the node.
					if 'sols' not in G.node[p]:
						G.node[p]['sols'] = dict()
						
					if cut not in G.node[p]['sols']:
						G.node[p]['sols'][cut] = dict()
						
					G.node[p]['sols'][cut][x] = sol
				
		# build up the complete solution object.
		csol = Solution(nset)
			
		# top down application.
		for p, parent in tpath:
			
			# get active set.
			s0 = G.node[p]['set']
			
			# just solve root.
			if parent == -1:
				
				# just apply the solution.
				solo = G.node[p]['root_sol']._o
				for x in solo:
					
					# set orientation.
					csol.set_orientation(x, solo[x])	
					
			else:
				
				# get the cut.
				cut = G[parent][p]['cut']
				cutsz = len(cut)				
				
				# build cut values.
				vals = []
				for x in cut:
					vals.append(csol.get_orientation(x))
				vals = tuple(vals)
				
				# apply the appropriate solution.
				solo = G.node[p]['sols'][cut][vals]._o
				for x in solo:
					
					# set orientation.						
					csol.set_orientation(x, solo[x])	
									
		# return solution.
		return csol		

		

#################### functions ####################

def draw(name, subg):
	''' draw an interactive graph.'''
	# write dot file.
	dot = "./tmp.dot"
	pic = "/home/jlindsay/central/transfer/%s.jpg" % name
	
	nx.write_dot(subg, dot)
	
	# convert to picture using neato.
	subprocess.call(["neato", "-Tjpg", "-o", pic, dot])
	
	# remove dot.
	subprocess.call(["rm","-f",dot])

def create_dir(file_path):
	''' creates a dir. '''
	if os.path.isdir(file_path) == False:
		subprocess.call(["mkdir",file_path])

def load_decomposition(decomp_0_file, decomp_1_file, decomp_2_file):
	''' loads skeleton from decomposition file.'''
	
	# build dictionary of decomposition sets.
	DG = nx.Graph()
	
	# loop over all files.
	for file_path in [decomp_0_file, decomp_1_file, decomp_2_file]:
		
		# loop over all lines.
		fin = open(file_path, "rb")
		for line in fin:
			
			# tokenize.
			line = line.strip().strip(",").split("\t")
			stage_type = int(line[0].replace("stage=",""))
			line_type = line[1].replace("type=","")

			# look for nodes.
			if line_type == "N":
			
				# get index.
				tmp = line[2].replace("idx=","").strip().strip(",").split(",")
				idx0 = int(tmp[0])
				idx1 = -1
				idx2 = -1
				
				if len(tmp) > 1:
					idx1 = int(tmp[1])
				if len(tmp) > 2:
					idx2 = int(tmp[2])

				# build list of active.
				tmp1 = line[3].replace("set=","").strip(",")
				tmp2 = tmp1.split(",")			
				
				# add this set to list.
				tmp = set()
				for x in tmp2:
					tmp.add(int(x))

				# add set to graph.
				if idx0 != -1 and  DG.has_node(idx0) == False:
					DG.add_node(idx0, {'graph':nx.DiGraph(), 'set':set()})

				if idx1 != -1 and DG.node[idx0]['graph'].has_node(idx1) == False:
					DG.node[idx0]['graph'].add_node(idx1, {'graph':nx.DiGraph(), 'set':set()})

				if idx2 != -1 and DG.node[idx0]['graph'].node[idx1]['graph'].has_node(idx2) == False:
					DG.node[idx0]['graph'].node[idx1]['graph'].add_node(idx2, {'graph':None, 'set':set()})
				
				# add set at maximum key size.
				if idx0 != -1 and idx1 != -1 and idx2 != -1:
					DG.node[idx0]['graph'].node[idx1]['graph'].node[idx2]['set'] = tmp
					
				elif idx0 != -1 and idx1 != -1:
					DG.node[idx0]['graph'].node[idx1]['set'] = tmp
					
				elif idx0 != -1:
					DG.node[idx0]['set'] = tmp

			elif line_type == "E":
				
				# get indicies.
				tmp = line[2].replace("idx1=","").strip().strip(",").split(",")
				ida0 = int(tmp[0])
				ida1 = -1
				ida2 = -1
				if len(tmp) > 1:
					ida1 = int(tmp[1])
				if len(tmp) > 2:
					ida2 = int(tmp[2])
					
				tmp = line[3].replace("idx2=","").strip().strip(",").split(",")
				idb0 = int(tmp[0])
				idb1 = -1
				idb2 = -1	
				if len(tmp) > 1:
					idb1 = int(tmp[1])
				if len(tmp) > 2:
					idb2 = int(tmp[2])

				# ge the cut.
				cut = line[4].replace("cut=","").strip().strip(",").split(",")
				for i in range(len(cut)):
					cut[i] = int(cut[i])
				
				# add directed edge.
				if ida2 == -1 and ida1 == -1:
					DG.add_edge(ida0, idb0, {'cut':tuple(cut)})
					
				elif ida2 == -1:
					DG.node[ida0]['graph'].add_edge(ida1, idb1, {'cut':tuple(cut)})
					
				else:
					DG.node[ida0]['graph'].node[ida1]['graph'].add_edge(ida2, idb2, {'cut':tuple(cut)})
				
			elif line_type == "R":
				
				# get index.
				tmp = line[2].replace("idx=","").strip().strip(",").split(",")
				idx0 = int(tmp[0])
				idx1 = -1
				idx2 = -1
				
				if len(tmp) > 1:
					idx1 = int(tmp[1])
				if len(tmp) > 2:
					idx2 = int(tmp[2])
					
				# add the root.
				if idx2 == -1 and idx1 == -1:
					DG.graph['root'] = idx0
					
				elif idx2 == -1:
					DG.node[idx0]['graph'].graph['root'] = idx1
					
				else:
					DG.node[idx0]['graph'].node[idx1]['graph'].graph['root'] = idx2
				
		fin.close()
	
	# clear out non-decomposed entries.
	recurse_clear(DG)
	
	# compute edges.
	#recurse_edgeify(DG)
	
	# compute decomposition paths.
	for n in DG.nodes():
		
		# pathify this node if necessary.
		if DG.node[n]['graph'] != None:
			recurse_paths(DG.node[n]['graph'])

	# return the final object.
	return DG

	
def recurse_paths(G):
	''' replaces each graph with a directed version '''
	
	# create nlist.
	nlist = G.nodes()
		
	# digg deeper for each node.
	for n in nlist:
			
		# check if we add graph.
		if G.node[n]['graph'] != None:
			recurse_paths(G.node[n]['graph'])
					
	# build the edge lists.
	G.graph['bottom'] = dfs_bottom(G, G.graph['root'])
	G.graph['top'] = dfs_top(G, G.graph['root'])	

	
def recurse_edgeify(G):
	''' adds edges to graph based on overlap '''
	
	# create nlist.
	nlist = G.nodes()
	
	# loop over nodes of graph.
	for i in range(len(nlist)):
		
		# check if this requires further edgeification.
		if G.node[nlist[i]]['graph'] != None:
			recurse_edgeify(G.node[nlist[i]]['graph'])
			
		# look for edges.
		for j in range(i+1, len(nlist)):
			
			# check for intersection.
			inter = G.node[nlist[i]]['set'].intersection(G.node[nlist[j]]['set'])
			
			if len(inter) > 2:
				print "SAAAD"
			
			# if intersection annotate it and add edge.
			if len(inter) > 0:
				G.add_edge(nlist[i], nlist[j], {'cut': inter})
				

def dfs_bottom(G, root):
	''' builds edge list from bottom'''
		
	# get the predecessor dict.
	preds = nx.dfs_predecessors(G, root)
	
	# build the node list.
	nlist = [x for x in nx.dfs_postorder_nodes(G, root)]
	
	# build the edge list.
	elist = list()
	for n in nlist:
		if n in preds:
			elist.append( (n, preds[n]) )
		else:
			elist.append( (n, -1) )
	
	# return the list.
	return elist
	
		
def dfs_top(G, root):
	''' builds edge list from bottom'''
		
	# get the predecessor dict.
	preds = nx.dfs_predecessors(G, root)
	
	# build the node list.
	nlist = [x for x in nx.dfs_preorder_nodes(G, root)]
	
	# build the edge list.
	elist = list()
	for n in nlist:
		if n in preds:
			elist.append( (n, preds[n]) )
		else:
			elist.append( (n, -1) )
	
	# return the list.
	return elist
		
def recurse_clear(G):
	for n in G.nodes():
		if G.node[n]['graph'] != None:
			if len(G.node[n]['graph'].nodes()) == 0:
				G.node[n]['graph'] = None
			else:
				recurse_clear(G.node[n]['graph'])
		
########### script ################## 

# load hdf5 information.
logging.info("loading data arrays")
nodes = load_nodes(input_nodes_file)
bundles = load_bundles(input_bundles_file)

# test solution.
sol_test = TestSol()

# load decomposition.
logging.info("loading decomposition")
DG = load_decomposition(decomp_0_file, decomp_1_file, decomp_2_file)

# create an SPQR solve object.
logging.info("solve the object")
solver = SpqrTest(nodes, bundles, DG, sol_test)

# verify solution.
solution = solver._solution
logging.info("verifying solution completion")
for idx in nodes[:]['node_idx']:
	
	# check orientation.
	solution.get_orientation(idx)
	
