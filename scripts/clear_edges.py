'''
solves scaffolding problem using ILP
on pairs only. Then weighted bi-partite matching
to find the paths.
'''
# program imports.
from data_structs.nodes import load_nodes
from data_structs.nodes import save_nodes
from data_structs.nodes import create_lookup
from data_structs.edges import load_edges
from data_structs.edges import save_edges
from data_structs.edges import determine_class, determine_dist
from data_structs.agps import load_agps
from data_structs import types

# system imports.
import time
import numpy as np
import subprocess
import itertools
import networkx as nx
import logging
import sys
import os
import h5py
import tempfile
time_start = time.time()

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

#################### parameters ####################

## user-supplied parameters.
edge_file = sys.argv[1]

#################### classes ####################

#################### functions ####################	
		
#################### script ####################

# load the edges.
edges = load_edges(edge_file)

# zero the invalid fields.
edges[:]['invalid'] = -1
edges[:]['used'] = -1

# save the edges back to file.
save_edges(edges, edge_file)
