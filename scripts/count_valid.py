'''
finds out number of valid adjacencies at different bundle sizes.
'''
# program imports.
from data_structs.edges import load_edges
from data_structs.edges import save_edges

# system imports.
from collections import deque
import random
import time
import numpy as np
import subprocess
import itertools
import networkx as nx
import logging
import sys
import os
import h5py
import tempfile
time_start = time.time()

# logging.
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
#logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

# parameters.
edge_file = sys.argv[1]

# load the edges.
edges = load_edges(edge_file)

# build adjacency.
adjs = dict()
for e in edges:
	key = tuple(sorted([e['ctg_a_idx'], e['ctg_b_idx']]))
	if key not in adjs:
		adjs[key] = 0
	adjs[key] += 1
	
# count by filter.
for x in [5, 4, 3, 2, 1]:
	cnt = 0
	for y in adjs:
		if adjs[y] >= x:
			cnt += 1
	print x, cnt
