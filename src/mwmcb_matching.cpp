#include <LEDA/graph/graph.h>
#include <LEDA/core/basic_alg.h>
//#include <LEDA/graph/graph_alg.h>

using namespace leda;

int main()
{
  graph G; 

  list<node> A;
  node a0=G.new_node(); A.append(a0);
  node a1=G.new_node(); A.append(a1);
  node a2=G.new_node(); A.append(a2);

  list<node> B;
  node b0=G.new_node(); B.append(b0);
  node b1=G.new_node(); B.append(b1);
  node b2=G.new_node(); B.append(b2);
  node b3=G.new_node(); B.append(b3);

  edge e0=G.new_edge(a0,b1); edge e1=G.new_edge(a0,b3);
  edge e2=G.new_edge(a1,b0); edge e3=G.new_edge(a2,b0);
  edge e4=G.new_edge(a2,b2); edge e5=G.new_edge(a0,b0);
 
  edge_array<double> weight(G);
  weight[e0]=1; weight[e1]=2; weight[e2]=3;
  weight[e3]=2; weight[e4]=1; weight[e5]=10;

  node_array<double> pot(G);
  list<edge> M=MWMCB_MATCHING(G,A,B,weight,pot);

  std::cout << "Maximum Weighted Maximum " 
            << " Cardinality Matching:" << std::endl;
  double weight_of_M=0;
  edge e;
  forall(e,M) {G.print_edge(e); weight_of_M+=weight[e];}
  std::cout << " weight: " << weight_of_M << std::endl;

  node v; forall_nodes(v,G) {
    std::cout << "pot"; G.print_node(v);
    std::cout << "=" << pot[v] << std::endl;
  }
 
  return 0;
}
