 
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include "Array.hh"
#include "Array.cc"
#include "QuadProg++.hh"
#include "QuadProg++.cc"

using namespace std;
using namespace QuadProgPP;
int main (int argc, char *const argv[]) 
{      
     int i,j,k,l;
     float C[20000],R[2000][30],gap[100];
     int a=0,b=1; 
	 string line;
     string field;      
         
 //******************************************************  SCAFFOLD  FILE **************************************************************
 
 string  V[100][9];
 V[0][0]=1;
  V[0][1]=1;
   V[0][2]=1;
    V[0][3]=1;
	 V[0][4]=1;
	  V[0][5]=1;
	   V[0][6]=1;
	    V[0][7]=1;
		 V[0][8]=1;
	  
 ifstream f1;   
 string path=argv[1]; 
 f1.open(path.c_str());
 
      cout << "argc = " << argc << endl; 
      cout << "argv[1] = " << argv[1] << endl; 
 
       if(!f1){
	   
               cout<<"Unable to open"<<argv[1]<<endl;
        
               exit(1);               
       }    	      
	   
       while(f1.good())
	   {
               getline(f1,line);               
			   //cout<<line<<endl;
               if(!line.empty())
                   {
                       istringstream iss(line);                    
                       for (int i=0;i<9;i++)
                       {
                         getline(iss,field,'\t');
                         V[b][i]=field; 						
                       }
                       b++;					   
                   }
       }
    
    f1.close();

    
       for(i = 1; i <= b; i+=2) 
     
       { 
          if (V[i][8]=="-")
          {V[i][8]="1";}        
          else
          {V[i][8]="0";}           		  
       } 
         
 //*********************************** Reading the edges file E, and making vector C and matrix R *****************************************
 
  string E[13];  
  int num=-1; 
  ifstream f2;
  f2.open("E.txt");
  if(!f2)
  {
   cout<<"Unable to open"<<endl;
   exit(1);
  }
        
    while(f2.good())
        {
            getline(f2,line);               
            if(!line.empty())
            {
                istringstream iss(line);                    
                for (k=0;k<=12;k++)
                     
                {
                getline(iss,field,'\t');
                E[k]=field;        
                }
				   
				a++;
                        
                  for(k=1; k<=(b-2); k+=2)
                    {          
                     for (l=k+2; l<=b; l+=2)                                                      
                                                              
                        {                                                             

                           if ((E[0]==V[k][5])&&(E[1]==V[l][5])&&(atoi(E[6].c_str())==atoi(V[k][8].c_str()))&&(atoi(E[7].c_str())==atoi(V[l][8].c_str())))
                                {                                                                                                             
      
                                  num++;                                                            
                                                           
                                    int m=atoi(V[k][7].c_str());                                   
                                    int n=atoi(V[l][7].c_str());
                                    int p=atoi(V[k][8].c_str());                                  
                                    int q=atoi(V[l][8].c_str());
                                    int r=atoi(E[2].c_str());
                                    int s=atoi(E[3].c_str());
                                    int t=atoi(E[4].c_str());
                                    int w=atoi(E[5].c_str());   
                               	   
                                    C[num]= p*r + (1-p)*(m-s) + (1-q)*t + q*(n-w);  

                                    for(int kl = k+2; kl < l; kl+=2)
									{
                                      C[num]+=atoi(V[kl][7].c_str()); 									
                                    } 
									
                                    for(int y = 0; y <=(b-4)/2; y++)
                                    {
                                        if ((y>=k/2)&&(y<=(l-2)/2))
                                        R[num][y]=1.0;
                                        else
                                        R[num][y]=0.0;                       
                                    }                            			
                                    
                                }                              
          
                        }  
					   
                    }  

                                            
                  for(k=1; k<=(b-2); k+=2)
                    {          
                     for (l=k+2; l<=b; l+=2)
                        {                         
                           if ((E[0]==V[l][5])&&(E[1]==V[k][5])&&(atoi(E[6].c_str())+atoi(V[l][8].c_str())==1)&&(atoi(E[7].c_str())+atoi(V[k][8].c_str()))==1)
                                {                                                     
                                  num++;                  
                                                           
                                    int m=atoi(V[k][7].c_str());                                   
                                    int n=atoi(V[l][7].c_str());
                                    int p=atoi(V[k][8].c_str());                                  
                                    int q=atoi(V[l][8].c_str());
                                    int r=atoi(E[2].c_str());
                                    int s=atoi(E[3].c_str());
                                    int t=atoi(E[4].c_str());
                                    int w=atoi(E[5].c_str());   
                               	   
                                    C[num]=p*t + (1-p)*(m-w) + (1-q)*r + q*(n-s);  

									for(int kl = k+2; kl < l; kl+=2)
									{
                                      C[num]+=atoi(V[kl][7].c_str()); 									
                                    }
                                         
                                    for(int y = 0; y <=(b-4)/2; y++)
                                    {
                                        if ((y>=k/2)&&(y<=(l-2)/2))
                                        R[num][y]=1.0;
                                        else
                                        R[num][y]=0.0;                                       
                                    }                                   			
                                    
                                }                                 
          
                        }  
					   
                    } 
                       
            }
			
      
                         
        } 		
    
      f2.close();
      
      int u=num, v=(b-4)/2;
          
      cout<<" # of edges in E : " << a-1 << endl;
      cout<<" # of contigs in the scaffold: " << b/2 << endl;
      cout<<" # of gaps in the scaffold: " << v+1 << endl; 
      cout<<" # of edges contribute into the scaffold: " << u << endl; 
      cout<<"-----------------------------------------------" << endl;
	  
 
 //**************************************************** MATRIX  f (for mean=1395 & std=250) **************************************************  
                                                                         
  float f[v+1]; 
                                                                          
    for(k=0; k<=v; k++)
        {
            f[k]=0.0;             
            for(l=0; l<=u; l++)
            {  
               f[k]+=((C[l]-1395.0)*(R[l][k]))/125.0;                    
            }
	
        }
               
 //**************************************************** MATRIX H1 (for mean=1395 & std=250) **************************************************  
			            
             
  float H1[v+1][v+1]; 
                                                                          
    for(int p=0; p<=v; p++)
        {
       for(int q=0; q<=v; q++)
          { 
             H1[p][q]=0.0;            
            
                          
             for(l=0; l<=u; l++)
             {     
                  H1[p][q]+=((R[l][p])*(R[l][q]))/31250.0;   				  
             }
                    
             
          } 
         
        } 

		  
        int n=v+1;      
       
        cout<<"n="<<n<<endl;                                                                         

 // ***************************************************** QuadProg++ *****************************************************************

  Matrix<double>  G, CE, CI;
  Vector<double>  g0, ce0, ci0, x;
	int  m, p;
	double sum = 0.0;
	char ch;
   G.resize(n, n);
   {
				
				for(int i = 0; i < n; i++)
				  {
                   for(int j = 0; j < n; j++)
                    { 
                      G[i][j] = H1[i][j];
					}
                  } 	
   } 
	
 g0.resize(n);
    {

			for (int j = 0; j < n; j++)
              {
                g0[j] = f[j];  
			  } 
		
	}

  
  m = n;
  CE.resize(n, m);
	{
				 
				for(int i = 0; i < n; i++)
                  for(int j = 0; j < m; j++)
                    CE[i][j] = 0.0;
	} 
	
  
  ce0.resize(m);
	{

		for (int j = 0; j < m; j++)
              ce0[j] = 0.0;	
    }
	
  p = n;
  
  CI.resize(n, p);
  {

  for(int i = 0; i < n; i++)
   for(int j = 0; j < p; j++)
   
    if(i == j)
       {       
        CI[i][j] = 1.0;      
       }
    else
       {
        CI[i][j] = 0.0;
       }

  }
  
  ci0.resize(p);
 {

			for (int j = 0; j < p; j++)
                  {
                  ci0[j] = 0.0;
				  }			
 }

  x.resize(n);
 

 solve_quadprog(G, g0, CE, ce0, CI, ci0, x);
 
//std::cout << "f: " << solve_quadprog(G, g0, CE, ce0, CI, ci0, x) << std::endl; 
//std::cout << "x: " << x << std::endl; 
	
  for (int i = 0; i < n; i++)
  {
    gap[i]=floor(x[i]);
    std::cout << "gap["<<i<<"]="<<gap[i]<< ' ';
	std::cout << std::endl; 
  }
    	
//******************************************************** AGP output *****************************************************************

 for(i = 2; i < b; i+=2)
    { 
   	 
	 std::ostringstream ss;	 
     float aa=atoi(V[i-1][2].c_str())+1;    	 
     ss << aa;
     V[i][1] = ss.str();	 
	 //cout<<"V[i][1]="<<V[i][1]<<endl;
	
	 std::ostringstream tt;	 	 	
	 float bb=atoi(V[i-1][2].c_str())+gap[(i-2)/2];	     	 
     tt << bb;
     V[i][2] = tt.str();
	 //cout<<"V[i][2]="<<V[i][2]<<endl;
	 
	 std::ostringstream uu;
	 float cc=atoi(V[i-1][2].c_str())+gap[(i-2)/2]+1;	 
	 uu << cc;
     V[i+1][1] = uu.str();
	 //cout<<"V[i+1][1]="<<V[i+1][1]<<endl;
	
	 std::ostringstream vv;
	 float dd=atoi(V[i+1][1].c_str())+atoi(V[i+1][7].c_str());	
	 vv << dd;
     V[i+1][2] = vv.str();     
	 //cout<<"V[i+1][2]="<<V[i+1][2]<<endl;	
	} 
	
	   ofstream out;
       out.open("AGP",fstream::app);
       if(!out)
	   {
            cout<<"Unable to open"<<endl;
            exit(1);
       }
	
for(i = 1; i <= b; i++)
     { 
     for(j = 0; j < 9; j++)
	   {
	    out<<V[i][j]<<" ";
	   }
	 out<<endl;
     }
     out.close();	 
}



