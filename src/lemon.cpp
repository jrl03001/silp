#include "lemon.h"
using namespace google;
using namespace lemon;
using namespace std;

/* local definitions */
char const row_delim = '\n';

/* support functions */
void tokenize(string txt, vector<string> & result){
	
	// clear result.
	result.clear();
	
	// define seperator.
	boost::char_separator<char> sep("\t");
	
	// tokenize the line.
	boost::tokenizer<boost::char_separator<char> > tokens(txt, sep);
	
	// add tokens to vector.
	BOOST_FOREACH(string t, tokens){
		result.push_back(t);
	}
}

/* program functions */
void load_nodes(const char * file_path, ListGraph & G, ListGraph::NodeMap<string> & n2idx, sparse_hash_map<string, ListGraph::Node> & idx2n){

	// open the file.
	ifstream fin(file_path);
	if( fin.is_open() == false ){
		cerr << "error: opening query" << endl;
		exit(1);
	}
	
	// loop over lines.
	vector<string> tokens;
	for(string line; getline(fin, line, row_delim); ) {
		
		// tokenize.
		tokenize(line, tokens);
		
		// create node.
		ListGraph::Node n = G.addNode();
		
		// map node.
		n2idx[n] = tokens[0];
		idx2n[tokens[0]] = n;
		
	}

	// close file.
	fin.close();
}

void load_edges(const char * file_path, ListGraph & G, sparse_hash_map<string, ListGraph::Node> & idx2n, ListGraph::EdgeMap<int> & weights){

	// open the file.
	ifstream fin(file_path);
	if( fin.is_open() == false ){
		cerr << "error: opening query" << endl;
		exit(1);
	}
	
	// loop over lines.
	vector<string> tokens;
	for(string line; getline(fin, line, row_delim); ) {
		
		// tokenize.
		tokenize(line, tokens);
		
		// create edge.
		ListGraph::Edge e = G.addEdge(idx2n[tokens[0]], idx2n[tokens[1]]);
		
		// weight the edge.
		weights[e] = atoi(tokens[2].c_str());
	}

	// close file.
	fin.close();
}

vector<pair<string,string> > cardinality(ListGraph & G, ListGraph::NodeMap<string> & n2idx){

	// compute the maximum matching.
	MaxMatching<ListGraph> M(G);
	M.run();
	
	// write matching to file.
	vector<pair<string,string> > results;
	for(ListGraph::NodeIt n(G); n != INVALID; ++n){
		
		// skip no match.
		if( M.mate(n) == INVALID ) continue;
		
		// append to results.
		results.push_back(pair<string,string>(n2idx[n],  n2idx[M.mate(n)]));
		
	}

	// return results.
	return results;
	
}

vector<pair<string,string> > weighted(ListGraph & G, ListGraph::NodeMap<string> & n2idx, ListGraph::EdgeMap<int> & weights){

	// compute the maximum matching.
	MaxWeightedMatching<ListGraph> M(G, weights);
	M.run();
	
	// write matching to file.
	vector<pair<string,string> > results;
	for(ListGraph::NodeIt n(G); n != INVALID; ++n){
		
		// skip no match.
		if( M.mate(n) == INVALID ) continue;
		
		// append to results.
		results.push_back(pair<string,string>(n2idx[n],  n2idx[M.mate(n)]));
		
	}

	// return results.
	return results;
	
}

void write_match(const char * file_path, vector<pair<string,string> > results){

	// open output file.
	ofstream fout(file_path);
	if( fout.is_open() == false ){
		cerr << "error: opening output" << endl;
		exit(1);
	}	
	
	// iterate over results.
	char * buffer = new char[10000];
	for(unsigned int i=0; i<results.size(); i++){
		
		// write the match.
		sprintf(buffer, "%s\t%s\n", results[i].first.c_str(), results[i].second.c_str());
		fout.write(buffer, strlen(buffer));
		
	}
	
	// close file.
	fout.close();
	
	// delete memory.
	delete[] buffer;
	
	
}

/* main function */
int main(int argc, const char * argv[]) {
	
	// sanity check.
	if(argc != 4){
		cerr << "error: bad arguments" << endl;
		exit(1);	
	}
	
	// parse arguments.
	const char * node_file = argv[1];
	const char * edge_file = argv[2];
	const char * out_file = argv[3];
	
	// declare the graph and maps.
	ListGraph G;
	ListGraph::EdgeMap<int> weights(G);
	ListGraph::NodeMap<string> n2idx(G);
	sparse_hash_map<string, ListGraph::Node> idx2n;
	
	// load the graph.
	load_nodes(node_file, G, n2idx, idx2n);
	load_edges(edge_file, G, idx2n, weights);

	// make the match.
	vector<pair<string,string> > results = weighted(G, n2idx, weights);

	// write the match.
	write_match(out_file, results);

	return 0;
}
