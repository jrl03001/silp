/*
 * super_contig.h
 *
 *  Created on: Feb 27, 2012
 *      Author: jlindsay
 */

#ifndef SUPER_CONTIG_H_
#define SUPER_CONTIG_H_

// system
#include <fstream>
#include <iostream>
#include <time.h>
#include <assert.h>
#include <stdlib.h>

// stl
#include <map>
#include <set>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <utility>

// boost
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/dynamic_bitset.hpp>

// HDF5
#include "types.h"

// google hash
#include <google/sparse_hash_map>
#include <google/sparse_hash_set>


#endif /* SUPER_CONTIG_H_ */
