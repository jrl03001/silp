// system
#include <fstream>
#include <iostream>
#include <time.h>
#include <assert.h>
#include <stdlib.h>

// stl
#include <map>
#include <set>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <utility>

// boost
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/dynamic_bitset.hpp>

// HDF5
#include "types.h"

// OGDF
#include <ogdf/basic/Graph.h>
#include <ogdf/basic/GraphAttributes.h>

// google hash
#include <google/sparse_hash_map>
#include <google/sparse_hash_set>

// DEBUG SETTINGS.
#define DEBUG 1
#define VERBOSE 0
#define FASTABUFFER 1000000
char * DEBUGTXT = new char[5012];

using namespace std;
using namespace boost;
using google::sparse_hash_map;
using google::sparse_hash_set;

/*
********** local definitions ************
*/

struct UndirectedPairs {
	int idxa;
	int idxb;
	list<int> elist;
};

class ScaffGraph {
public:
	// the graph.
	ogdf::Graph G;
	
	// index to node/edge vectors.
	vector<ogdf::node> idx2n;
	vector<ogdf::edge> idx2e;
	
	// node/edge to idnex lookup array.
	ogdf::NodeArray<int> n2idx;//(G);
	ogdf::EdgeArray<int> e2idx;//(G);
	
	// pointers to base data.
	NodePair * np_ptr;
	EdgePair * ep_ptr;
	
	// list of undirected edge pairs.
	list<UndirectedPairs> ud_pairs;
	
	// basic constructor.
	ScaffGraph(NodePair * n, EdgePair * p){
		
		// save pointers to base data.
		np_ptr = n;
		ep_ptr = p;
		
		// associate Node/Edge Array with graph.
		n2idx.init(G);
		e2idx.init(G);
	}
	
	// build undirected pairs.
	void make_ud_pairs(){
		
		// clear out list.
		ud_pairs.clear();
		
		// vector to track visited finished pairs.
		sparse_hash_set<int> visited;
		
		// hash to track current pairs.
		sparse_hash_map<int, list<int> > pairs;
		sparse_hash_map<int, list<int> >::iterator pairs_it;
		list<int>::iterator ilist_it;
		
		// loop over each node.
		int idxa, idxb, idxe;
		ogdf::node p, q;
		ogdf::edge e;
		forall_nodes(p, G){
			// simplify.
			idxa = n2idx[p];

			// clear pairs.
			pairs.clear();

			// loop over each neighbor.
			forall_adj_edges(e, p){
				// set q.
				if( e->source() == p ){
					q = e->target();
				} else {
					q = e->source();
				}
				
				// simplify.
				idxb = n2idx[q];
				idxe = e2idx[e];

				// check if this pair has been processed.
				if( visited.find(idxe) != visited.end() ) continue;
				
				// check if this edge is valid.
				if( ep_ptr->first[idxe].invalid == 1 ) continue;
					
				// note its processed.
				visited.insert(idxe);
					
				// add to pairs.
				pairs[idxb].push_back(idxe);
			}
			
			// loop over each key/value pair and insert into list.
			for(pairs_it=pairs.begin(); pairs_it!= pairs.end(); pairs_it++){
				
				// simplify.
				idxb = pairs_it->first;
				
				// create struct.
				UndirectedPairs tmp;
				
				// set index.
				if( idxa < idxb ){
					tmp.idxa = idxa;
					tmp.idxb = idxb;
				} else {
					tmp.idxa = idxb;
					tmp.idxb = idxa;
				}					
				
				// add list.
				tmp.elist = pairs_it->second;
				
				// add this to master list.
				ud_pairs.push_back(tmp);
			}
		}
	}
	
};

typedef vector<boost::dynamic_bitset<> > GenomeBit;

/*
********** helper macros ************
*/





/*
********** helper functions ************
*/

static void WRITE_OUT(const char * txt){
	if( VERBOSE == 0 ) {
		fprintf(stdout,txt);
		fflush(stdout);
	}
}

static void WRITE_ERR(const char * txt){
	fprintf(stderr,txt);
	fflush(stdout);
}

static bool ACTIVITY(int i, int j, int k){
	if(i != 0 && (i % k) == 0){
		if( VERBOSE == 0 ) fprintf(stdout,"%d of %d\n", i, j);
		fflush(stdout);
		if( DEBUG == 0 ) {
			return true;

		}
	}
	return false;
}

static void make_key(int a, int b, char * key){
	if( a < b ){
		sprintf(key, "%d_%d", a, b);
	} else {
		sprintf(key, "%d_%d", b, a);
	}
}

vector<string> tokenize(string txt){
	// create a vector.
	vector<string> result;
	
	// define seperator.
	char_separator<char> sep("\t");
	
	// tokenize the line.
	tokenizer<char_separator<char> > tokens(txt, sep);
	
	// add tokens to vector.
	BOOST_FOREACH(string t, tokens){
		result.push_back(t);
	}
	
	// return everything.
	return result;
}

vector<string> tokenize(string txt, string s){
	// create a vector.
	vector<string> result;
	
	// define seperator.
	char_separator<char> sep(s.c_str());
	
	// tokenize the line.
	tokenizer<char_separator<char> > tokens(txt, sep);
	
	// add tokens to vector.
	BOOST_FOREACH(string t, tokens){
		result.push_back(t);
	}
	
	// return everything.
	return result;
}

/*
********** program functions ************
*/

sparse_hash_map<string, int> make_lookup(NodePair np){

	// create lookup hash.
	sparse_hash_map<string, int> node_lookup;

	// populate from node table.
	for(int i=0; i<np.second; i++){
		
		// save to lookup.
		node_lookup[string(np.first[i].ctg_name)] = np.first[i].node_idx;
	}

	return node_lookup;
}



ScaffGraph build_multi(NodePair * np, EdgePair * ep){

	// create graph object.
	ScaffGraph SG(np, ep);
	
	// add the nodes.
	for(int i=0; i<np->second; i++){
		
		// add node.
		ogdf::node n = SG.G.newNode();
		
		// set lookup.
		SG.idx2n.push_back(n);
		SG.n2idx[n] = i;
	}
	
	// add the edges.
	ogdf::node na, nb;
	for(int i=0; i<ep->second; i++){
		
		// get nodes.
		na = SG.idx2n[ep->first[i].ctg_a_idx];
		nb = SG.idx2n[ep->first[i].ctg_b_idx];
		
		// add edge.
		ogdf::edge e = SG.G.newEdge(na, nb);
		
		// associate with lookup.
		SG.idx2e.push_back(e);
		SG.e2idx[e] = i;
	}
	
	// construct edge pairs.
	SG.make_ud_pairs();
	
	// return the graph.
	return SG;
}

// applies min bound filter.
int min_bound(NodePair * np, EdgePair * ep, int std_thresh_min){
	
	// counter.
	int invalid_cnt = 0;
	
	// vars for simplify.
	int plp, prp, qlp, qrp, i, j, insert_size, std_dev, psz, qsz;
	int min1, min2;
	
	// loop over edges.
	for(int idx=0; idx < ep->second; idx++){
		
		// skip bad.
		if( ep->first[idx].invalid == true ) continue;
		
		// simplify.
		plp = ep->first[idx].read_a_left_pos;
		prp = ep->first[idx].read_a_right_pos;
		qlp = ep->first[idx].read_b_left_pos;
		qrp = ep->first[idx].read_b_right_pos;		
		i = ep->first[idx].ctg_a_idx;
		j = ep->first[idx].ctg_b_idx;
		insert_size = ep->first[idx].insert_size;
		std_dev = ep->first[idx].std_dev;
		
		// node info.
		psz = np->first[i].ctg_width;
		qsz = np->first[j].ctg_width;
		
		// build min.
		if( plp < (psz - prp) ){
			min1 = plp;
		} else {
			min1 = psz - prp;
		}
		
		if( qlp < (qsz - qrp) ){
			min2 = qlp;
		} else {
			min2 = qsz - qrp;
		}
		
		// check min bound.
		if( (min1 + min2) > (insert_size + (std_thresh_min * std_dev)) ){
		
			// mark as invalid.
			ep->first[idx].invalid = 1;
			invalid_cnt++;
		}
	}
	
	return invalid_cnt;
}

// applies state dist bound filter.
int statedist_bound(NodePair * np, EdgePair * ep, int std_thresh_min, int std_thresh_max){
	
	// counter.
	int invalid_cnt = 0;
	
	// vars for simplify.
	int implied_dist, insert_size, std_dev;
	int min1, min2;
	
	// loop over edges.
	for(int eidx=0; eidx < ep->second; eidx++){
		
		// skip bad.
		if( ep->first[eidx].invalid == true ) continue;
		
		// simplify.
		implied_dist = ep->first[eidx].implied_dist;
		insert_size = ep->first[eidx].insert_size;
		std_dev = ep->first[eidx].std_dev;
		
		// upper bound.
		if( implied_dist > (insert_size + (std_dev * std_thresh_max)) ){
			ep->first[eidx].invalid = 1;
			invalid_cnt++;
			continue;
			
		} else if( implied_dist < (-1 * (std_dev * std_thresh_min)) ){
			ep->first[eidx].invalid = 1;
			invalid_cnt++;
			continue;
		}
	}
	
	// return count.
	return invalid_cnt;
}

// applies state type bound filter.
int statetype_bound(ScaffGraph SG){
	
	// counter.
	int invalid_cnt = 0;
	
	// loop over edge pairs.
	int idxa, idxb, idxe;
	int state;
	bool stype, ttype, bad;
	for(list<UndirectedPairs>::iterator it1=SG.ud_pairs.begin(); it1!=SG.ud_pairs.end(); it1++){
		
		// simplify.
		idxa = it1->idxa;
		idxa = it1->idxb;
		
		// bootstrap state.
		bad = false;
		state = SG.ep_ptr->first[it1->elist.front()].implied_state;
		if( state == 0 || state == 3){
			stype = true;
		} else {
			stype = false;
		}
		
		// loop over edges.
		for(list<int>::iterator it2=it1->elist.begin(); it2!=it1->elist.end(); it2++){
			
			// simplify.
			idxe = *it2;
			state = SG.ep_ptr->first[idxe].implied_state;
			
			if( state == 0 || state == 3){
				ttype = true;
			} else {
				ttype = false;
			}
			
			// check if state type changes.			
			if( ttype != stype ){
				bad = true;
			}
		}
		
		// mark edges as bad.
		if( bad == true ){
			for(list<int>::iterator it2=it1->elist.begin(); it2!=it1->elist.end(); it2++){
				if( SG.ep_ptr->first[*it2].invalid != 1 ){
					SG.ep_ptr->first[*it2].invalid = 1;
					invalid_cnt++;
				}
			}
		}
	}
	
	// update adjacent.
	SG.make_ud_pairs();
	
	// return count.
	return invalid_cnt;
}

// applies neighbor bound.
int neighbor_bound(ScaffGraph MG, int max_degree){
	
	// counter.
	int invalid_cnt = 0;
	
	// create set of hash map of neighbors.
	sparse_hash_map<int, sparse_hash_set<int> > nbors;
	
	// loop over all edges and add to it.
	char * tmpkey = new char[100000];
	int keya, keyb;
	for(int i=0; i<MG.ep_ptr->second; i++){
		
		// skip bad ones.
		if( MG.ep_ptr->first[i].invalid == 1 ) continue;
		
		// simplify.
		keya = MG.ep_ptr->first[i].ctg_a_idx;
		keyb = MG.ep_ptr->first[i].ctg_b_idx;
		
		// bootstrap.
		if( nbors.find(keya) == nbors.end() ){
			nbors[keya] =  sparse_hash_set<int>();
		}
		if( nbors.find(keyb) == nbors.end() ){
			nbors[keyb] =  sparse_hash_set<int>();
		}
		
		// add to set.
		nbors[keya].insert(keyb);
		nbors[keyb].insert(keya);

	}

	// mark invalid ones.
	for(int i=0; i<MG.ep_ptr->second; i++){
		
		// skip bad ones.
		if( MG.ep_ptr->first[i].invalid == 1 ) continue;
		
		// simplify.
		keya = MG.ep_ptr->first[i].ctg_a_idx;
		keyb = MG.ep_ptr->first[i].ctg_b_idx;
		
		// check both.
		if( (nbors[keya].size() > max_degree) || (nbors[keyb].size() > max_degree) ){
			MG.ep_ptr->first[i].invalid = 1;
			invalid_cnt++;			
		}
	}

	// return bad cnt;
	return invalid_cnt;
}

// applies bundle bound.
int bundle_bound(ScaffGraph SG, int bundle_size){
	
	// counter.
	int invalid_cnt = 0;
	
	// loop over edge pairs.
	int idxa, idxb, idxe;
	int state;
	bool stype, ttype, bad;
	for(list<UndirectedPairs>::iterator it1=SG.ud_pairs.begin(); it1!=SG.ud_pairs.end(); it1++){
		
		// simplify.
		idxa = it1->idxa;
		idxa = it1->idxb;
		
		// check bundle size.
		bool bad = false;
		if( it1->elist.size() < bundle_size ){
			bad = true;
		}
				
		// mark edges as bad.
		if( bad == true ){
			for(list<int>::iterator it2=it1->elist.begin(); it2!=it1->elist.end(); it2++){
				if( SG.ep_ptr->first[*it2].invalid != 1) {
					SG.ep_ptr->first[*it2].invalid = 1;
					invalid_cnt++;
				}
			}
		}
	}
	
	// update adjacent.
	SG.make_ud_pairs();	
	
	// return bad cnt;
	return invalid_cnt;
}

GenomeBit load_annotation(NodePair np, sparse_hash_map<string, int> nlookup, const char * file_path){
	
	// build bitset arrays.
	GenomeBit genome(np.second);
	for(int i=0; i<np.second; i++){
		genome[np.first[i].node_idx] = boost::dynamic_bitset<> (np.first[i].ctg_width);
	}
	
	// open gff file.
	ifstream myfile(file_path);
	
	// check if file is open.
	if( myfile.is_open() == false ){
		WRITE_ERR("error opening file.\n");
		exit(1);
	}
	// loop over file.
	vector<string> tokens;
	std::string line, ctg;
	int idx, start, stop;
	while ( myfile.good() ) {
		
		// get line.
		getline (myfile,line);
		
		// skip empty lines.
		if(line.length() < 2){
			continue;
		}
		
		// tokenize.
		tokens = tokenize(line);
		
		// grab info.
		ctg = tokens[0];
		start = atoi(tokens[3].c_str());
		stop = atoi(tokens[4].c_str());
			
		// skip non-contig.
		if(nlookup.find(ctg) == nlookup.end()){
			continue;
		}
			
		// translate info.
		idx = nlookup[ctg];
		
		// annotate the genome.
		for(int i=start; i<stop; i++){
			genome[idx][i] = true;
		}
	}
	
	// close file.
	myfile.close();
	
	// return result.
	return genome;
}

int repoverlap_bound(NodePair * np, EdgePair * ep, GenomeBit repeat_ant, float repoverlap_thresh){
	
	// counter.
	int invalid_cnt = 0;
	
	// loop over edges.
	int idxc, start, stop, olap_cnt;
	float olap_perc_a, olap_perc_b, total;
	bool reada, readb;
	for(hsize_t i=0; i < ep->second; i++){
		
		// skip already invalid.
		if( ep->first[i].invalid == 1 ) continue;
		
		// zero trackers.
		reada = false;
		readb = false;
		
		// process reada.
		idxc = ep->first[i].ctg_a_idx;
		start = ep->first[i].read_a_left_pos;
		stop = ep->first[i].read_a_right_pos;
		total = (float) start + (float) stop;
		
		// sum overlaps.
		olap_cnt = 0;
		for(int j=start; j<stop; j++){
			olap_cnt += repeat_ant[idxc][j];
		}
		
		// determine % overlap.
		olap_perc_a = 1.0 - ((float) olap_cnt / total);
		
		// process readb.
		idxc = ep->first[i].ctg_b_idx;
		start = ep->first[i].read_b_left_pos;
		stop = ep->first[i].read_b_right_pos;
		total = (float) start + (float) stop;
		
		// sum overlaps.
		olap_cnt = 0;
		for(int j=start; j<stop; j++){
			olap_cnt += repeat_ant[idxc][j];
		}
		
		// determine % overlap.
		olap_perc_b = 1.0 - ((float) olap_cnt / total);		
		
		// check if eighter crosses threshold.
		if( olap_perc_a < repoverlap_thresh || olap_perc_b < repoverlap_thresh ){
			
			// mark invalid.
			ep->first[i].invalid = 1;
			invalid_cnt++;	
		} 
	}
	
	// return count.
	return invalid_cnt;
}

AgpPair load_agp(const char * file_path){
	
	// create a list of appropriate types.
	list<AgpTable> agps;
	
	// open AGP file.
	ifstream myfile(file_path);
	
	// check if file is open.
	if( myfile.is_open() == false ){
		WRITE_ERR("error opening file.\n");
		exit(1);
	}
	// loop over file.
	vector<string> tokens;
	std::string line, ctg;
	int idx, start, stop;
	while ( myfile.good() ) {
		
		// get line.
		getline (myfile,line);
		
		// skip empty lines.
		if(line.length() < 2){
			continue;
		}
		
		// tokenize.
		tokens = tokenize(line);
		
		// skip runtime line.
		if(tokens[0] == "RUNTIME:") continue;
		
		// prep struct.
		AgpTable tmp;
		
		// copy to struct base on type.
		if( tokens[4] == "W" ){
			strcpy(tmp.scaf_name, tokens[0].c_str());
			tmp.scaf_start = atoi(tokens[1].c_str());
			tmp.scaf_stop = atoi(tokens[2].c_str());
			tmp.scaf_idx = atoi(tokens[3].c_str());
			strcpy(tmp.comp_type, tokens[4].c_str());
			strcpy(tmp.comp_name, tokens[5].c_str());
			tmp.comp_start = atoi(tokens[6].c_str());
			tmp.comp_stop = atoi(tokens[7].c_str());
			if( tokens[8] == "+" ){
				tmp.comp_orien = 0;
			} else {
				tmp.comp_orien = 1;
			}
			tmp.comp_linkage = 0;
			
		} else if( tokens[4] == "N" ){
			strcpy(tmp.scaf_name, tokens[0].c_str());
			tmp.scaf_start = atoi(tokens[1].c_str());
			tmp.scaf_stop = atoi(tokens[2].c_str());
			tmp.scaf_idx = atoi(tokens[3].c_str());
			strcpy(tmp.comp_type, tokens[4].c_str());
			strcpy(tmp.comp_name, tokens[6].c_str());
			tmp.comp_start = 1;
			tmp.comp_stop = atoi(tokens[5].c_str());
			tmp.comp_orien = -1;
			if( tokens[7] == "no" ){
				tmp.comp_linkage = 0;
			} else {
				tmp.comp_linkage = 1;
			}			
			
		} else {
			cout << tokens[5].c_str() << endl;
			cout << line.c_str() << endl;
			WRITE_ERR("shouldn't be here\n");
			exit(1);
		}
		
		// add to list.
		agps.push_back(tmp);
		
	}
	
	// create array from list.
	AgpTable * result = new AgpTable[agps.size()];
	
	// copy shtuff into it.
	hsize_t i =0;
	for( list<AgpTable>::iterator it=agps.begin(); it!=agps.end(); it++){
		
		strcpy(result[i].scaf_name, it->scaf_name);
		result[i].scaf_start = it->scaf_start;
		result[i].scaf_stop = it->scaf_stop;
		result[i].scaf_idx = it->scaf_idx;
		strcpy(result[i].comp_type, it->comp_type);
		strcpy(result[i].comp_name, it->comp_name);
		result[i].comp_start = it->comp_start;
		result[i].comp_stop = it->comp_stop;
		result[i].comp_orien = it->comp_orien;
		result[i].comp_linkage = it->comp_linkage;
		i++;
	}

	// close file.
	myfile.close();
	
	// return pair.
	return AgpPair(result, agps.size());
}

sparse_hash_set<int> good_nodes(AgpPair ap, NodePair np){
	
	// crease bad set.
	sparse_hash_set<int> bad;
	
	// make lookup.
	sparse_hash_map<string, int> nlookup = make_lookup(np);
	
	// add internal contigs to bad.
	bool first, last;
	for(hsize_t i=0; i<ap.second; i++){
		
		// reset.
		first = false;
		last = false;
		
		// check if first.
		if( ap.first[i].scaf_idx == 1 ){
			first = true;
		}
		
		// check if last.
		if( i+1 == ap.second || ( strcmp(ap.first[i].scaf_name, ap.first[i+1].scaf_name) == 0) ){
			last = true;
		}
		
		// add if both are false.
		if( first == false && last == false ){
			bad.insert(nlookup[ap.first[i].comp_name]);
		}
	}
	
	// build good set.
	sparse_hash_set<int> good;
	
	for(hsize_t i=0; i<np.second; i++){
		if( bad.find(np.first[i].node_idx) == bad.end() ){
			good.insert(i);
		}
	}
	
	// return the good set.
	return good;
}

int agpbad_bound(NodePair * np, EdgePair * ep, sparse_hash_set<int> good){
	
	// count.
	int invalid_cnt = 0;
	
	// loop over each edge.
	for(hsize_t i=0; i<ep->second; i++){
		
		// check if edge is bad already.
		if( ep->first[i].invalid == 1 ) continue;
		
		// check if both not in good.
		if( good.find(ep->first[i].ctg_a_idx) == good.end() || good.find(ep->first[i].ctg_b_idx) == good.end() ){
			
			// record.
			ep->first[i].invalid = 1;
			invalid_cnt++;
		}
	}
	
	// return count.
	return invalid_cnt;
}

int agpsize_bound(NodePair * np, EdgePair * ep, AgpPair * ap, sparse_hash_set<int> good){
	
	// count.
	int invalid_cnt = 0;
	
	// make node lookup.
	sparse_hash_map<string, int> nlookup = make_lookup(*np);
	
	// make AGP lookup.
	sparse_hash_map<int, int> alookup;
	for(hsize_t  i=0; i<ap->second; i++){
		if( ap->first[i].comp_type[0] == 'W'){
			alookup[nlookup[string(ap->first[i].comp_name)]] = i;
		}
	}
	
	// loop over each edge.
	int idxa, idxb, agpa, agpb;
	for(hsize_t i=0; i<ep->second; i++){
		
		// check if edge is bad already.
		if( ep->first[i].invalid == 1 ) continue;
		
		// simplify.
		idxa = ep->first[i].ctg_a_idx;
		idxb = ep->first[i].ctg_b_idx;
		agpa = alookup[idxa];
		agpb = alookup[idxb];
			
		// query position read a mapped to.
		if( ap->first[agpa].scaf_idx == 1 ){
			if( ep->first[i].read_a_orien == 0 ){
				ep->first[i].invalid = 1;
				invalid_cnt++;
				continue;		// note we skip the readb test if this failed.
			}
		} else {
			if( ep->first[i].read_a_orien == 1 ){
				ep->first[i].invalid = 1;
				invalid_cnt++;
				continue;		// note we skip the readb test if this failed.
			}
		}
		
		// query position read b mapped to.
		if( ap->first[agpb].scaf_idx == 1 ){
			if( ep->first[i].read_b_orien == 1 ){
				ep->first[i].invalid = 1;
				invalid_cnt++;
				continue;
			}
		} else {
			if( ep->first[i].read_b_orien == 0 ){
				ep->first[i].invalid = 1;
				invalid_cnt++;
				continue;
			}
		}
	}
	
	// return count.
	return invalid_cnt;
}

int agpused_bound(NodePair * np, EdgePair * ep){
	
	// count.
	int invalid_cnt = 0;
	
	// loop over edges.
	for(hsize_t i=0; i<ep->second; i++){
		
		// check if edge is bad already.
		if( ep->first[i].invalid == 1 ) continue;
		
		// check if was used.
		if( ep->first[i].used != -1 ){
			ep->first[i].invalid = 1;
			invalid_cnt++;
		}
	}
	
	// return count.
	return invalid_cnt;
}

sparse_hash_set<int> load_subset(const char * file_path, sparse_hash_map<string, int> nlookup){
	
	// make set for strings.
	sparse_hash_set<int> active;
	
	// open gff file.
	ifstream myfile(file_path);
	
	// check if file is open.
	if( myfile.is_open() == false ){
		WRITE_ERR("error opening file.\n");
		exit(1);
	}
	// loop over file.
	vector<string> tokens;
	std::string line, ctg;
	int idx, start, stop;
	while ( myfile.good() ) {
		
		// get line.
		getline (myfile,line);
		
		// add to set.
		active.insert(atoi(line.c_str()));
	}
	
	// close file.
	myfile.close();	
	
	// return it.
	return active;
}

EdgePair reduce_subset(EdgePair ep, vector<int> * mapback, sparse_hash_set<int> active){
		
	cout << "LOOKING AT " << ep.second << endl;
		
	// loop over edges.
	hsize_t count = 0;
	for(hsize_t i=0; i<ep.second; i++){
		
		cout << "APPLEBEES" << endl;
		
		// skip non double members.
		if( active.find(ep.first[i].ctg_a_idx) == active.end() || active.find(ep.first[i].ctg_b_idx) == active.end() ){
			continue;
		}
		
		// add member to list.
		count++;
		cout << "HIEEET" << endl;
	}
	
	// create the table.
	EdgeTable * table = new EdgeTable[count];
	
	// copy info the table noting idx.
	hsize_t j = 0;
	for(hsize_t i=0; i<ep.second; i++){
		
		// skip non double members.
		if( active.find(ep.first[i].ctg_a_idx) == active.end() || active.find(ep.first[i].ctg_b_idx) == active.end() ){
			continue;
		}
		
		// copy.
		table[j] = ep.first[i];
		j++;
		
		// note.
		mapback->push_back(i);
	}
	
	// return the new pair.
	return EdgePair(table, count);
}

/*
********** driver script ************
*/

int main(int argc, char* argv[]) {
	
	// sanity check.
	if( argc < 2 ){
		fprintf(stderr, "usage: (filtersteps,*) [args] \n");
		fprintf(stderr, "min_bound: applies min bound between pairs\n");
		fprintf(stderr, "state_dist: for each pair, the distance of implied state\n");
		fprintf(stderr, "state_type: for each bundle, filter if more than one type (A,D) or (B,C)\n");
		fprintf(stderr, "neighbor: max degree in bundle graph\n");
		fprintf(stderr, "bundle: set minimum number of pairs per bundle\n");
		fprintf(stderr, "repoverlap: filter if overlaps with repeat\n");
		fprintf(stderr, "agpbad: not incident with degree 1 contigs\n");
		fprintf(stderr, "agpsize: inside an exisiting scaffold\n");
		fprintf(stderr, "agpused: already used in previous iteration\n");
		exit(1);
	}

	// parse the filtering order.
	vector<string> filter_order = tokenize(string(argv[1]), ",");
	
	// prep remaining args.
	const char * node_file = NULL;
	const char * edge_file = NULL;
	const char * annot_file = NULL;
	const char * agp_file = NULL;
	const char * sub_file = NULL;
	int bundle_size = -1;
	int std_thresh_min = -1;
	int std_thresh_max = -1;
	int max_degree = -1;
	int min_dist = -9999999;
	float repoverlap_thresh = -1;
	bool clear = false;

	// parse remaining args.
	for(int i=2; i<argc; i++){
		
		// look for switch.
		if( argv[i][0] == '-' ){
			
			// switch on switches.
			if( strcmp(argv[i], "-nodes") == 0 ){
				node_file = argv[i+1];
				
			} else if( strcmp(argv[i], "-edges") == 0 ){
				edge_file = argv[i+1];
				
			} else if( strcmp(argv[i], "-gff") == 0 ){
				annot_file = argv[i+1];

			} else if( strcmp(argv[i], "-agp") == 0 ){
				agp_file = argv[i+1];
				
			} else if( strcmp(argv[i], "-bundle") == 0 ){
				bundle_size = atoi(argv[i+1]);
			
			} else if( strcmp(argv[i], "-std_min") == 0 ){
				std_thresh_min = atof(argv[i+1]);
					
			} else if( strcmp(argv[i], "-std_max") == 0 ){
				std_thresh_max = atof(argv[i+1]);

			} else if( strcmp(argv[i], "-neighbor") == 0 ){
				max_degree = atoi(argv[i+1]);

			} else if( strcmp(argv[i], "-rep_over") == 0 ){
				repoverlap_thresh = atof(argv[i+1]);
			
			} else if( strcmp(argv[i], "-subset") == 0 ){
				sub_file = argv[i+1];													
																					
			} else if( strcmp(argv[i], "-clear") == 0 ){
				clear = true;													
			}
		}
		
	}

	// required sanity check.
	if( node_file == NULL || edge_file == NULL ){
		WRITE_ERR("nodes or edges not set\n");
		exit(1);		
	}
	
	// load node info.
	WRITE_OUT("load node information\n");
	NodePair np = node_table_load(node_file);
	sparse_hash_map<string, int> nlookup = make_lookup(np);
	
	// load edge info.
	WRITE_OUT("load pair information\n");
	EdgePair ep = edge_table_load(edge_file);
	int total_edges = ep.second;
	int total_filt = 0;

	// zero out invalid.
	for(int i=0; i<total_edges; i++){
		ep.first[i].invalid = 0;
		
		if( clear == true ){
			ep.first[i].used = -1;
		}
	}

	// mark intra-contig pairs invalid.
	for(int i=0; i<total_edges; i++){
		if(	ep.first[i].ctg_a_idx == ep.first[i].ctg_b_idx){
			ep.first[i].invalid = 1;
		}
	}


	// build the multigraph.
	WRITE_OUT("building the multigraph\n");
	ScaffGraph MG = build_multi(&np, &ep);

	// load the AGP.
	AgpPair ap;
	bool agp_loaded = false;
	sparse_hash_set<int> good;

	// call the filters in order.
	for(size_t i=0; i<filter_order.size(); i++){
		
		// check for function.
		if( filter_order[i] == "min_bound" ){
			
			// sanity.
			if( std_thresh_min == -1 ){
				WRITE_ERR("std_min not set\n");
				exit(1);
			}
			
			// min bound on edge.
			int mb_cnt = min_bound(&np, &ep, std_thresh_min);
			sprintf(DEBUGTXT, "min_bound: %d of %d\n", mb_cnt, total_edges);
			WRITE_OUT(DEBUGTXT);
			total_filt += mb_cnt;

		} else if( filter_order[i] == "agpused" ){
			
			// sanity.
			if( agp_file == NULL ){
				WRITE_ERR("agp file or repoverlap not set\n");
				exit(1);
			}
			
			// load agp if necessary.
			if( agp_loaded == false ){
				
				// load agp and process it.
				WRITE_OUT("loading AGP file\n");
				ap = load_agp(agp_file);
				good = good_nodes(ap, np);
				agp_loaded = true;
			}
		
			// filter edges already used.
			int au_cnt = agpused_bound(&np, &ep);
			sprintf(DEBUGTXT, "agpused_bound: %d of %d\n", au_cnt, total_edges);
			WRITE_OUT(DEBUGTXT);
			total_filt += au_cnt;

		} else if( filter_order[i] == "agpsize" ){
			
			// sanity.
			if( agp_file == NULL ){
				WRITE_ERR("agp file or repoverlap not set\n");
				exit(1);
			}
			
			// load agp if necessary.
			if( agp_loaded == false ){
				
				// load agp and process it.
				WRITE_OUT("loading AGP file\n");
				ap = load_agp(agp_file);
				good = good_nodes(ap, np);
				agp_loaded = true;
			}
		
			// filter edges with impossible results.
			int as_cnt = agpsize_bound(&np, &ep, &ap, good);
			sprintf(DEBUGTXT, "agpsize_bound: %d of %d\n", as_cnt, total_edges);
			WRITE_OUT(DEBUGTXT);
			total_filt += as_cnt;

		} else if( filter_order[i] == "agpbad" ){
			
			// sanity.
			if( agp_file == NULL ){
				WRITE_ERR("agp file or repoverlap not set\n");
				exit(1);
			}
			
			// load agp if necessary.
			if( agp_loaded == false ){
				
				// load agp and process it.
				WRITE_OUT("loading AGP file\n");
				ap = load_agp(agp_file);
				good = good_nodes(ap, np);
				agp_loaded = true;
			}
		
			// filter any edges not containing good.
			int ab_cnt = agpbad_bound(&np, &ep, good);
			sprintf(DEBUGTXT, "agpbad_bound: %d of %d\n", ab_cnt, total_edges);
			WRITE_OUT(DEBUGTXT);
			total_filt += ab_cnt;

		} else if( filter_order[i] == "repoverlap" ){
			
			// sanity.
			if( annot_file == NULL || repoverlap_thresh == -1 ){
				WRITE_ERR("agp file or repoverlap not set\n");
				exit(1);
			}
			
			// load annotation info.
			WRITE_OUT("load annotation information\n");
			GenomeBit repeat_ant = load_annotation(np, nlookup, annot_file);
			
			// repeat overlap.
			int rob_cnt = repoverlap_bound(&np, &ep, repeat_ant, repoverlap_thresh);
			sprintf(DEBUGTXT, "repoverlap_bound: %d of %d\n", rob_cnt, total_edges);
			WRITE_OUT(DEBUGTXT);
			total_filt += rob_cnt;

	
		} else if( filter_order[i] == "bundle" ){
			
			// sanity.
			if( bundle_size == -1 ){
				WRITE_ERR("max_degree not set\n");
				exit(1);
			}
			
			// bundle bound on adjacent pairs.
			int bb_cnt = bundle_bound(MG, bundle_size);
			sprintf(DEBUGTXT, "bundle_bound: %d of %d\n", bb_cnt, total_edges);
			WRITE_OUT(DEBUGTXT);
			total_filt += bb_cnt;
	
		} else if( filter_order[i] == "neighbor" ){
			
			// sanity.
			if( max_degree == -1 ){
				WRITE_ERR("max_degree not set\n");
				exit(1);
			}
			
			// neighbor bound.
			int nb_cnt = neighbor_bound(MG, max_degree);
			sprintf(DEBUGTXT, "neighbor_bound: %d of %d\n", nb_cnt, total_edges);
			WRITE_OUT(DEBUGTXT);
			total_filt += nb_cnt;
	
		} else if( filter_order[i] == "state_type" ){
			
			// state type bound on edges.
			int stb_cnt = statetype_bound(MG);
			sprintf(DEBUGTXT, "statetype_bound: %d of %d\n", stb_cnt, total_edges);
			WRITE_OUT(DEBUGTXT);
			total_filt += stb_cnt;
			
		} else if( filter_order[i] == "state_dist" ){
			
			// sanity.
			if( std_thresh_min == -1 || std_thresh_max == -1 ){
				WRITE_ERR("std_min or max not set\n");
				exit(1);
			}
			
			// state dist bound on edges.
			int sdb_cnt = statedist_bound(&np, &ep, std_thresh_min, std_thresh_max);
			sprintf(DEBUGTXT, "statedist_bound: %d of %d\n", sdb_cnt, total_edges);
			WRITE_OUT(DEBUGTXT);
			total_filt += sdb_cnt;
			
		} else if( filter_order[i] == "blank" || filter_order[i] == "subset"){
			continue;
			
		} else {
			sprintf(DEBUGTXT, "filter is not valid: %s\n", filter_order[i].c_str());
			WRITE_ERR(DEBUGTXT);
			exit(1);			
		}
		
	}

	// sanity check.
	if( total_filt > total_edges ){
		sprintf(DEBUGTXT, "error: too many edges were filtered!: %d of %d\n", total_filt, total_edges);
		WRITE_ERR(DEBUGTXT);
		exit(1);		
	} else if( total_filt == total_edges ){
		WRITE_ERR("error: all edges were filtered out!\n");
		exit(1);
	}
	
	// save edges back to file.
	sprintf(DEBUGTXT, "filtered %d of %d edges table to disk\n", total_filt, total_edges);
	WRITE_OUT(DEBUGTXT);
	edge_table_save(ep.first, ep.second, "edges", edge_file);
	
	// note we finished properly.
	if( VERBOSE == 0 ) printf("finished properly\n");
	return 0;
}
