#include "super_contig.h"

using namespace std;
using namespace boost;
using google::sparse_hash_map;
using google::sparse_hash_set;

/*
********** local definitions ************
*/
char const row_delim = '\n';

typedef sparse_hash_map<string, vector<AgpTable> > ScafAgp;
typedef sparse_hash_map<string, string> CtgScaf;

/*
********** helper functions ************
*/

void tokenize(string txt, vector<string> & result){
	
	// clear result.
	result.clear();
	
	// define seperator.
	boost::char_separator<char> sep("\t");
	
	// tokenize the line.
	boost::tokenizer<boost::char_separator<char> > tokens(txt, sep);
	
	// add tokens to vector.
	BOOST_FOREACH(string t, tokens){
		result.push_back(t);
	}
}

sparse_hash_map<string, long> make_lookup(NodePair np){

	// create lookup hash.
	sparse_hash_map<string, long> node_lookup;

	// populate from node table.
	for(int i=0; i<np.second; i++){
		
		// save to lookup.
		node_lookup[string(np.first[i].ctg_name)] = np.first[i].node_idx;
	}

	return node_lookup;
}

AgpPair load_agp(const char * file_path){
	
	// create a list of appropriate types.
	list<AgpTable> agps;
	
	// open AGP file.
	ifstream fin(file_path);
	
	// check if file is open.
	if( fin.is_open() == false ){
		cerr << "error opening file: " << file_path << endl;
		exit(1);
	}
	
	// loop over file.
	std::string ctg;
	int idx, start, stop;
	vector<string> tokens;
	for(string line; getline(fin, line, row_delim); ) {
		
		// skip empty lines.
		if(line.length() < 2){
			continue;
		}
		
		// tokenize.
		tokenize(line, tokens);
		
		// skip runtime line.
		if(tokens[0] == "RUNTIME:") continue;
		
		// prep struct.
		AgpTable tmp;
		
		// copy to struct base on type.
		if( tokens[4] == "W" ){
			strcpy(tmp.scaf_name, tokens[0].c_str());
			tmp.scaf_start = atoi(tokens[1].c_str());
			tmp.scaf_stop = atoi(tokens[2].c_str());
			tmp.scaf_idx = atoi(tokens[3].c_str());
			strcpy(tmp.comp_type, tokens[4].c_str());
			strcpy(tmp.comp_name, tokens[5].c_str());
			tmp.comp_start = atoi(tokens[6].c_str());
			tmp.comp_stop = atoi(tokens[7].c_str());
			if( tokens[8] == "+" ){
				tmp.comp_orien = 0;
			} else {
				tmp.comp_orien = 1;
			}
			tmp.comp_linkage = 0;
			
		} else if( tokens[4] == "N" ){
			strcpy(tmp.scaf_name, tokens[0].c_str());
			tmp.scaf_start = atoi(tokens[1].c_str());
			tmp.scaf_stop = atoi(tokens[2].c_str());
			tmp.scaf_idx = atoi(tokens[3].c_str());
			strcpy(tmp.comp_type, tokens[4].c_str());
			strcpy(tmp.comp_name, tokens[6].c_str());
			tmp.comp_start = 1;
			tmp.comp_stop = atoi(tokens[5].c_str());
			tmp.comp_orien = -1;
			if( tokens[7] == "no" ){
				tmp.comp_linkage = 0;
			} else {
				tmp.comp_linkage = 1;
			}			
			
		} else {
			cout << tokens[5].c_str() << endl;
			cout << line.c_str() << endl;
			cerr << "shouldn't be here" << endl;
			exit(1);
		}
		
		// add to list.
		agps.push_back(tmp);
		
	}
	
	// create array from list.
	AgpTable * result = new AgpTable[agps.size()];
	
	// copy shtuff into it.
	hsize_t i =0;
	for( list<AgpTable>::iterator it=agps.begin(); it!=agps.end(); it++){
		
		strcpy(result[i].scaf_name, it->scaf_name);
		result[i].scaf_start = it->scaf_start;
		result[i].scaf_stop = it->scaf_stop;
		result[i].scaf_idx = it->scaf_idx;
		strcpy(result[i].comp_type, it->comp_type);
		strcpy(result[i].comp_name, it->comp_name);
		result[i].comp_start = it->comp_start;
		result[i].comp_stop = it->comp_stop;
		result[i].comp_orien = it->comp_orien;
		result[i].comp_linkage = it->comp_linkage;
		i++;
	}

	// close file.
	fin.close();
	
	// return pair.
	return AgpPair(result, agps.size());
}

long determine_state(long idxa, long idxb, long oriena, long orienb){
	long state = -1;
	if( idxa < idxb ){
		if( oriena == 0 && orienb == 0 ){
			state = 0;
		} else if( oriena == 0 && orienb == 1 ){
			state = 1;
		} else if( oriena == 1 && orienb == 0 ){
			state = 2;
		} else if( oriena == 1 && orienb == 1 ){
			state = 3;
		}
	} else {
		if( oriena == 1 && orienb == 1 ){
			state = 0;
		} else if( oriena == 0 && orienb == 1 ){
			state = 1;
		} else if( oriena == 1 && orienb == 0 ){
			state = 2;
		} else if( oriena == 0 && orienb == 0 ){
			state = 3;
		}		
	}
	return state;
}

/*
********** program functions ************
*/

ScafAgp filter_agps(AgpPair ap){
	
	// count each scaffold.
	sparse_hash_map<string, int> cnts;
	
	// loop over agp table.
	for(hsize_t i=0; i<ap.second; i++){
	
		// save scaf name as string.
		string scaf_name = string(ap.first[i].scaf_name);	
		
		// check if present.
		if( cnts.find(scaf_name) == cnts.end() ){
			cnts[scaf_name] = 0;
		}
		
		// count it.
		cnts[scaf_name]++;
	}
	
	// build a map of scaf name to agp.
	ScafAgp valid_agps;
	
	// loop over agp table.
	for(hsize_t i=0; i<ap.second; i++){
	
		// save scaf name as string.
		string scaf_name = string(ap.first[i].scaf_name);	
		
		// skip small.
		if( cnts[scaf_name] < 2 ) continue;
		
		// skip gaps.
		if( strcmp(ap.first[i].comp_type, "W") == 0 ) continue;
		
		// check if present.
		if( cnts.find(scaf_name) == cnts.end() ){
			valid_agps[scaf_name] = vector<AgpTable>();
		}	
		
		// store it.
		valid_agps[scaf_name].push_back(ap.first[i]);
	}
	
	// return the vector.
	return valid_agps;
	
}

CtgScaf ctgscaf_map(AgpPair ap){
	
	// create map.
	CtgScaf cs_map;
	
	// iterate over agps.
	for(hsize_t i=0; i<ap.second; i++){
		
		// skip gaps.
		if( strcmp(ap.first[i].comp_type, "W") == 0 ) continue;
		
		// do map.
		cs_map[string(ap.first[i].comp_name)] = string(ap.first[i].scaf_name);
	
	}
	
	// return map.
	return cs_map;
	
}

NodePair new_node_table(const NodePair np, ScafAgp valid_agps){
	
	// count the number of scaffolded nodes.
	hsize_t cnt = 0;
	for(ScafAgp::iterator it=valid_agps.begin(); it!=valid_agps.end(); it++){
		cnt += it->second.size();
	}
		
	// copy the singelton data.
	list<NodeTable> tmps;
	for(hsize_t i=0; i<np.second; i++){
		
		// skip if in valid.
		if( valid_agps.find(string(np.first[i].ctg_name)) != valid_agps.end() ) continue;
		
		// create the entry.
		NodeTable tmp;
		
		// copy it explicitly.
		tmp.node_idx = -1;
		strcpy(tmp.ctg_name, string(np.first[i].ctg_name).c_str());
		tmp.ctg_width = np.first[i].ctg_width;
		tmp.ctg_orien = np.first[i].ctg_orien;
		tmp.ctg_order = np.first[i].ctg_order;
		tmp.invalid = np.first[i].invalid;		
				
		// add to list.
		tmps.push_back(tmp);
	}
	
	// copy the super contig data.
	for(ScafAgp::iterator it=valid_agps.begin(); it!=valid_agps.end(); it++){

		// create the entry.
		NodeTable tmp;
		
		// fill in entry.
		tmp.node_idx = -1;
		strcpy(tmp.ctg_name, it->first.c_str());
		tmp.ctg_width = it->second.back().scaf_stop - 1;
		tmp.ctg_orien = 0;
		tmp.ctg_order = 0;
		tmp.invalid = 0;
		
		// add to list.
		tmps.push_back(tmp);
	}
	
	
	
	// create a table of that size.
	NodeTable * table = new NodeTable[tmps.size()];
	
	// loop over list.
	hsize_t i = 0;
	for(list<NodeTable>::iterator it=tmps.begin(); it!= tmps.end(); it++){
		
		// copy over.
		table[i].node_idx = i;
		strcpy(table[i].ctg_name, it->ctg_name);
		table[i].ctg_width = it->ctg_width;
		table[i].ctg_orien = it->ctg_orien;
		table[i].ctg_order = it->ctg_order;
		table[i].invalid = it->invalid;		
	}
	
	// return pair.
	return NodePair(table, cnt);
}

/*
********** driver script ************
*/

int main(int argc, char* argv[]) {
	
	// sanity check.
	if( argc < 2 ){
		fprintf(stderr, "wrong number of arguments\n");
		exit(1);
	}

	// parse args.
	const char * old_node_file = argv[1];
	const char * old_edge_file = argv[2];
	const char * agp_file = argv[3];
	const char * new_node_file = argv[4];
	const char * new_edge_file = argv[5];	
	
	// load node info.
	cout << "load node information" << endl;
	NodePair np = node_table_load(old_node_file);
	sparse_hash_map<string, long> nlookup = make_lookup(np);
	
	// load edge info.
	cout << "load pair information" << endl;
	EdgePair ep = edge_table_load(old_edge_file);
	
	// load the agp info.
	cout << "load agp information" << endl;
	AgpPair ap = load_agp(agp_file);
	
	// identify the important scaffolds.
	cout << "finding large scaffolds" << endl;
	sparse_hash_map<string, vector<AgpTable> > valid_agps = filter_agps(ap);
	
	// create the node map.
	cout << "creating ctg to scaf map" << endl;
	CtgScaf cs_map = ctgscaf_map(ap);
	
	// create the new node table.
	cout << "creating new node table" << endl;
	NodePair new_np = new_node_table(np, valid_agps);
	

	// save the node table.
	node_table_save(new_np.first, new_np.second, "nodes", new_node_file);

	// note we finished properly.
	cout << "finished properly" << endl;
	return 0;
}
