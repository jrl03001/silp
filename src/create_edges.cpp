// system
#include <fstream>
#include <iostream>
#include <time.h>
#include <assert.h>
#include <stdlib.h>

// stl
#include <map>
#include <set>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <utility>

// boost
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/dynamic_bitset.hpp>

// samtools
#include "sam.h"

// HDF5
#include "types.h"

// google hash
#include <google/sparse_hash_map>

// DEBUG SETTINGS.
#define DEBUG 1
#define VERBOSE 0

using namespace std;
using namespace boost;
using google::sparse_hash_map;

/*
********** local definitions ************
*/

/*
********** helper functions ************
*/

static void stdwrite(const char * txt){
	if( VERBOSE == 0 ) fprintf(stdout,txt);
}

static bool activity(int i, int j){
	if(i != 0 && (i % j) == 0){
		if( VERBOSE == 0 ) fprintf(stdout,".");
		fflush(stdout);
		if( DEBUG == 0 ) {
			return true;
		}
	}
	return false;
}

#define bam1_unmapped(b) (((b)->core.flag & BAM_FUNMAP) != 0)

/*
********** program functions ************
*/

sparse_hash_map<string, long> make_lookup(pair<NodeTable *, hsize_t> nodes){

	// create lookup hash.
	sparse_hash_map<string, long> node_lookup;

	// populate from node table.
	for(long i=0; i<nodes.second; i++){
		
		// save to lookup.
		node_lookup[string(nodes.first[i].ctg_name)] = nodes.first[i].node_idx;
	}

	return node_lookup;
}

long determine_state(EdgeTable e){
	long state = -1;
	if( e.ctg_a_idx < e.ctg_b_idx ){
		if( e.read_a_orien == 0 && e.read_b_orien == 0 ){
			state = 0;
		} else if( e.read_a_orien == 0 && e.read_b_orien == 1 ){
			state = 1;
		} else if( e.read_a_orien == 1 && e.read_b_orien == 0 ){
			state = 2;
		} else if( e.read_a_orien == 1 && e.read_b_orien == 1 ){
			state = 3;
		}
	} else {
		if( e.read_a_orien == 1 && e.read_b_orien == 1 ){
			state = 0;
		} else if( e.read_a_orien == 0 && e.read_b_orien == 1 ){
			state = 1;
		} else if( e.read_a_orien == 1 && e.read_b_orien == 0 ){
			state = 2;
		} else if( e.read_a_orien == 0 && e.read_b_orien == 0 ){
			state = 3;
		}		
	}
	return state;
}

long determine_dist(EdgeTable e, long state, long n1_width, long n2_width){

	long d1, d2;
	if( e.ctg_a_idx < e.ctg_b_idx ){
		if( state == 0 ){
			d1 = n1_width - e.read_a_left_pos;
			d2 = e.read_b_left_pos;
		} else if( state == 1 ){
			d1 = n1_width - e.read_a_right_pos;
			d2 = n2_width - e.read_b_right_pos;
		} else if( state == 2 ){
			d1 = e.read_a_left_pos;
			d2 = e.read_b_left_pos;
		} else if( state == 3 ){
			d1 = e.read_a_left_pos;
			d2 = n2_width - e.read_b_right_pos;
		}
	} else {
		if( state == 0 ){
			d1 = e.read_a_left_pos;
			d2 = n2_width - e.read_b_right_pos;
		} else if( state == 1 ){
			d1 = n1_width - e.read_a_right_pos;
			d2 = n2_width - e.read_b_right_pos;
		} else if( state == 2 ){
			d1 = e.read_a_left_pos;
			d2 = e.read_b_left_pos;
		} else if( state == 3 ){
			d1 = n1_width - e.read_a_right_pos;
			d2 = e.read_b_left_pos;
		}	
	}
	
	
	// return result.
	return e.insert_size - d1 - d2;
}

pair<EdgeTable *, long> build_edges(pair<NodeTable *, hsize_t> nodes, sparse_hash_map<string, long> lookup, const char * sam1_file, const char * sam2_file, long insert_size, long std_dev){
	
	// create list to store info.
	list<EdgeTable> edges;
	
	// open the BAM in file.
	samfile_t * samin1 = samopen(sam1_file, "r", 0);
	if( samin1 == 0 ){
		fprintf(stderr, "failed to load SAM file: %s", sam1_file);
		exit(1);
	}
	
	samfile_t * samin2 = samopen(sam2_file, "r", 0);
	if( samin2 == 0 ){
		fprintf(stderr, "failed to load SAM file: %s", sam2_file);
		exit(1);
	}
	
	// allocate a bam object array.
	bam1_t * b1 = bam_init1();
	bam1_t * b2 = bam_init1();
	
	long total = 0;
	long bad = 0;
	
	// loop over alignments.
	long state, dist, n1_width, n2_width;
	long status1 = samread(samin1, b1);  
	long status2 = samread(samin2, b2);  
	while( status1 > 0 && status2 > 0 ){

		// build struct.
		EdgeTable tmp;
		tmp.ctg_a_idx = lookup[string(samin1->header->target_name[b1->core.tid])];
		tmp.ctg_b_idx = lookup[string(samin2->header->target_name[b2->core.tid])];
		
		tmp.read_a_left_pos = b1->core.pos;
		tmp.read_a_right_pos = b1->core.pos + b1->core.l_qseq;
		tmp.read_b_left_pos = b2->core.pos;
		tmp.read_b_right_pos = b2->core.pos + b2->core.l_qseq;		
	
		if( b1->core.flag != BAM_FREVERSE ) tmp.read_a_orien = 0;
		else tmp.read_a_orien = 1;
		if( b2->core.flag != BAM_FREVERSE ) tmp.read_b_orien = 0;
		else tmp.read_b_orien = 1;
		
		tmp.insert_size = insert_size;
		tmp.std_dev = std_dev;
		tmp.invalid = 0;
		tmp.used = -1;
		
		// skip if to same node.
		if( tmp.ctg_a_idx != tmp.ctg_b_idx ) {
		
			// determine state.
			state = determine_state(tmp);
			tmp.implied_state = state;
			
			if(state == -1){
				fprintf(stderr,"BAD STATE!\n");
				exit(1);
			}
			
			// determine dist.
			n1_width = nodes.first[lookup[bam1_qname(b1)]].ctg_width;
			n2_width = nodes.first[lookup[bam1_qname(b2)]].ctg_width;
			tmp.implied_dist = determine_dist(tmp, state, n1_width, n2_width);
			
			// count.
			total++;
			if( tmp.implied_dist < (-3 * std_dev) ){
				bad++;
			}
			
			// add to list.
			edges.push_back(tmp);
		}
	
		// update.
		status1 = samread(samin1, b1);  
		status2 = samread(samin2, b2);  
	}
	
	// close file.
	samclose(samin1);
	samclose(samin2);

	fprintf(stdout, "total: %d\tbad: %d\n", total, bad);

	// copy these to a table.
	EdgeTable * edge_table = new EdgeTable[edges.size()];
	long i = 0;
	for(list<EdgeTable>::iterator it = edges.begin(); it != edges.end(); it++){
		edge_table[i] = *it;
		i++;
	}
	
	// return table.
	return pair<EdgeTable *, long>(edge_table, edges.size());
}

/*
********** driver script ************
*/

int main(int argc, char* argv[]) {
	
	// sanity check.
	if( argc < 2 ){
		fprintf(stderr, "wrong number of arguments\n");
		exit(1);
	}

	// parse args.
	const char * node_file = argv[1];
	const char * sam1_file = argv[2];
	const char * sam2_file = argv[3];
	const char * edge_file = argv[4];
	long insert_size = atoi(argv[5]);
	long std_dev = atoi(argv[6]);
	

	// load node info.
	stdwrite("load node information\n");
	pair<NodeTable *, hsize_t> nodes = node_table_load(node_file);
	sparse_hash_map<string, long> lookup = make_lookup(nodes);

	// build edge table.
	pair<EdgeTable *, long> edge_table = build_edges(nodes, lookup, sam1_file, sam2_file, insert_size, std_dev);

	// save edge table.
	edge_table_save(edge_table.first, edge_table.second, "edges", edge_file);

	// note we finished properly.
	if( VERBOSE == 0 ) printf("finished properly\n");
	return 0;
}
