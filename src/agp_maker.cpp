// system
#include <fstream>
#include <iostream>
#include <time.h>
#include <assert.h>
#include <stdlib.h>
#include <bitset>

// stl
#include <map>
#include <set>
#include <string>
#include <vector>
#include <list>
#include <utility>

// boost
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/dynamic_bitset.hpp>

// google hash
#include <google/sparse_hash_map>
#include <google/sparse_hash_set>

// samtools
#include "sam.h"

// interval tree
#include "IntervalTree.h"

// data types
#include "types.h"

// DEBUG SETTINGS.
#define DEBUG 1
#define VERBOSE 0
#define bam1_unmapped(b) (((b)->core.flag & BAM_FUNMAP) != 0)

using namespace std;
using namespace tr1;
using namespace boost;
using namespace google;
/*
********** local definitions ************
*/

typedef struct ScafMap {
	char * chr;
	int start;
	int stop;
	int orien;
} ScafMap;

typedef struct ReadMap {
	char * qname;
	char * rname;
	int tid;
	int start;
	int stop;
	int orien;
} ReadMap;


typedef map<string, ScafMap> CtgChrMap;


/*
********** helper functions ************
*/
vector<string> tokenize(string txt){
	// create a vector.
	vector<string> result;
	
	// define seperator.
	char_separator<char> sep("\t");
	
	// tokenize the line.
	tokenizer<char_separator<char> > tokens(txt, sep);
	
	// add tokens to vector.
	BOOST_FOREACH(string t, tokens){
		result.push_back(t);
	}
	
	// return everything.
	return result;
}

static void stdwrite(const char * txt){
	if( VERBOSE == 0 ) fprintf(stdout,txt);
}

static bool activity(int i, int j){
	if(i != 0 && (i % j) == 0){
		fprintf(stdout,".");
		fflush(stdout);
		if( DEBUG == 0 ) {
			return true;
		}
	}
	return false;
}

bool compare_rname(const ReadMap &a, const ReadMap &b) {
	int c = strcmp(a.qname, b.qname);
	if( c <= 0 ){
		return true;
	} else {
		return false;
	}
}


bool compare_qname(const ReadMap &a, const ReadMap &b) {
	int c = strcmp(a.qname, b.qname);
	if( c <= 0 ){
		return true;
	} else {
		return false;
	}
}

bool compare_rname_start(const ReadMap &a, const ReadMap &b) {
	// non-equal rname.
	int c = strcmp(a.rname, b.rname);
	if( c != 0 ){
		if( c <= 0 ){
			return true;
		} else {
			return false;
		}
	} 
	
	// equal tid.
	return a.start < b.start;
}
struct eqstr { bool operator()(const char* s1, const char* s2) const { return (s1 == s2) || (s1 && s2 && strcmp(s1, s2) == 0); } };

typedef struct ListMap {
	list<ReadMap> * reads;
	sparse_hash_map<string, ScafMap> * lookup;
	bam_header_t * header;
} ListMap;

/*
********** program functions ************
*/

// creates a filter for 
sparse_hash_set<string> make_contig_filter(const char * file_name){

	// create hash.
	sparse_hash_set<string> valid;

	// create buffer.
	string buffer;

	// parse out contigs.
	ifstream infile;
	infile.open(file_name);
	while(!infile.eof()) {
		
		// load line into buffer.
		getline(infile,buffer); 
		
		// add buffer to set.
		valid.insert(string(buffer.c_str()));
	}
	infile.close();
	
	// return hash.
	return valid;
}

// stores agp info by contig.
sparse_hash_map<string, ScafMap> agplookup(const char * file_name){
	
	// create hash.
	sparse_hash_map<string, ScafMap> scaflookup;

	// create buffer.
	string buffer;
	vector<string> tokens;

	// parse out contigs.
	ifstream infile;
	infile.open(file_name);
	while(!infile.eof()) {
		
		// load line into buffer.
		getline(infile, buffer); 
		
		// tokenize it.
		tokens = tokenize(buffer);
		
		// skip gaps.
		if(tokens[4].compare("N") == 0) continue;
		
		// create ScafMap.
		ScafMap tmp;
		
		// copy read into it.
		const char * ap = tokens[0].c_str();
		tmp.chr = new char[strlen(ap)];
		strcpy(tmp.chr, ap);
		
		// copy integers.
		tmp.start = atoi(tokens[1].c_str());
		tmp.stop = atoi(tokens[2].c_str());
		if( tokens[8].compare("+") == 0 ){
			tmp.orien = 0;
		} else {
			tmp.orien = 1;
		}
		
		// add to map.
		scaflookup[tokens[5]] = tmp;
	}
	infile.close();
	
	// return hash.
	return scaflookup;
	
}


// loads required info from BAM file.
list<ReadMap> load_bam(samfile_t * bamin, bam_index_t * bamidx, sparse_hash_map<string, ScafMap> scaflookup, sparse_hash_set<string> valid){

	// create list for alignments.
	list<ReadMap> alignments;
	
	// allocate a bam object.
	bam1_t * b = new bam1_t();
	
	// loop over alignments.
	string qname;
	int idx = 0;
	int str_size;
	int status = bam_read1(bamin->x.bam, b);  
	while( status > 0 ){
		
		// get rname.valid.find(rname) == valid.end()
		qname = string(bam1_qname(b));
		
		// skip unmapped and invalid.
		if( (bam1_unmapped(b) == false) && (valid.find(qname) != valid.end()) ) {

			// create the ReadMap.
			ReadMap tmp;

			// copy the read name.
			tmp.qname = new char[strlen(bam1_qname(b))];
			strcpy(tmp.qname, bam1_qname(b));

			// copy the reference name.
			tmp.rname = new char[strlen(scaflookup[bamin->header->target_name[b->core.tid]].chr)];
			strcpy(tmp.rname, scaflookup[bamin->header->target_name[b->core.tid]].chr);
			
			// copy the tid.
			tmp.tid = b->core.tid;
			
			// copy the start / stop.
			tmp.start = scaflookup[bamin->header->target_name[b->core.tid]].start + b->core.pos + 1;
			tmp.stop = scaflookup[bamin->header->target_name[b->core.tid]].start + b->core.pos + b->core.l_qseq + 1;
			
			// copy the original orientation.
			if( b->core.flag != BAM_FREVERSE ){
				tmp.orien = 0;
			} else {
				tmp.orien = 1;
			}
			
			// append this to the list.
			alignments.push_back(tmp);
			
			//cout << tmp.qname  << " " << tmp.rname << " " << tmp.start << " " << tmp.stop << endl;
			

		} 
		
		// update things.
		status = bam_read1(bamin->x.bam, b); 
		idx++;
		
		// activity.
		if( activity(idx, 100000) == true ) break;
	}

	// pretty print.
	stdwrite("\n");
	
	
	// return list.
	return alignments;
}

// removes non-unique alignments.
void non_unique(list<ReadMap> * alignments){
	
	// sort the alignments by name.
	alignments->sort(compare_qname);
	
	// look for sequential names.
	list<ReadMap>::iterator pit = alignments->begin();
	list<ReadMap>::iterator cit = alignments->begin();
	cit++;
	
	// loop over list.
	while( pit != alignments->end() ){
			
		// check next.
		if( strcmp(cit->qname,pit->qname) == 0 ){
			
			// remove it if same.
			pit = alignments->erase(pit);
			
		} else {
			// otherwise just increment it.
			pit++;
		}
		
		// increment next.
		cit++;
		
		// end case.
		if( cit == alignments->end() ){
			cit--;
		}
	}
}

// removes non-unique alignments.
list<ReadMap> overlapping(list<ReadMap> alignments, int window){
	
	// sort by rname.
	alignments.sort(compare_rname_start);
	
	// create vector for intervals.
	vector<Interval<ReadMap> > intervals;
	
	// create vector for overlaps.
	vector<Interval<ReadMap> > overlaps;
	vector<Interval<ReadMap> >::iterator ol_it;
	
	// create new list.
	list<ReadMap> updated;
	
	// setup helper variables.
	int start, stop, self_size;
	char * p_rname = new char[10];
	strcpy(p_rname, "");
	list<ReadMap>::iterator start_it, stop_it, al_it, it;
	
	// iterate over alignments.
	for(it = alignments.begin(); it != alignments.end(); it++){
	
		// set the stop.
		stop_it = it;
		
		// check if we should process.
		if( strcmp(it->rname, p_rname) != 0 ){
			
			// check if first case or actual.
			if( strcmp(p_rname, "") != 0 ){
				
				// build intervals.
				intervals.clear();
				for(al_it = start_it; al_it != stop_it; al_it++){
					intervals.push_back(Interval<ReadMap>(al_it->start - window, al_it->stop + window, *al_it));
				}
				
				// create tree.
				IntervalTree<ReadMap> tree(intervals);	
				
				// identify overlaps.
				for(al_it = start_it; al_it != stop_it; al_it++){
									
					// compute overlapping set.
					overlaps.clear();
					tree.findOverlapping(al_it->start - window, al_it->stop + window, overlaps);
					
					// add to new list if not overlapping.
					if( (int) overlaps.size() < 2 ){
						updated.push_back(*al_it);
					} 
				}
			}
			
			// reset trackers.
			start_it = it;
			
			// set new previous.
			p_rname = it->rname;
		}
	
	}
	
	// return updated list.
	return updated;
}

void write_agp(list<ReadMap> alignments, char * outagp_file, bam_header_t * bamhead ){
	
	// sort by read name.
	alignments.sort(compare_rname_start);
	
	// open the output file.
	FILE * fout = fopen(outagp_file, "w");
	
	// bootstrap 1 delayed iteration.
	list<ReadMap>::iterator pit = alignments.begin();
	list<ReadMap>::iterator cit = alignments.begin();
	cit++;
	
	// loop until pit hits the end.
	int start, stop;
	int scaf_part = 1;
	bool last = false;
	char * out_txt = new char[5012];
	for(; pit != alignments.end(); pit++){
		
		// check if cit changes scaffold.
		if( strcmp(pit->rname, cit->rname) != 0 ){
			last = true;
		}
		
		// simplify.		
		char o;
		if(pit->orien == 0){
			o = '+';
		} else {
			o = '-';
		}
		
		// make text.
		sprintf(out_txt, "%s\t%d\t%d\t%d\t%s\t%s\t%d\t%d\t%c\n", pit->rname, pit->start, pit->stop, scaf_part, "W", pit->qname, 1, ((pit->stop)-(pit->start)), o);
		
		// sanity check.
		if(pit->stop < pit->start){
			fprintf(stderr, "stop before start:\n%s\n", out_txt);
			exit(1);
		}
		
		/*
		if( last == false && pit->stop > cit->start ){
			fprintf(stderr, "bad order:\n%s\n", out_txt);
			exit(1);
		}
		* */
		
		// write output to file.
		fprintf(fout, out_txt);
		
		// increment cit and part.
		cit++;
		scaf_part++;
		
		// check if end.
		if( cit == alignments.end() ){
			// move back.
			cit--;
		}
	}

	// close output file.
	fclose(fout);
	
	return;
}

/*
********** driver script ************
*/

int main(int argc, char* argv[]) {
	

	// sanity check.
	if( argc < 3 ){
		fprintf(stderr, "wrong number of arguments\n");
		exit(1);
	}

	// parse arguments.
	char * agp_file = argv[1];
	char * refsam_file = argv[2];
	char * outagp_file = argv[3];
	char * filter_file = argv[4];
	int window = atoi(argv[5]);
	fprintf(stdout, "parsed args\n");
	
	// load valid set.
	fprintf(stdout, "loading contig filter.\n");
	sparse_hash_set<string> valid;
	if( string(filter_file) != "-" ){
		valid = make_contig_filter(filter_file);
	}
	
	// load scaffold info.
	fprintf(stdout, "loading agp lookup.\n");
	sparse_hash_map<string, ScafMap> scaflookup = agplookup(agp_file);
	
	// build bad set **********************
	// open the ref BAM file.
	samfile_t * ref_bamin = samopen(refsam_file, "rb", 0);
	if( ref_bamin == 0 ){
		fprintf(stderr, "failed to load BAM file");
		return 1;
	}
	fprintf(stdout, "opened ref sam file\n");
	
	// open the ref BAM index file.
	bam_index_t * bamidx = bam_index_load(refsam_file);
	if( bamidx == 0 ){
		fprintf(stderr,"failed to load index file");
		return 1;		
	}
	fprintf(stdout, "opened ref sam index\n");
	
	// load the alignments into a list.
	list<ReadMap> alignments = load_bam(ref_bamin, bamidx, scaflookup, valid);
	int align_total = (int) alignments.size();
	fprintf(stdout, "loaded alignments: %d\n", align_total);
	
	// remove non-unique alignments.
	non_unique(&alignments);
	int diff = align_total - ((int) alignments.size());
	align_total = (int) alignments.size();
	fprintf(stdout, "removed non-unique: %d\n", diff);
	
	// remove overlaping alignments.
	alignments = overlapping(alignments, window);
	diff = align_total - ((int) alignments.size());
	align_total = (int) alignments.size();
	fprintf(stdout, "removed overlapping: %d\n", diff);

	// write out the AGP.
	write_agp(alignments, outagp_file, ref_bamin->header);
	fprintf(stdout, "wrote agp to file: %d\n", alignments.size());
	
	// close input.
	bam_index_destroy(bamidx);
	samclose(ref_bamin);

	// note we finished properly.
	printf("finished properly\n");
	return 0;
}
