// system
#include <fstream>
#include <iostream>
#include <time.h>
#include <assert.h>
#include <stdlib.h>

// stl
#include <map>
#include <set>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <utility>

// boost
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/dynamic_bitset.hpp>

// HDF5
#include "types.h"

// OGDF
#include <ogdf/basic/Graph.h>
#include <ogdf/basic/GraphAttributes.h>

// google hash
#include <google/sparse_hash_map>
#include <google/sparse_hash_set>

// DEBUG SETTINGS.
#define DEBUG 1
#define VERBOSE 0
#define FASTABUFFER 1000000
char * DEBUGTXT = new char[5012];

using namespace std;
using namespace boost;
using google::sparse_hash_map;
using google::sparse_hash_set;

/*
********** local definitions ************
*/
typedef vector<boost::dynamic_bitset<> > GenomeBit;
typedef sparse_hash_map<string, vector<long> > AdjMap;
/*
********** helper macros ************
*/





/*
********** helper functions ************
*/

static void WRITE_OUT(const char * txt){
	if( VERBOSE == 0 ) {
		fprintf(stdout,txt);
		fflush(stdout);
	}
}

static void WRITE_ERR(const char * txt){
	fprintf(stderr,txt);
	fflush(stdout);
}

static bool ACTIVITY(int i, int j, int k){
	if(i != 0 && (i % k) == 0){
		if( VERBOSE == 0 ) fprintf(stdout,"%d of %d\n", i, j);
		fflush(stdout);
		if( DEBUG == 0 ) {
			return true;

		}
	}
	return false;
}

static string make_key(long a, long b, char * key){
	if( a < b ){
		sprintf(key, "%d_%d", a, b);
	} else {
		sprintf(key, "%d_%d", b, a);
	}
	return string(key);
}

vector<string> tokenize(string txt){
	// create a vector.
	vector<string> result;
	
	// define seperator.
	char_separator<char> sep("\t");
	
	// tokenize the line.
	tokenizer<char_separator<char> > tokens(txt, sep);
	
	// add tokens to vector.
	BOOST_FOREACH(string t, tokens){
		result.push_back(t);
	}
	
	// return everything.
	return result;
}

vector<string> break_key(string txt){
	// create a vector.
	vector<string> result;
	
	// define seperator.
	char_separator<char> sep("_");
	
	// tokenize the line.
	tokenizer<char_separator<char> > tokens(txt, sep);
	
	// add tokens to vector.
	BOOST_FOREACH(string t, tokens){
		result.push_back(t);
	}
	
	// return everything.
	return result;
}

sparse_hash_map<string, long> make_lookup(NodePair np){

	// create lookup hash.
	sparse_hash_map<string, long> node_lookup;

	// populate from node table.
	for(int i=0; i<np.second; i++){
		
		// save to lookup.
		node_lookup[string(np.first[i].ctg_name)] = np.first[i].node_idx;
	}

	return node_lookup;
}

vector<double> load_coverage(NodePair np, EdgePair ep){
	
	// build bitset arrays.
	GenomeBit genome(np.second);
	for(int i=0; i<np.second; i++){
		genome[np.first[i].node_idx] = boost::dynamic_bitset<> (np.first[i].ctg_width);
	}
	
	// annotate the bitset.
	double cum_sum = 0.0;
	double act_cnt = 0.0;
	int idx1, idx2;
	for(unsigned int i=0; i<ep.second; i++){
	
		// skip invalid.
		if( ep.first[i].invalid == 1 ) continue;
		
		// get indices.
		idx1 = ep.first[i].ctg_a_idx;
		idx2 = ep.first[i].ctg_b_idx;
		
		// annotate the contig.
		for(int j=ep.first[i].read_a_left_pos; j<ep.first[i].read_a_right_pos; j++){
			genome[idx1][j] = true;
		}
		for(int j=ep.first[i].read_b_left_pos; j<ep.first[i].read_b_right_pos; j++){
			genome[idx2][j] = true;
		}		
		
		// note the insert size.
		cum_sum += ep.first[i].insert_size;
		act_cnt++;
	}	
	
	// settle on insert size.
	int insert_size = cum_sum / act_cnt;
	
	// calculate coverage of contig ends.
	vector<double> annotation(np.second);
	double tcnt, ttot;
	int front_stop, back_start;
	for(int i=0; i<np.second; i++){
		
		// prep numbers.
		tcnt = 0.0;
		ttot = 0.0;
		
		if( np.first[i].ctg_width > insert_size ){
			front_stop = insert_size;
		}
		
		back_start = np.first[i].ctg_width - insert_size;
		
		if( back_start < 0 ){
			back_start = 0;
		}
		
		// count front.
		for(int j=0; j<front_stop; j++){
			tcnt += genome[i][j];
			ttot++;
		}
		
		// count back.
		if( back_start > front_stop ){
			for(int j=back_start; j<np.first[i].ctg_width; j++){
				tcnt += genome[i][j];
				ttot++;
			}			
		}
		
		// calculate coverage.
		annotation[i] = tcnt / ttot;
	}
	
	// return result.
	return annotation;
	
}


GenomeBit load_annotation(NodePair np, sparse_hash_map<string, long> nlookup, const char * file_path){
	
	// build bitset arrays.
	GenomeBit genome(np.second);
	for(int i=0; i<np.second; i++){
		genome[np.first[i].node_idx] = boost::dynamic_bitset<> (np.first[i].ctg_width);
	}
	
	// open gff file.
	ifstream myfile(file_path);
	
	// check if file is open.
	if( myfile.is_open() == false ){
		WRITE_ERR("error opening file.\n");
		exit(1);
	}
	// loop over file.
	vector<string> tokens;
	std::string line, ctg;
	long idx, start, stop;
	while ( myfile.good() ) {
		
		// get line.
		getline (myfile,line);
		
		// skip empty lines.
		if(line.length() < 2){
			continue;
		}
		
		// tokenize.
		tokens = tokenize(line);
		
		// grab info.
		ctg = tokens[0];
		start = atoi(tokens[3].c_str());
		stop = atoi(tokens[4].c_str());
		
		// translate info.
		if( nlookup.find(ctg) == nlookup.end() ){
			continue;
		}
		idx = nlookup[ctg];
		
		// annotate the genome.
		for(long i=start; i<stop; i++){
			genome[idx][i] = true;
		}
	}
	
	// close file.
	myfile.close();
	
	// return result.
	return genome;
}

AgpPair load_agp(const char * file_path){
	
	// create a list of appropriate types.
	list<AgpTable> agps;
	
	// open AGP file.
	ifstream myfile(file_path);
	
	// check if file is open.
	if( myfile.is_open() == false ){
		WRITE_ERR("error opening file.\n");
		exit(1);
	}
	// loop over file.
	vector<string> tokens;
	std::string line, ctg;
	int idx, start, stop;
	while ( myfile.good() ) {
		
		// get line.
		getline (myfile,line);
		
		// skip empty lines.
		if(line.length() < 2){
			continue;
		}
		
		// tokenize.
		tokens = tokenize(line);
		
		// skip runtime line.
		if(tokens[0] == "RUNTIME:") continue;
		
		// prep struct.
		AgpTable tmp;
		
		// copy to struct base on type.
		if( tokens[4] == "W" ){
			strcpy(tmp.scaf_name, tokens[0].c_str());
			tmp.scaf_start = atoi(tokens[1].c_str());
			tmp.scaf_stop = atoi(tokens[2].c_str());
			tmp.scaf_idx = atoi(tokens[3].c_str());
			strcpy(tmp.comp_type, tokens[4].c_str());
			strcpy(tmp.comp_name, tokens[5].c_str());
			tmp.comp_start = atoi(tokens[6].c_str());
			tmp.comp_stop = atoi(tokens[7].c_str());
			if( tokens[8] == "+" ){
				tmp.comp_orien = 0;
			} else {
				tmp.comp_orien = 1;
			}
			tmp.comp_linkage = 0;
			
		} else if( tokens[4] == "N" ){
			strcpy(tmp.scaf_name, tokens[0].c_str());
			tmp.scaf_start = atoi(tokens[1].c_str());
			tmp.scaf_stop = atoi(tokens[2].c_str());
			tmp.scaf_idx = atoi(tokens[3].c_str());
			strcpy(tmp.comp_type, tokens[4].c_str());
			strcpy(tmp.comp_name, tokens[6].c_str());
			tmp.comp_start = 1;
			tmp.comp_stop = atoi(tokens[5].c_str());
			tmp.comp_orien = -1;
			if( tokens[7] == "no" ){
				tmp.comp_linkage = 0;
			} else {
				tmp.comp_linkage = 1;
			}			
			
		} else {
			cout << tokens[5].c_str() << endl;
			cout << line.c_str() << endl;
			WRITE_ERR("shouldn't be here\n");
			exit(1);
		}
		
		// add to list.
		agps.push_back(tmp);
		
	}
	
	// create array from list.
	AgpTable * result = new AgpTable[agps.size()];
	
	// copy shtuff into it.
	hsize_t i =0;
	for( list<AgpTable>::iterator it=agps.begin(); it!=agps.end(); it++){
		
		strcpy(result[i].scaf_name, it->scaf_name);
		result[i].scaf_start = it->scaf_start;
		result[i].scaf_stop = it->scaf_stop;
		result[i].scaf_idx = it->scaf_idx;
		strcpy(result[i].comp_type, it->comp_type);
		strcpy(result[i].comp_name, it->comp_name);
		result[i].comp_start = it->comp_start;
		result[i].comp_stop = it->comp_stop;
		result[i].comp_orien = it->comp_orien;
		result[i].comp_linkage = it->comp_linkage;
		i++;
	}

	// close file.
	myfile.close();
	
	// return pair.
	return AgpPair(result, agps.size());
}

long determine_state(long idxa, long idxb, long oriena, long orienb){
	long state = -1;
	if( idxa < idxb ){
		if( oriena == 0 && orienb == 0 ){
			state = 0;
		} else if( oriena == 0 && orienb == 1 ){
			state = 1;
		} else if( oriena == 1 && orienb == 0 ){
			state = 2;
		} else if( oriena == 1 && orienb == 1 ){
			state = 3;
		}
	} else {
		if( oriena == 1 && orienb == 1 ){
			state = 0;
		} else if( oriena == 0 && orienb == 1 ){
			state = 1;
		} else if( oriena == 1 && orienb == 0 ){
			state = 2;
		} else if( oriena == 0 && orienb == 0 ){
			state = 3;
		}		
	}
	return state;
}

/*
********** program functions ************
*/

BundlePair create_bundles(AdjMap adjmap, long ecount){
	
	// allocate table memory.
	BundleTable * bundles = new BundleTable[adjmap.size() + ecount];
	
	// populate table with default info.
	for(hsize_t i=0; i< adjmap.size() + ecount; i++){
		
		// zero the array.
		bundles[i].ctg_a_idx = -1;
		bundles[i].ctg_b_idx = -1;
		bundles[i].WT_A = 0.0;
		bundles[i].WT_B = 0.0;
		bundles[i].WT_C = 0.0;
		bundles[i].WT_D = 0.0;
	}
	
	
	
	// return pair.
	return BundlePair(bundles, adjmap.size() + ecount);
}

void count_weights(AdjMap adjmap, EdgePair ep, BundlePair bp){
	
	// loop over adjacency.
	vector<string> tokens;
	long idxa, idxb;
	long idx = 0;
	for(AdjMap::iterator it1=adjmap.begin(); it1!=adjmap.end(); it1++){
		
		// tokenize index.
		tokens = break_key(it1->first);
		idxa = atoi(tokens[0].c_str());
		idxb = atoi(tokens[1].c_str());
			
		// set index.
		bp.first[idx].ctg_a_idx = idxa;
		bp.first[idx].ctg_b_idx = idxb;
			
		// loop over edge vector.
		for(vector<long>::iterator it2=it1->second.begin(); it2 != it1->second.end(); it2++){
			
			// set weights.
			switch( ep.first[*it2].implied_state ){
			case 0:	
				bp.first[idx].WT_A += 1.0;
				break;
			case 1:
				bp.first[idx].WT_B += 1.0;
				break;
			case 2:
				bp.first[idx].WT_C += 1.0;
				break;
			case 3:
				bp.first[idx].WT_D += 1.0;
				break;
			default:
				WRITE_ERR("bad state\n");
				exit(1);
			}
		}
		
		// increment bundle index.
		idx++;
	}
	
}

void repeat_weights(AdjMap adjmap, NodePair np, EdgePair ep, BundlePair bp, const char * file_path){
	
	// make node lookup.
	sparse_hash_map<string, long> nlookup = make_lookup(np);
	
	
	// load annotation.
	GenomeBit repeat_ant = load_annotation(np, nlookup, file_path);
	
	// loop over adjacency.
	vector<string> tokens;
	long idxa, idxb;
	long idx = 0;
	long start, stop, total, olap_cnt;
	double olap_perc_a, olap_perc_b, weight;
	for(AdjMap::iterator it1=adjmap.begin(); it1!=adjmap.end(); it1++){
		
		// tokenize index.
		tokens = break_key(it1->first);
		idxa = atoi(tokens[0].c_str());
		idxb = atoi(tokens[1].c_str());
			
		// set index.
		bp.first[idx].ctg_a_idx = idxa;
		bp.first[idx].ctg_b_idx = idxb;
			
		// loop over edge vector.
		for(vector<long>::iterator it2=it1->second.begin(); it2 != it1->second.end(); it2++){
			
			// calculate overlap.
			start = ep.first[*it2].read_a_left_pos;
			stop = ep.first[*it2].read_a_right_pos;
			total = (double) start + (double) stop;
			olap_cnt = 0;
			for(int j=start; j<stop; j++){
				olap_cnt += repeat_ant[idxa][j];
			}
			
			olap_perc_a = 1.0 - ((double) olap_cnt / total);
			start = ep.first[*it2].read_b_left_pos;
			stop = ep.first[*it2].read_b_right_pos;
			total = (double) start + (double) stop;
			olap_cnt = 0;
			for(int j=start; j<stop; j++){
				olap_cnt += repeat_ant[idxb][j];
			}
			olap_perc_b = 1.0 - ((double) olap_cnt / total);	
			
			// calculate weight.
			weight = olap_perc_a * olap_perc_b;
			
			// set weights.
			switch( ep.first[*it2].implied_state ){
			case 0:	
				bp.first[idx].WT_A += weight;
				break;
			case 1:
				bp.first[idx].WT_B += weight;
				break;
			case 2:
				bp.first[idx].WT_C += weight;
				break;
			case 3:
				bp.first[idx].WT_D += weight;
				break;
			default:
				WRITE_ERR("bad state\n");
				exit(1);
			}
		}
		
		// increment bundle index.
		idx++;
	}	
}

void coverage_weights(AdjMap adjmap, NodePair np, EdgePair ep, BundlePair bp){
	
	// annotate the contigs.
	vector<double> annotation = load_coverage(np, ep);
	
	// weight each bundle by sum of probabilities.
	vector<string> tokens;
	long idxa, idxb;
	long idx = 0;
	int case1, case2, case3, case4;
	double min;
	for(AdjMap::iterator it1=adjmap.begin(); it1!=adjmap.end(); it1++){
		
		// tokenize index.
		tokens = break_key(it1->first);
		idxa = atoi(tokens[0].c_str());
		idxb = atoi(tokens[1].c_str());
			
		// set index.
		bp.first[idx].ctg_a_idx = idxa;
		bp.first[idx].ctg_b_idx = idxb;
		
		// count each style.
		for(vector<long>::iterator it2=it1->second.begin(); it2 != it1->second.end(); it2++){
			
			// set weights.
			switch( ep.first[*it2].implied_state ){
			case 0:	
				bp.first[idx].WT_A += 1.0;
				break;
			case 1:
				bp.first[idx].WT_B += 1.0;
				break;
			case 2:
				bp.first[idx].WT_C += 1.0;
				break;
			case 3:
				bp.first[idx].WT_D += 1.0;
				break;
			default:
				WRITE_ERR("bad state\n");
				exit(1);
			}
		}
		
		// determine largest.
		case1 = bp.first[idx].WT_A >= bp.first[idx].WT_B && bp.first[idx].WT_A >= bp.first[idx].WT_C && bp.first[idx].WT_A >= bp.first[idx].WT_D;
		case2 = bp.first[idx].WT_B >= bp.first[idx].WT_A && bp.first[idx].WT_B >= bp.first[idx].WT_C && bp.first[idx].WT_B >= bp.first[idx].WT_D;
		case3 = bp.first[idx].WT_C >= bp.first[idx].WT_A && bp.first[idx].WT_C >= bp.first[idx].WT_B && bp.first[idx].WT_C >= bp.first[idx].WT_D;
		case4 = bp.first[idx].WT_D >= bp.first[idx].WT_A && bp.first[idx].WT_D >= bp.first[idx].WT_B && bp.first[idx].WT_D >= bp.first[idx].WT_C;
		
		// determine the minimum.
		if( annotation[idxa] < annotation[idxb] ){
			min = annotation[idxa];
		} else {
			min = annotation[idxb];
		}
		
		// weight the largest according to confidence, scale the rest down to 10%.
		if( case1 ){
			bp.first[idx].WT_A = min;
			if( bp.first[idx].WT_B > 0.0 ) bp.first[idx].WT_B = .01;
			if( bp.first[idx].WT_C > 0.0 ) bp.first[idx].WT_C = .01;
			if( bp.first[idx].WT_D > 0.0 ) bp.first[idx].WT_D = .01;
		} else if( case2 ){
			bp.first[idx].WT_B = min;
			if( bp.first[idx].WT_A > 0.0 ) bp.first[idx].WT_A = .01;
			if( bp.first[idx].WT_C > 0.0 ) bp.first[idx].WT_C = .01;
			if( bp.first[idx].WT_D > 0.0 ) bp.first[idx].WT_D = .01;			
		} else if( case3 ){
			bp.first[idx].WT_C = min;
			if( bp.first[idx].WT_A > 0.0 ) bp.first[idx].WT_A = .01;
			if( bp.first[idx].WT_B > 0.0 ) bp.first[idx].WT_B = .01;
			if( bp.first[idx].WT_D > 0.0 ) bp.first[idx].WT_D = .01;			
		} else if( case4 ){
			bp.first[idx].WT_D = min;
			if( bp.first[idx].WT_A > 0.0 ) bp.first[idx].WT_A = .01;
			if( bp.first[idx].WT_B > 0.0 ) bp.first[idx].WT_B = .01;
			if( bp.first[idx].WT_C > 0.0 ) bp.first[idx].WT_C = .01;			
		} else {
			cout << "errr" << endl;
			cout << bp.first[idx].WT_A << endl;
			cout << bp.first[idx].WT_B << endl;
			cout << bp.first[idx].WT_C << endl;
			cout << bp.first[idx].WT_D << endl;
			exit(1);
		}
		
		
		// increment bundle index.
		idx++;
		
	}	
}

void given_weights(AdjMap adjmap, NodePair np, EdgePair ep, BundlePair bp){
	
	// loop over edge pair.
	char * tmpkey = new char[10000];
	double weight;
	long long int idx, idxa, idxb;
	vector<string> tokens;
	for(long i=0; i<ep.second; i++){
		
		// skip invalid.
		if( ep.first[i].invalid == 1 ) continue;
		
		// make key.
		string key = make_key(ep.first[i].ctg_a_idx, ep.first[i].ctg_b_idx, tmpkey);
		
		// tokenize index.
		tokens = break_key(key);
		idxa = atoi(tokens[0].c_str());
		idxb = atoi(tokens[1].c_str());
		
		// get the vector.
		vector<long> bidxs = adjmap[key];
		
		// lookup bundles for this edge.
		for(unsigned int j = 0; j < bidxs.size(); j++){
			
			// simplify.
			weight = ep.first[i].weight;
			idx = bidxs[j];
			
			// set index.
			bp.first[idx].ctg_a_idx = idxa;
			bp.first[idx].ctg_b_idx = idxb;
			
			// switch on state.
			switch( ep.first[i].implied_state ){
			case 0:	
				bp.first[idx].WT_A += weight;
				break;
			case 1:
				bp.first[idx].WT_B += weight;
				break;
			case 2:
				bp.first[idx].WT_C += weight;
				break;
			case 3:
				bp.first[idx].WT_D += weight;
				break;
			default:
				WRITE_ERR("bad state\n");
				exit(1);
			}
		}
		
	}
}

AdjMap build_adjacency(EdgePair ep){
	
	// create a hash table for the lookup.
	AdjMap adjmap;
	
	// add pairs to hash.
	char * tmpkey = new char[10000];
	for(long i=0; i<ep.second; i++){
		
		// skip invalid.
		if( ep.first[i].invalid == 1 ) continue;
		
		// make key.
		string key = make_key(ep.first[i].ctg_a_idx, ep.first[i].ctg_b_idx, tmpkey);
		
		// check for bootstrap.
		if( adjmap.find(key) == adjmap.end() ){
			adjmap[key] = vector<long>();
		}
		
		// add index to map.
		adjmap[key].push_back(i);
	}
	
	return adjmap;
}

void agp_add(AgpPair ap, BundlePair bp, sparse_hash_map<string, long> nlookup){

	// figure starting point.
	long idx = -1;
	for(hsize_t i=0; i<bp.second; i++){
		if( bp.first[i].ctg_a_idx == -1){
			idx = i;
			break;
		}
	}
	if( idx == -1 ){
		WRITE_ERR("BAD BUNDLE BEAR\n");
		exit(1);
	}

	// add edges.
	long idxa, idxb, oriena, orienb, state, s;
	for(hsize_t i=0; i<ap.second; i++){
		if( string(ap.first[i].comp_type) == "N" ){
			
			// get neighbor info.
			idxa = nlookup[ap.first[i-1].comp_name];
			idxb = nlookup[ap.first[i+1].comp_name];
			
			oriena = ap.first[i-1].comp_orien;
			orienb = ap.first[i+1].comp_orien;
			
			// perform order swap.
			if( idxa > idxb ){
				s = idxa;
				idxa = idxb;
				idxb = s;
				
				s = oriena;
				oriena = orienb;
				orienb = s;
			}
			
			// determine state.
			state = determine_state(idxa, idxb, oriena, orienb);
			
			// fill out fake bundle.
			bp.first[idx].ctg_a_idx = idxa;
			bp.first[idx].ctg_b_idx = idxb;
			bp.first[idx].WT_A = 987654321.0;
			bp.first[idx].WT_B = 987654321.0;
			bp.first[idx].WT_C = 987654321.0;
			bp.first[idx].WT_D = 987654321.0;

			/*
			// weight appropriate.
			switch(state){
			case 0:
				bp.first[idx].WT_A = 987654321.0;
				break;
			case 1:
				bp.first[idx].WT_B = 987654321.0;
				break;
			case 2:
				bp.first[idx].WT_C = 987654321.0;
				break;
			case 3:
				bp.first[idx].WT_D = 987654321.0;
				break;
			default:
				WRITE_ERR("SHOULDNT BE HERE\n");
				exit(1);
			}
			*/
			// increment idx.
			idx++;
		}
	}
	
}

/*
********** driver script ************
*/

int main(int argc, char* argv[]) {
	
	// sanity check.
	if( argc < 2 ){
		fprintf(stderr, "wrong number of arguments\n");
		exit(1);
	}

	// parse args.
	const char * node_file = argv[1];
	const char * edge_file = argv[2];
	const char * bundle_file = argv[3];
	const char * weight_type = argv[4];
	const char * annot_file = argv[5];
	const char * agp_file = argv[6];
	int debug = atoi(argv[7]);
	
	// load node info.
	WRITE_OUT("load node information\n");
	NodePair np = node_table_load(node_file);
	sparse_hash_map<string, long> nlookup = make_lookup(np);
	
	// load edge info.
	WRITE_OUT("load pair information\n");
	EdgePair ep = edge_table_load(edge_file);
	
	// build the adjacency set.
	AdjMap adjmap;
	if( debug == 0 ){
		adjmap = build_adjacency(ep);
	}
	
	// load agp if necessary.
	long ecount = 0;
	AgpPair ap;
	if( strcmp(agp_file, "-") != 0 ){
		WRITE_OUT("loading AGP file\n");
		ap = load_agp(agp_file);
	}
	
	// create the pair.
	WRITE_OUT("creating bundle table\n");
	BundlePair bp = create_bundles(adjmap, ecount);

	// sanityc check bundles.
	if( bp.second < 1 ){
		WRITE_ERR("ERROR: no bundles were created!\n");
		exit(1);
	}
	
	// assign weights.
	if( strcmp(weight_type, "count") == 0 ){
		WRITE_OUT("assigning count weights\n");
		count_weights(adjmap, ep, bp);
		
	} else if( strcmp(weight_type, "repeat") == 0 ){
		WRITE_OUT("assigning repeat overlap weights\n");
		repeat_weights(adjmap, np, ep, bp, annot_file);		
	
	} else if( strcmp(weight_type, "coverage") == 0 ){
		WRITE_OUT("assigning coverage weights\n");
		coverage_weights(adjmap, np, ep, bp);		
	
	} else if( strcmp(weight_type, "given") == 0 ){
		WRITE_OUT("assigning given weights\n");
		given_weights(adjmap, np, ep, bp);	
	
	
	} else {
		WRITE_ERR("unkown weight type\n");
		exit(1);
	}
	
	/*
	if( strcmp(agp_file, "-") != 0 ){
		WRITE_OUT("adding artificial AGP bundles\n");
		agp_add(ap, bp, nlookup);
	}
	*/

	// save bundle.
	sprintf(DEBUGTXT, "save %d bundles\n", bp.second);
	WRITE_OUT(DEBUGTXT);
	bundle_table_save(bp.first, bp.second, "bundles", bundle_file);
	
	// note we finished properly.
	if( VERBOSE == 0 ) printf("finished properly\n");
	return 0;
}
