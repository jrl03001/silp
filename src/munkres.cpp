#include "munkres.h"
using namespace H5;


int main (int argc, char * const argv[]) {
	
	// parse arguments.
	const char * FILE_NAME = argv[1];
	const char * OUT_FILE = argv[2];
	
	// open the file.
	hid_t FILE_ID = H5Fopen(FILE_NAME, H5F_ACC_RDWR, H5P_DEFAULT);
	
	// get the rank and dimensions.
	int rank;
	hsize_t * dims;
	H5LTget_dataset_ndims (FILE_ID, "adj",  &rank );
	dims = new hsize_t[rank];
	H5LTget_dataset_info ( FILE_ID, "adj", dims, NULL, NULL);
	
	// allocate the integer array.
	int ** buffer = new int * [dims[0]];
	for(int i=0; i<dims[0]; i++){
		buffer[i] = new int[dims[1]];
	}
	
	// read the data.
	H5LTread_dataset_int( FILE_ID, "adj", *buffer);
	
	// close the file.
	H5Fclose(FILE_ID);
	
	/*
	for(int i=0; i<dims[0]; i++){
		for(int j=0; j<dims[1]; j++){
			fprintf(stdout,"%d\t", buffer[i][j]);
		}
		fprintf(stdout,"\n");
	}
	*/
	
	// create new hungarian problem.
	hungarian_problem_t p;
	
	// initialize the gungarian_problem using the cost matrix
	int matrix_size = hungarian_init(&p, buffer , dims[0], dims[1], HUNGARIAN_MODE_MAXIMIZE_UTIL) ;
	
	// some output 
	//fprintf(stderr, "cost-matrix:");
	//hungarian_print_costmatrix(&p);

	// solve the assignement problem 
	hungarian_solve(&p);

	// some output 
	//fprintf(stderr, "assignment:");
	//hungarian_print_assignment(&p);
	
	// output matched verticies:
	FILE * fout = fopen(OUT_FILE, "w");
	for(int i=0; i<dims[0]; i++){
		for(int j=0; j<dims[1]; j++){
			if( p.assignment[i][j] == 1 ){
				fprintf(fout, "%d\t%d\n", i, j);
			}
		}
	}
	fclose(fout);
	
	// free used memory 
	hungarian_free(&p);

	
	return 0;
}
