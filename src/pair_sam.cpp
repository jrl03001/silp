// system
#include <fstream>
#include <iostream>
#include <time.h>
#include <assert.h>
#include <stdlib.h>

// stl
#include <map>
#include <set>
#include <string>
#include <vector>
#include <list>
#include <algorithm>

// boost
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/dynamic_bitset.hpp>

// samtools
#include "sam.h"

// google hash
#include <google/sparse_hash_map>
#include <google/sparse_hash_set>

// DEBUG SETTINGS.
#define DEBUG 1
#define VERBOSE 0

// global variable for key dist.
int KEY_DIST;

using namespace std;
using namespace boost;
using google::sparse_hash_map;
using google::sparse_hash_set;

/*
********** local definitions ************
*/

typedef struct PairMap {
	const char * read;
	int tid;
	int idx;
} PairMap;

/*
********** helper functions ************
*/

static void stdwrite(const char * txt){
	if( VERBOSE == 0 ) fprintf(stdout,txt);
}

static bool activity(int i, int j){
	if(i != 0 && (i % j) == 0){
		if( VERBOSE == 0 ) fprintf(stdout,".");
		fflush(stdout);
		if( DEBUG == 0 ) {
			return true;
		}
	}
	return false;
}

bool compare_read(const PairMap &a, const PairMap &b) {
	
	int c = strcmp(a.read, b.read);
	if( c <= 0 ){
		return true;
	} else {
		return false;
	}
}

#define bam1_unmapped(b) (((b)->core.flag & BAM_FUNMAP) != 0)

/*
********** program functions ************
*/

// creates a filter for 
sparse_hash_set<string> make_contig_filter(const char * file_name){

	// create hash.
	sparse_hash_set<string> valid;

	// create buffer.
	string buffer;

	// parse out contigs.
	ifstream infile;
	infile.open(file_name);
	while(!infile.eof()) {
		
		// load line into buffer.
		getline(infile,buffer); 
		
		// add buffer to set.
		valid.insert(string(buffer.c_str()));
	}
	infile.close();
	
	// return hash.
	return valid;
}

// loads info needed to pair from alignments.
list<PairMap> load_pair(char * read_file, sparse_hash_set<string> valid){

	// open the BAM file.
	samfile_t * bamin = samopen(read_file, "rb", 0);
	if( bamin == 0 ){
		fprintf(stderr, "failed to load BAM file: %s", read_file);
		exit(1);
	}
	
	// create list.
	list<PairMap> pairs;
	
	// allocate a bam object.
	bam1_t * b = new bam1_t();
	
	// loop over alignments.
	string rname;
	int idx = 0;
	int str_size;
	int status = bam_read1(bamin->x.bam, b);  
	while( status > 0 ){
		
		// skip unkown reference.
		if( b->core.tid != -1) {
		
			// identify rname string.
			rname = string(bamin->header->target_name[b->core.tid]);
			
			// skip unmapped and invalid.&& valid.find( ) != valid.end()
			//if( (bam1_unmapped(b) == false) && (valid.find(rname) != valid.end()) ) {
			if( (bam1_unmapped(b) == false) ) {

				// shorten the read name.
				str_size = strlen(bam1_qname(b));
				char * qname = new char[str_size - KEY_DIST + 1];

				// shorten read name.
				strncpy(qname, bam1_qname(b), str_size - KEY_DIST);
				qname[str_size - KEY_DIST] = '\0';
				
				// create pair struct.
				PairMap pair;
				pair.tid = b->core.tid;
				pair.read = qname;
				pair.idx = idx;
				
				// add to list.
				pairs.push_back(pair);

			} 
		}
		
		// update things.
		status = bam_read1(bamin->x.bam, b); 
		idx++;
		
		// activity.
		if( activity(idx, 1000000) == true ) break;
	}

	// pretty print.
	stdwrite("\n");
	
	// close the BAM file.
	samclose(bamin);
	
	// return pairs.
	return pairs;
}

// removes non-unique alignments.
list<PairMap> non_unique(list<PairMap> alignments){
	
	// sort the alignments by name.
	alignments.sort(compare_read);
	
	// create a list for accepted alignments.
	list<PairMap> accepted;
	
	// bootstrap comparison.
	list<PairMap>::iterator p_it = alignments.begin();
	list<PairMap>::iterator c_it = alignments.begin();
	c_it++;
	
	// look for sequential names.
	while( c_it != alignments.end() ){
	
		// check previous.
		if( strcmp(c_it->read, p_it->read) != 0 ){
			
			// copy previous to new list.
			accepted.push_back(*p_it);
			
		} else {
		
			printf("exit1\n");
			exit(1);
		}
		
		// update iterators.
		p_it++;
		c_it++;
	}
	
	// add last.
	accepted.push_back(*p_it);
	
	// release old memory.
	alignments.clear();

	// return modified alignments.
	return accepted;
}

// pairs the reads and ensure they map to difference ctgs.
void pair_reads(list<PairMap> pairs1, list<PairMap> pairs2, list<int> * use1, list<int> * use2){
	
	// iterate over lists.
	list<PairMap>::iterator it1 = pairs1.begin();
	list<PairMap>::iterator it2 = pairs2.begin();
	int comp;
	while( it1 != pairs1.end() && it2 != pairs2.end() ){
		
		// make comparison.
		comp = strcmp(it1->read, it2->read);
		
		// check if they are equal and diff tid.
		if( comp == 0 ){
			
			// append the match if different contigs.
			if( it1->tid != it2->tid ){
				use1->push_back(it1->idx);
				use2->push_back(it2->idx);
			}
			
			// increment both.
			it1++;
			it2++;
		} else if( comp < 0 ){
			it1++;
		} else {
			it2++;
		}
	}
}

// reads BAM file again and writes it out.
void write_pair(list<int> use, char * read_file, char * out_file){

	// sort the use list.
	use.sort();

	// open the BAM in file.
	samfile_t * bamin = samopen(read_file, "rb", 0);
	if( bamin == 0 ){
		fprintf(stderr, "failed to load BAM file: %s", read_file);
		exit(1);
	}

	// open the BAM out file.
	samfile_t * samout = samopen(out_file, "w", bamin->header);
	if( samout == 0 ){
		fprintf(stderr, "failed to open SAM file: %s", out_file);
		exit(1);
	}
	
	// allocate a bam object array.
	bam1_t * b = bam_init1();
	
	// track integer we keep.
	int track_idx = 0;
	list<int>::iterator use_it = use.begin();
	
	// loop over alignments.
	int cur_idx = 0;
	int status = bam_read1(bamin->x.bam, b);  
	while( status > 0 ){
		
		// check if this is an active idx.
		if( cur_idx == (*use_it) ){
			
			// write this.
			samwrite(samout, b);
			
			// increment use pointer.
			use_it++;
			
			// check if we're done.
			if( use_it == use.end() ){
				break;
			}
		}
		
		// update things.
		status = bam_read1(bamin->x.bam, b); 
		cur_idx++;
		
		// activity.
		if( activity(cur_idx, 10000000) == true ) break;
	}

	// pretty print.
	stdwrite("\n");
    
	// close the BAM in file.
	samclose(bamin);
	
	// close the BAM out file. 
	samclose(samout);
	
	// release.
	bam_destroy1(b);
	
}

/*
********** driver script ************
*/

int main(int argc, char* argv[]) {
	
	// sanity check.
	if( argc < 3 ){
		fprintf(stderr, "wrong number of arguments\n");
		exit(1);
	}

	// parse arguments.
	char * filter_file = argv[1];
	char * read1_file = argv[2];
	char * read2_file = argv[3];
	KEY_DIST = atoi(argv[4]);
	char * out1_file = argv[5];
	char * out2_file = argv[6];
	if( VERBOSE == 0 ) fprintf(stdout, "parsed args\n");
	
	// load valid set.
	sparse_hash_set<string> valid = make_contig_filter(filter_file);
	//sparse_hash_set<string> valid;
	if( VERBOSE == 0 ) fprintf(stdout, "made filter\n");
	
	
	// create pair list from 1.
	list<PairMap> pairs1 = load_pair(read1_file, valid);
	int size1a = pairs1.size();
	if( VERBOSE == 0 ) fprintf(stdout, "loaded entries from file 1: %d\n", size1a);

	// create pair list from 2.
	list<PairMap> pairs2 = load_pair(read2_file, valid);
	int size2a = pairs2.size();
	if( VERBOSE == 0 ) fprintf(stdout, "loaded entries from file 2: %d\n", size2a);

	// remove duplicates.
	pairs1 = non_unique(pairs1);
	int size1b = pairs1.size();
	if( VERBOSE == 0 ) fprintf(stdout, "removed duplicates 1: %d\n", size1a - size1b);
	
	pairs2 = non_unique(pairs2);
	int size2b = pairs2.size();
	if( VERBOSE == 0 ) fprintf(stdout, "removed duplicates 2: %d\n", size2a - size2b);

	// pair them.
	list<int> use1;
	list<int> use2;
	pair_reads(pairs1, pairs2, &use1, &use2);
	if( VERBOSE == 0 ) fprintf(stdout, "found the following pairs: %d %d\n", use1.size(), use2.size());
	
	// write first pair.
	write_pair(use1, read1_file, out1_file);
	write_pair(use2, read2_file, out2_file);

	// note we finished properly.
	if( VERBOSE == 0 ) printf("finished properly\n");
	return 0;
}
