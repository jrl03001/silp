// system
#include <fstream>
#include <iostream>
#include <time.h>
#include <assert.h>
#include <stdlib.h>

// stl
#include <map>
#include <set>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <utility>

// boost
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/dynamic_bitset.hpp>

// samtools
#include "sam.h"

// HDF5
#include "types.h"

// FASTA
#include "kseq.h"
KSEQ_INIT(gzFile, gzread)

// DEBUG SETTINGS.
#define DEBUG 1
#define VERBOSE 0

using namespace std;
using namespace boost;

/*
********** local definitions ************
*/

typedef struct PairMap {
	const char * read;
	int tid;
	int idx;
} PairMap;

/*
********** helper functions ************
*/

static void stdwrite(const char * txt){
	if( VERBOSE == 0 ) fprintf(stdout,txt);
}

static bool activity(int i, int j){
	if(i != 0 && (i % j) == 0){
		if( VERBOSE == 0 ) fprintf(stdout,".");
		fflush(stdout);
		if( DEBUG == 0 ) {
			return true;
		}
	}
	return false;
}

#define bam1_unmapped(b) (((b)->core.flag & BAM_FUNMAP) != 0)

/*
********** program functions ************
*/

list<pair<char *, int> > load_fasta(const char * file_path){
	
	// create file pointer.
	gzFile fp;

	// open the file.
	fp = gzopen(file_path, "r");
	
	// create list to store structs.
	list<pair<char *, int> > seqs;
	
	// create seq struct pointer.
	kseq_t * seq;
	seq = kseq_init(fp);
	
	// loop over sequences.
	int l;
	while ((l = kseq_read(seq)) >= 0) {
		
		// copy name.
		char * name = new char[seq->name.l];
		strcpy(name, seq->name.s);
		
		// push info to list.
		seqs.push_back( pair<char *, int>(name, seq->seq.l) );
		
		/*
		printf("name: %s\n", seq->name.s);
		if (seq->comment.l) printf("comment: %s\n", seq->comment.s);
		printf("seq: %s\n", seq->seq.s);
		if (seq->qual.l) printf("qual: %s\n", seq->qual.s);
		*/
	}

	// clean up and close.
	kseq_destroy(seq);
	gzclose(fp);	
		
	// return list.
	return seqs;
}

NodeTable * create_nodes(list<pair<char *, int> > seqs){
	
	// build table from list.
	NodeTable * table = new NodeTable[seqs.size()];
	
	// fill the table.
	int i = 0;
	for(list<pair<char *, int> >::iterator it = seqs.begin(); it!=seqs.end(); it++){
		
		// fill table.
		table[i].node_idx = i;
		strcpy(table[i].ctg_name, it->first);
		table[i].ctg_width = it->second;
		table[i].ctg_orien = 0;
		table[i].ctg_order = i;
		table[i].invalid = false;
		i++;
		
	}
	
	// clear list.
	seqs.clear();
	
	// return it.
	return table;
}
/*
********** driver script ************
*/

int main(int argc, char* argv[]) {
	
	// sanity check.
	if( argc < 2 ){
		fprintf(stderr, "wrong number of arguments\n");
		exit(1);
	}

	// parse args.
	const char * fasta_file = argv[1];
	const char * node_file = argv[2];

	// load fasta entries.
	stdwrite("reading fasta\n");
	list<pair<char *, int> > seqs = load_fasta(fasta_file);
	size_t table_size = seqs.size();
	
	// create table.
	stdwrite("creating table\n");
	NodeTable * table = create_nodes(seqs);
	
	// save table.
	stdwrite("saving table\n");	
	node_table_save(table, table_size, "nodes", node_file);

	// note we finished properly.
	if( VERBOSE == 0 ) printf("finished properly\n");
	return 0;
}
